#pragma once
#include <fstream>
#include "TypeDef.h"
#include "cYUV.h"
#include "cDepthBlock.h"
#include <deque>
#include <vector>

using namespace std;

class cSplitter
{
public:
	cSplitter();
	cSplitter(const String& _yuvFilenameIn, const Float _skipThresh, const UInt _partThresh, const UInt _quantInterval, const V2ui& _maxBlockSize, const V2ui& _minBlockSize, Bool _useRects, Bool drawBlocks, const String& _txtFnOut);
	cSplitter(const String& _yuvFilenameIn, const V2ui& _frameSize, const UInt _chromaFormat, const UInt _BPS, const Float _skipThresh, const UInt _partThresh, const UInt _quantInterval, const V2ui& _maxBlockSize, const V2ui& _minBlockSize, Bool _useRects, Bool drawBlocks, const String& _txtFnOut);

	Void start(const UInt startFrame, const UInt numFrames);

	
private:
	//output txt
	String m_txtFnOut;
	ofstream m_TXT;
	UInt m_quantInterval;

	//split params
	Float m_skipThresh;
	UInt m_partThresh;
	V2ui m_maxBlockSize;
	V2ui m_minBlockSize;
	Bool m_useRects;

	//yuv input
	String m_yuvFnIn;
	UInt m_BPS;
	UInt m_chromaFormat;
	V2ui m_frameSize;
	cYUV<DepthPixelType> m_currentDepthMap;
	cYUV<DepthPixelType> m_previousDepthMap;
	UInt m_SF;
	UInt m_NOF;

	//depth data
	deque<cDepthBlock<DepthPixelType>> m_currentFrameBlocks;
	deque<cDepthBlock<DepthPixelType>> m_previousFrameBlocks;

	vector<cDepthBlock<DepthPixelType>> m_Vrects25_75;
	vector<cDepthBlock<DepthPixelType>> m_Vrects75_25;
	vector<cDepthBlock<DepthPixelType>> m_Vrects;
									  
	vector<cDepthBlock<DepthPixelType>> m_Hrects25_75;
	vector<cDepthBlock<DepthPixelType>> m_Hrects75_25;
	vector<cDepthBlock<DepthPixelType>> m_Hrects;

	vector<cDepthBlock<DepthPixelType>> m_squares;

	//optional output blocks 
	Bool m_drawBlocks;
	String m_yuvFnSplitBlocks = "v0_block_1920x1080_yuv420p10le.yuv";
	cYUV<DepthPixelType> m_depthMapBlocks;

	DepthPixelType m_MAX_Z;
	UInt m_numSplitLvl;

private:
	UInt quantUp(UInt val);
	UInt quantDn(UInt val);

	Bool split(const V2ui& blockSize, const V2ui& startPosition, const V2ui& endPosition, const UInt splitLevel = 0);
	Bool splitUsingRects(const V2ui& blockSizeToSplit, const V2ui& startPosition, const V2ui& endPosition, const UInt splitLevel);

	vector<cDepthBlock<DepthPixelType>> splitSquare(const V2ui& blockSizeToSplit, const V2ui& startPosition, const V2ui& endPosition, const UInt splitLevel);
	vector<cDepthBlock<DepthPixelType>> splitHorizontalRect(const V2ui& blockSizeToSplit, const V2ui& startPosition, const V2ui& endPosition, const V2ui& ratio, const UInt splitLevel);
	vector<cDepthBlock<DepthPixelType>> splitVerticalRect(const V2ui& blockSizeToSplit, const V2ui& startPosition, const V2ui& endPosition, const V2ui& ratio, const UInt splitLevel);

	Void skip();
	Bool canSkip(const cDepthBlock<DepthPixelType>& prevBlock, const cDepthBlock<DepthPixelType>& currBlock);
	Bool canSkip(const cDepthBlock<DepthPixelType>& currBlock);

	//drawing
	Void drawBlocksToYUV(UInt frame);
	Void DrawRect(const V2ui& posTL, const V2ui& posBR, DepthPixelType val);
	Void DrawLine(const V2ui& pos1, const V2ui& pos2, DepthPixelType val);
	Void Draw(const V2ui& pos, DepthPixelType val);
	Void FillRect(const V2ui& posTL, const V2ui& posBR, DepthPixelType val);

	//output data
	Void exportTXT(const UInt frame, const UInt numSplitLvl);
};

