#pragma once
#include "TypeDef.h"


enum class SPLIT_TYPE {
	NO_SPLIT,
	QUAD,
	V_SYM,
	V_ASYM_BIG,
	V_ASYM_SMALL,
	H_SYM,
	H_ASYM_BIG,
	H_ASYM_SMALL,
	UNDEFINED
};

template <class PixelType>
class cDepthBlock
{
public:
	cDepthBlock()
		: m_Zmin(-1), m_Zmax(0), m_positionTopLeft({ 0, 0 }), m_positionBottomRight({ 0, 0 }), m_size({ 0,0 }), m_splitLevel(-1), m_skipFlag(false)
	{
		m_splitType = SPLIT_TYPE::UNDEFINED;
		m_costVolume = -1;
	}

	cDepthBlock(const PixelType _Zmin, const PixelType _Zmax, const V2ui& _positionTopLeft, const V2ui& _positionBottomRight, const V2ui& _size, const UInt _splitLevel, SPLIT_TYPE _splitType)
		: m_Zmin(_Zmin), m_Zmax(_Zmax), m_positionTopLeft(_positionTopLeft), m_positionBottomRight(_positionBottomRight), m_size(_size), m_splitLevel(_splitLevel), m_splitType(_splitType), m_skipFlag(false)
	{
		m_costVolume = (m_Zmax - m_Zmin + 1) * m_size.x * m_size.y;
	}

private:
	PixelType m_Zmin;
	PixelType m_Zmax;

	V2ui m_positionTopLeft;
	V2ui m_positionBottomRight;
	V2ui m_size;
	UInt m_splitLevel;

	UInt m_costVolume;

	Bool m_skipFlag;

	SPLIT_TYPE m_splitType;

public:
	PixelType	getZmin()					const { return m_Zmin; }
	PixelType	getZmax()					const { return m_Zmax; }
	V2ui		getPositionTopLeft()		const { return m_positionTopLeft; }
	V2ui		getPositionBottomRight()	const { return m_positionBottomRight; }
	V2ui		getSize()					const { return m_size; }
	UInt		getSplitLevel()				const { return m_splitLevel; }
	UInt		getCostVolume()				const { return m_costVolume; }
	Bool		getSkipFlag()				const { return m_skipFlag; }
	SPLIT_TYPE	getSplitType()				const { return m_splitType; }

	Void		setSkipFlag(bool val)						{ m_skipFlag = val; }
	Void		setSplitType(const SPLIT_TYPE splitType)	{ m_splitType = splitType; }

	Bool operator == (const cDepthBlock<PixelType>& rhs) const {
		return (this->m_positionTopLeft == rhs.m_positionTopLeft
			&& this->m_positionBottomRight == rhs.m_positionBottomRight
			&& this->m_size == rhs.m_size
			&& this->m_splitLevel == rhs.m_splitLevel);
	}
};


