#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include "TypeDef.h"
#include "cYUV.h"
#include "cSplitter.h"

#define DEBUG 0

using namespace std;

//args: fnYuvIn fnTxtOut width height bps cs sf nof maxBS minBS skipThresh partThresh quantInterval useRects drawBlocksFlag
//TODO - parse filename

Void parseYUV(String& fname, UInt* width, UInt* height, UInt* BPS, UInt* CS);

int main(int argc, char* argv[])
{

#if(!DEBUG)
	if (argc < 15)
	{
		std::cout << "ERROR! \n provide: \n \tfnYuvIn fnTxtOut width height BPS CS startFrame NOF maxBS minBS skipThresh partThresh quantInterval useRectsFlag drawBlocksFlag" << std::endl;
		return 0;
	}
#endif

#if(DEBUG)

	

	string fnYuvIn = "C:/Data/TMIV/Experiment/TMIV9_G17_FEX/G17/E/QP5/TMIV_G17_E_QP5_tex_pv09_1920x1080_yuv420p10le.yuv";
	string fnTxtOut = "C:/Data/TMIV/Test/Fex9/Fex_tex_th0.7/G17/E/QP5/G17_E_FEATURES_pv09.txt";


	UInt W = 1920;
	UInt H = 1080;
	UInt BPS = 10;
	UInt CS = 420;
	UInt SF = 0;
	UInt NOF = 17;

	V2ui maxBlockSize(64, 64);
	V2ui minBlockSize(32, 32);

	Float skipThresh = 0.7; //% of max Z val
	UInt partThresh = LONG_MAX;
	UInt quantInterval = 4;
	Bool useRects = false;
	Bool drawBlocks = false;

#else

	string fnYuvIn = argv[1];
	string fnTxtOut = argv[2];

	UInt W = stoi(string(argv[3]));
	UInt H = stoi(string(argv[4]));
	UInt BPS = stoi(string(argv[5]));
	UInt CS = stoi(string(argv[6]));
	UInt SF = stoi(string(argv[7]));
	UInt NOF = stoi(string(argv[8]));

	V2ui maxBlockSize(stoi(string(argv[9])), stoi(string(argv[9])));
	V2ui minBlockSize(stoi(string(argv[10])), stoi(string(argv[10])));

	Float skipThresh = stof(string(argv[11])); 
	UInt partThresh = stoi(string(argv[12]));
	UInt quantInterval = stoi(string(argv[13]));

	UInt useRects = stoi(string(argv[14]));
	Bool drawBlocks = stoi(string(argv[15]));
#endif

	UInt splitLvlNum = maxBlockSize.x / minBlockSize.x;

	float perc = 0.0;
	if (BPS != 10)
	{
		perc = ((float)partThresh / (pow(2, 10) - 1)) * 100.0f;
		partThresh = ((pow(2, BPS) - 1) / 100) * perc;
	}


	cSplitter* splitter = new cSplitter(
		fnYuvIn, 
		{ W,H }, 
		CS, 
		BPS, 
		skipThresh, 
		partThresh, 
		quantInterval, 
		maxBlockSize, 
		minBlockSize, 
		useRects, 
		drawBlocks,
		fnTxtOut);

	splitter->start(SF, NOF);

	delete splitter;

	std::cout << "Done";
}

