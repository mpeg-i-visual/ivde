#ifndef __TYPEDEF_H__
#define __TYPEDEF_H__

#include <string>

typedef	void				Void;
typedef	bool				Bool;

typedef char				Char;
typedef unsigned char		UChar;
typedef	short				Short;
typedef	unsigned short		UShort;
typedef int					Int;
typedef unsigned int		UInt;

typedef long long			Int64;
typedef unsigned long long	UInt64;

typedef	float				Float;
typedef	double				Double;

typedef std::string			String;

typedef UShort				DepthPixelType;
typedef UChar				ImagePixelType;

#define MAX_TEXT_LENGTH		200

#define Max(x, y)                   ((x)>(y)?(x):(y))                                                 ///< max of (x, y)
#define Min(x, y)                   ((x)<(y)?(x):(y))                                                 ///< min of (x, y)

template <typename T>
struct Vec2
{
	Vec2() : x(0), y(0) {}
	Vec2(T _x, T _y) : x(_x), y(_y) {}
	T x;
	T y;

	bool operator== (const Vec2<T>& rhs) const
	{
		return (rhs.x == this->x && rhs.y == this->y);
	}

	Vec2 operator / (const T rhs) const
	{
		return Vec2<T>(this->x / rhs, this->y / rhs);
	}
};

typedef Vec2<Int> V2i;
typedef Vec2<UInt> V2ui;

#if (defined(WIN32) || defined(_WIN32) || defined(_WIN64))
#define X_SYSTEM_WINDOWS 1
#elif defined(__linux__)
#define X_SYSTEM_LINUX 1
#else
#define X_SYSTEM_UNKNOWN 0
#endif

#if X_SYSTEM_WINDOWS
#define xFtell64(FileStream)               _ftelli64(FileStream)
#define xFseek64(FileStream,Offset,Origin) _fseeki64(FileStream,Offset,Origin)
#elif X_SYSTEM_LINUX
#define _FILE_OFFSET_BITS 64
#define xFtell64(FileStream)               ftello(FileStream)
#define xFseek64(FileStream,Offset,Origin) fseeko(FileStream,Offset,Origin)
#else
#error Unrecognized platform
#endif

#endif