#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include "cSplitter.h"

cSplitter::cSplitter()
{
	
}

cSplitter::cSplitter(const String& _yuvFilenameIn, const Float _skipThresh, const UInt _partThresh, const UInt _quantInterval, const V2ui& _maxBlockSize, const V2ui& _minBlockSize, Bool _useRects, Bool _drawBlocks, const String& _txtFnOut)
	: m_yuvFnIn(_yuvFilenameIn), m_skipThresh(_skipThresh), m_partThresh(_partThresh), m_quantInterval(_quantInterval), m_maxBlockSize(_maxBlockSize), m_minBlockSize(_minBlockSize), m_useRects(_useRects), m_drawBlocks(_drawBlocks), m_txtFnOut(_txtFnOut)
{
	m_TXT.open(m_txtFnOut);

	m_currentFrameBlocks.clear();
	m_previousFrameBlocks.clear();

	m_currentDepthMap.init(m_frameSize.x, m_frameSize.y, m_BPS, m_chromaFormat);
	m_previousDepthMap.init(m_frameSize.x, m_frameSize.y, m_BPS, m_chromaFormat);

	m_depthMapBlocks.init(m_frameSize.x, m_frameSize.y, m_BPS, m_chromaFormat);

	m_MAX_Z = pow(2, m_BPS) - 1;

	for (UInt p = 0; p < m_depthMapBlocks.m_uiImSize; p++) {
		m_depthMapBlocks.m_atY[p] = 0;
	}
	for (UInt p = 0; p < m_depthMapBlocks.m_uiImSizeUV; p++) {
		m_depthMapBlocks.m_atU[p] = m_MAX_Z / 2;
		m_depthMapBlocks.m_atV[p] = m_MAX_Z / 2;
	}

	m_yuvFnSplitBlocks = m_yuvFnIn;
	Int n = m_yuvFnSplitBlocks.find("_" + to_string(m_frameSize.x));
	m_yuvFnSplitBlocks.insert(n, "_split_mesh");

	std::reverse(m_yuvFnSplitBlocks.begin(), m_yuvFnSplitBlocks.end());
	n = m_yuvFnSplitBlocks.find("/");
	if (n < m_yuvFnSplitBlocks.length()) m_yuvFnSplitBlocks.erase(n, m_yuvFnSplitBlocks.length());
	std::reverse(m_yuvFnSplitBlocks.begin(), m_yuvFnSplitBlocks.end());


}

cSplitter::cSplitter(const String& _yuvFilenameIn, const V2ui& _frameSize, const UInt _chromaFormat, const UInt _BPS, const Float _skipThresh, const UInt _partThresh, const UInt _quantInterval, const V2ui& _maxBlockSize, const V2ui& _minBlockSize, Bool _useRects, Bool _drawBlocks, const String& _txtFnOut)
	: m_yuvFnIn(_yuvFilenameIn), m_frameSize(_frameSize), m_chromaFormat(_chromaFormat), m_BPS(_BPS), m_skipThresh(_skipThresh), m_partThresh(_partThresh), m_quantInterval(_quantInterval), m_maxBlockSize(_maxBlockSize), m_minBlockSize(_minBlockSize), m_useRects(_useRects), m_drawBlocks(_drawBlocks), m_txtFnOut(_txtFnOut)
{
	m_TXT.open(m_txtFnOut);

	m_currentFrameBlocks.clear();
	m_previousFrameBlocks.clear();

	m_currentDepthMap.init(m_frameSize.x, m_frameSize.y, m_BPS, m_chromaFormat);
	m_previousDepthMap.init(m_frameSize.x, m_frameSize.y, m_BPS, m_chromaFormat);

	m_depthMapBlocks.init(m_frameSize.x, m_frameSize.y, m_BPS, m_chromaFormat);

	m_MAX_Z = pow(2, m_BPS) - 1;
	//init for drawing
	for (UInt p = 0; p < m_depthMapBlocks.m_uiImSize; p++) {
		m_depthMapBlocks.m_atY[p] = 0;
	}
	for (UInt p = 0; p < m_depthMapBlocks.m_uiImSizeUV; p++) {
		m_depthMapBlocks.m_atU[p] = m_MAX_Z / 2;
		m_depthMapBlocks.m_atV[p] = m_MAX_Z / 2;
	}

	m_yuvFnSplitBlocks = m_yuvFnIn;
	Int n = m_yuvFnSplitBlocks.find("_" + to_string(m_frameSize.x));
	m_yuvFnSplitBlocks.insert(n, "_split_mesh");

	std::reverse(m_yuvFnSplitBlocks.begin(), m_yuvFnSplitBlocks.end());
	n = m_yuvFnSplitBlocks.find("/");
	if(n < m_yuvFnSplitBlocks.length()) m_yuvFnSplitBlocks.erase(n, m_yuvFnSplitBlocks.length());
	std::reverse(m_yuvFnSplitBlocks.begin(), m_yuvFnSplitBlocks.end());
}

Void cSplitter::start(const UInt startFrame, const UInt numFrames)
{
	m_SF = startFrame;
	m_NOF = numFrames;

	m_TXT <<
		"yuv=" << m_yuvFnIn << " " <<
		m_frameSize.x << "x" << m_frameSize.y << " " <<
		"bitdepth=" << m_BPS << " " <<
		"blksz=" << m_maxBlockSize.x << " " <<
		"sf=" << m_SF << " " <<
		"nf=" << m_NOF << " " <<
		"skipthresh=" << m_skipThresh << "% " <<
		"partthresh=" << m_partThresh << " " <<
		"q=" << m_quantInterval << " " <<
		"userects=" << m_useRects << std::endl;

	m_numSplitLvl = m_maxBlockSize.x / m_minBlockSize.x;

    for(UInt f = startFrame; f < startFrame + numFrames; f++) {
    
        m_currentDepthMap.frameReader(m_yuvFnIn, f);
        this->split(m_maxBlockSize, V2ui(0, 0), m_frameSize);

        if (f > startFrame) this->skip();
        
		exportTXT(f, m_numSplitLvl);

		if(m_drawBlocks) drawBlocksToYUV(f);

		m_previousFrameBlocks = m_currentFrameBlocks;
		m_previousDepthMap.frameReader(m_yuvFnIn, f);

		m_currentFrameBlocks.clear();

		std::cout << "Frame " << f << " of " << startFrame + numFrames << std::endl;
    }

	m_currentDepthMap.~cYUV();
	m_previousDepthMap.~cYUV();

	m_TXT.close();

	return;
}

UInt cSplitter::quantUp(UInt val)
{
	if (val == m_MAX_Z) return val;
	return ((val + m_quantInterval - 1) / m_quantInterval) * m_quantInterval;
}

UInt cSplitter::quantDn(UInt val)
{
	return (val / m_quantInterval) * m_quantInterval;
}

//currently there is no recursion, but it is possible to use it, eg. split from 64x64 down to 8x8
Bool cSplitter::split(const V2ui& blockSize, const V2ui& startPosition, const V2ui& endPosition, const UInt splitLevel)
{
    if (blockSize.x < m_minBlockSize.x || blockSize.y < m_minBlockSize.y || splitLevel == m_numSplitLvl) return false;

    DepthPixelType Zmin = -1;
    DepthPixelType tempZmin = 0;

    DepthPixelType Zmax = 0;
    DepthPixelType tempZmax = 0;

    UInt width = m_frameSize.x;

    for (UInt blkY = startPosition.y; blkY < endPosition.y; blkY += blockSize.y) {
        for (UInt blkX = startPosition.x; blkX < endPosition.x; blkX += blockSize.x) {

			UInt x = 0, y = 0;
			Zmin = -1; tempZmin = 0; Zmax = 0; tempZmax = 0;
            for (y = blkY; y < blkY + blockSize.y && y < m_frameSize.y; y++) {
                for (x = blkX; x < blkX + blockSize.x && x < m_frameSize.x; x++) {

                    tempZmin = m_currentDepthMap.m_atY[x + width * y];
                    tempZmax = m_currentDepthMap.m_atY[x + width * y];

                    Zmin = std::min(Zmin, tempZmin);
                    Zmax = std::max(Zmax, tempZmax);
                }
            }


			if ((Zmax - Zmin) > m_partThresh) { //split

				if (m_useRects) {
					if (!splitUsingRects(blockSize, { blkX, blkY }, { x,y }, splitLevel + 1)) {
						m_squares = splitSquare(blockSize, { blkX, blkY }, { x,y }, splitLevel + 1);
						if (!m_squares.empty()) { //fix
							for (const auto& sq : m_squares)
								m_currentFrameBlocks.push_back(sq);
						}
						else {
							m_currentFrameBlocks.emplace_back(cDepthBlock<DepthPixelType>{ Zmin, Zmax, V2ui(blkX, blkY), V2ui(x, y), blockSize, splitLevel, SPLIT_TYPE::NO_SPLIT });
						}
					}
				}
				else {
					m_squares = splitSquare(blockSize, { blkX, blkY }, { x,y }, splitLevel + 1);
					if (!m_squares.empty()) { //fix
						for (const auto& sq : m_squares)
							m_currentFrameBlocks.push_back(sq);
					}
					else {
						m_currentFrameBlocks.emplace_back(cDepthBlock<DepthPixelType>{ Zmin, Zmax, V2ui(blkX, blkY), V2ui(x, y), blockSize, splitLevel, SPLIT_TYPE::NO_SPLIT });
					}
				}
			}
			else {
				m_currentFrameBlocks.emplace_back(cDepthBlock<DepthPixelType>{ Zmin, Zmax, V2ui(blkX, blkY), V2ui(x, y), blockSize, splitLevel, SPLIT_TYPE::NO_SPLIT });
			}

			
        }
    }
	return true;
}

Bool cSplitter::splitUsingRects(const V2ui& blockSizeToSplit, const V2ui& startPosition, const V2ui& endPosition, const UInt splitLevel)
{
	if (splitLevel == m_numSplitLvl) return false;

	m_Hrects = splitHorizontalRect(blockSizeToSplit, startPosition, endPosition, { 50,50 }, splitLevel);
	if (!m_Hrects.empty()) {
		m_Hrects[0].setSplitType(SPLIT_TYPE::H_SYM);
		m_Hrects[1].setSplitType(SPLIT_TYPE::H_SYM);
	}

	m_Hrects25_75 = splitHorizontalRect(blockSizeToSplit, startPosition, endPosition, { 25,75 }, splitLevel);
	if (!m_Hrects25_75.empty()) {
		m_Hrects25_75[0].setSplitType(SPLIT_TYPE::H_ASYM_SMALL);
		m_Hrects25_75[1].setSplitType(SPLIT_TYPE::H_ASYM_BIG);
	}

	m_Hrects75_25 = splitHorizontalRect(blockSizeToSplit, startPosition, endPosition, { 75,25 }, splitLevel);
	if (!m_Hrects75_25.empty()) {
		m_Hrects75_25[0].setSplitType(SPLIT_TYPE::H_ASYM_BIG);
		m_Hrects75_25[1].setSplitType(SPLIT_TYPE::H_ASYM_SMALL);
	}

	m_Vrects = splitVerticalRect(blockSizeToSplit, startPosition, endPosition, { 50,50 }, splitLevel);
	if (!m_Vrects.empty()) {
		m_Vrects[0].setSplitType(SPLIT_TYPE::V_SYM);
		m_Vrects[1].setSplitType(SPLIT_TYPE::V_SYM);
	}

	m_Vrects25_75 = splitVerticalRect(blockSizeToSplit, startPosition, endPosition, { 25,75 }, splitLevel);
	if (!m_Vrects25_75.empty()) {
		m_Vrects25_75[0].setSplitType(SPLIT_TYPE::V_ASYM_SMALL);
		m_Vrects25_75[1].setSplitType(SPLIT_TYPE::V_ASYM_BIG);
	}

	m_Vrects75_25 = splitVerticalRect(blockSizeToSplit, startPosition, endPosition, { 75,25 }, splitLevel);
	if (!m_Vrects75_25.empty()) {
		m_Vrects75_25[0].setSplitType(SPLIT_TYPE::V_ASYM_BIG);
		m_Vrects75_25[1].setSplitType(SPLIT_TYPE::V_ASYM_SMALL);
	}

	vector<vector<cDepthBlock<DepthPixelType>>> splitVariants{
		m_Hrects,
		m_Hrects25_75,
		m_Hrects75_25,
		m_Vrects,
		m_Vrects25_75,
		m_Vrects75_25,
	};

	splitVariants.erase(std::remove_if(splitVariants.begin(), splitVariants.end(),
		[&](const auto& variant) {
			if (variant.empty()) return true;

			int k = 0;
			for (const auto& block : variant) {
				if ((block.getZmax() - block.getZmin()) > m_partThresh)
					k++;
			}
			if (k == 2) return true;

			return false;
		}), splitVariants.end());

	std::sort(splitVariants.begin(), splitVariants.end(),
		[](const auto& V1, const auto& V2) -> bool {

			UInt costVolumeV1 = 0, costVolumeV2 = 0;

			for (auto& b : V1)
				costVolumeV1 += b.getCostVolume();

			for (auto& b : V2)
				costVolumeV2 += b.getCostVolume();

			return costVolumeV1 < costVolumeV2;
		});

	if (splitVariants.empty()) 
		return false;
	else {
		vector<cDepthBlock<DepthPixelType>> variant = splitVariants.front();

		for (auto& v : variant) {
			m_currentFrameBlocks.push_back(v);
		}
		return true;
	}
}

vector<cDepthBlock<DepthPixelType>> cSplitter::splitSquare(const V2ui& blockSizeToSplit, const V2ui& startPosition, const V2ui& endPosition, const UInt splitLevel)
{
	V2ui blockSize{ blockSizeToSplit.x / 2, blockSizeToSplit.y / 2 };
	vector<cDepthBlock<DepthPixelType>> result(0);

	if (startPosition.x + blockSize.x >= m_frameSize.x || startPosition.y + blockSize.y >= m_frameSize.y) return result; //fix


	DepthPixelType Zmin = -1;
	DepthPixelType tempZmin = 0;

	DepthPixelType Zmax = 0;
	DepthPixelType tempZmax = 0;

	UInt width = m_frameSize.x;

	for (UInt blkY = startPosition.y; blkY < endPosition.y; blkY+=blockSize.y) {
		for (UInt blkX = startPosition.x; blkX < endPosition.x; blkX+=blockSize.x) {
			UInt x = 0, y = 0;
			for (y = blkY; y < blkY + blockSize.y && y < m_frameSize.y; y++) {
				for (x = blkX; x < blkX + blockSize.x && x < m_frameSize.x; x++) {

					tempZmin = m_currentDepthMap.m_atY[x + width * y];
					tempZmax = m_currentDepthMap.m_atY[x + width * y];

					Zmin = std::min(Zmin, tempZmin);
					Zmax = std::max(Zmax, tempZmax);
				}
			}
			result.emplace_back(cDepthBlock<DepthPixelType>( Zmin, Zmax, { blkX, blkY }, { x,y }, blockSize, splitLevel, SPLIT_TYPE::QUAD));

			Zmin = -1; tempZmin = 0; Zmax = 0; tempZmax = 0;
		}
	}
	return result;
}

vector<cDepthBlock<DepthPixelType>> cSplitter::splitHorizontalRect(const V2ui& blockSizeToSplit, const V2ui& startPosition, const V2ui& endPosition, const V2ui& ratio, const UInt splitLevel)
{
	vector<cDepthBlock<DepthPixelType>> result(0);
	DepthPixelType Zmin = -1, tempZmin = 0, Zmax = 0, tempZmax = 0;
	UInt width = m_frameSize.x;
	V2ui blockSize{ blockSizeToSplit.x, (UInt)(((float)blockSizeToSplit.y / 100.0f) * (float)ratio.x) };

	if (startPosition.x + blockSize.x >= m_frameSize.x || startPosition.y + blockSize.y >= m_frameSize.y) return result; //fix


	UInt X = 0, Y = 0;
	for (Y = startPosition.y; Y < startPosition.y + blockSize.y && Y < m_frameSize.y; Y++) {
		for (X = startPosition.x; X < startPosition.x + blockSize.x && X < m_frameSize.x; X++) {
			tempZmin = m_currentDepthMap.m_atY[X + width * Y];
			tempZmax = m_currentDepthMap.m_atY[X + width * Y];

			Zmin = std::min(Zmin, tempZmin);
			Zmax = std::max(Zmax, tempZmax);
		}
	}
	result.emplace_back(cDepthBlock<DepthPixelType>(Zmin, Zmax, startPosition, { X, Y }, blockSize, splitLevel, SPLIT_TYPE::UNDEFINED));

	Zmin = -1; tempZmin = 0; Zmax = 0; tempZmax = 0;
	UInt stride = blockSize.y;
	blockSize.y = (UInt)(((float)blockSizeToSplit.y / 100.0f) * (float)ratio.y);

	for (Y = startPosition.y + stride; Y < startPosition.y + stride + blockSize.y && Y < m_frameSize.y; Y++) {
		for (X = startPosition.x; X < startPosition.x + blockSize.x && X < m_frameSize.x; X++) {
			tempZmin = m_currentDepthMap.m_atY[X + width * Y];
			tempZmax = m_currentDepthMap.m_atY[X + width * Y];

			Zmin = std::min(Zmin, tempZmin);
			Zmax = std::max(Zmax, tempZmax);
		}
	}
	result.emplace_back(cDepthBlock<DepthPixelType>(Zmin, Zmax, {startPosition.x, startPosition.y + stride}, { X, Y }, blockSize, splitLevel, SPLIT_TYPE::UNDEFINED));

	return result;
}

vector<cDepthBlock<DepthPixelType>> cSplitter::splitVerticalRect(const V2ui& blockSizeToSplit, const V2ui& startPosition, const V2ui& endPosition, const V2ui& ratio, const UInt splitLevel)
{
	vector<cDepthBlock<DepthPixelType>> result(0);
	DepthPixelType Zmin = -1, tempZmin = 0, Zmax = 0, tempZmax = 0;
	UInt width = m_frameSize.x;
	V2ui blockSize{ (UInt)(((float)blockSizeToSplit.x / 100.0f) * (float)ratio.x) , blockSizeToSplit.y};

	if (startPosition.x + blockSize.x >= m_frameSize.x || startPosition.y + blockSize.y >= m_frameSize.y) return result; //fix

	UInt X = 0, Y = 0;
	for (Y = startPosition.y; Y < startPosition.y + blockSize.y && Y < m_frameSize.y; Y++) {
		for (X = startPosition.x; X < startPosition.x + blockSize.x && X < m_frameSize.x; X++) {
			tempZmin = m_currentDepthMap.m_atY[X + width * Y];
			tempZmax = m_currentDepthMap.m_atY[X + width * Y];

			Zmin = std::min(Zmin, tempZmin);
			Zmax = std::max(Zmax, tempZmax);
		}
	}
	result.emplace_back(cDepthBlock<DepthPixelType>(Zmin, Zmax, startPosition, { X, Y }, blockSize, splitLevel, SPLIT_TYPE::UNDEFINED));

	Zmin = -1; tempZmin = 0; Zmax = 0;  tempZmax = 0;
	UInt stride = blockSize.x;
	blockSize.x = (UInt)(((float)blockSizeToSplit.x / 100.0f) * (float)ratio.y);

	for (Y = startPosition.y; Y < startPosition.y + blockSize.y && Y < m_frameSize.y; Y++) {
		for (X = startPosition.x + stride; X < startPosition.x + stride + blockSize.x && X < m_frameSize.x; X++) {
			tempZmin = m_currentDepthMap.m_atY[X + width * Y];
			tempZmax = m_currentDepthMap.m_atY[X + width * Y];

			Zmin = std::min(Zmin, tempZmin);
			Zmax = std::max(Zmax, tempZmax);
		}
	}
	result.emplace_back(cDepthBlock<DepthPixelType>(Zmin, Zmax, {startPosition.x + stride, startPosition.y}, { X, Y }, blockSize, splitLevel, SPLIT_TYPE::UNDEFINED));

	return result;
}

Void cSplitter::skip()
{
	deque<cDepthBlock<DepthPixelType>>::iterator itPrev = m_previousFrameBlocks.begin();
	deque<cDepthBlock<DepthPixelType>>::iterator itStart;
	deque<cDepthBlock<DepthPixelType>>::iterator itFind;
	cDepthBlock<DepthPixelType> prevBlock;

	for (cDepthBlock<DepthPixelType>& currBlock : m_currentFrameBlocks) {
		
		itStart = itPrev;
		itFind = std::find(itStart, m_previousFrameBlocks.end(), currBlock);
		
		if (itFind == end(m_previousFrameBlocks)) {
			
			itFind = itStart;
			while (!(*itFind == currBlock) && !(itFind == begin(m_previousFrameBlocks)))
				itFind--;

			itFind++;
		}

		itPrev = itFind;

		if(canSkip(*itPrev, currBlock)) currBlock.setSkipFlag(true);
		else currBlock.setSkipFlag(false);
	}

	/*for (cDepthBlock<DepthPixelType>& currBlock : m_currentFrameBlocks) {
		if (canSkip(currBlock))
			currBlock.setSkipFlag(true);
		else
			currBlock.setSkipFlag(false);
	}*/
}

Bool cSplitter::canSkip(const cDepthBlock<DepthPixelType>& currBlock)
{
	DepthPixelType prevZmin = -1;
	DepthPixelType prevZmax = 0;

	DepthPixelType tempZmin = 0;
	DepthPixelType tempZmax = 0;

	UInt width = m_frameSize.x;

	for (UInt y = currBlock.getPositionTopLeft().y; y < currBlock.getPositionBottomRight().y; y++) {
		for (UInt x = currBlock.getPositionTopLeft().x; x < currBlock.getPositionBottomRight().x; x++) {

			tempZmin = m_previousDepthMap.m_atY[x + width * y];
			tempZmax = m_previousDepthMap.m_atY[x + width * y];

			prevZmin = std::min(prevZmin, tempZmin);
			prevZmax = std::max(prevZmax, tempZmax);
		}
	}

	if (currBlock.getZmin() < prevZmin || currBlock.getZmax() > prevZmax) return false;

	DepthPixelType ZminDiff = currBlock.getZmin() - prevZmin;
	DepthPixelType ZmaxDiff = prevZmax - currBlock.getZmax();

	UInt Z_SAD = 0;
	UInt cnt = 0;
	for (UInt y = currBlock.getPositionTopLeft().y; y < currBlock.getPositionBottomRight().y; y++) {
		for (UInt x = currBlock.getPositionTopLeft().x; x < currBlock.getPositionBottomRight().x; x++) {
			//if (x >= m_frameSize.x || y >= m_frameSize.y) continue;
			Z_SAD += abs(m_currentDepthMap.m_atY[x + width * y] - m_previousDepthMap.m_atY[x + width * y]);
			cnt++;
		}
	}

	Float ZminDiffPerc = ((Float)ZminDiff / (Float)m_MAX_Z) * 100.0f;
	Float ZmaxDiffPerc = ((Float)ZmaxDiff / (Float)m_MAX_Z) * 100.0f;
	Float Z_SAD_Perc = ((Float)Z_SAD / (Float)cnt / (Float)m_MAX_Z) * 100.0f;

	if (ZminDiffPerc >= m_skipThresh || ZmaxDiffPerc >= m_skipThresh || Z_SAD_Perc >= m_skipThresh)
		return false;
	else
		return true;
}

Bool cSplitter::canSkip(const cDepthBlock<DepthPixelType>& prevBlock, const cDepthBlock<DepthPixelType>& currBlock)
{
	//if (currBlock.getZmin() < prevBlock.getZmin() || currBlock.getZmax() > prevBlock.getZmax()) return false;

	DepthPixelType ZminDiff = currBlock.getZmin() - prevBlock.getZmin();
	DepthPixelType ZmaxDiff = prevBlock.getZmax() - currBlock.getZmax();

	UInt width = m_frameSize.x;
	UInt Z_SAD = 0;
	UInt cnt = 0;
	for (UInt y = currBlock.getPositionTopLeft().y; y < currBlock.getPositionBottomRight().y; y++) {
		for (UInt x = currBlock.getPositionTopLeft().x; x < currBlock.getPositionBottomRight().x; x++) {
			//if (x >= m_frameSize.x || y >= m_frameSize.y) continue;
			Z_SAD += abs(m_currentDepthMap.m_atY[x + width * y] - m_previousDepthMap.m_atY[x + width * y]);
			cnt++;
		}
	}

	Float ZminDiffPerc = ((Float)ZminDiff / (Float)m_MAX_Z) * 100.0f;
	Float ZmaxDiffPerc = ((Float)ZmaxDiff / (Float)m_MAX_Z) * 100.0f;
	Float Z_SAD_Perc = ((Float)Z_SAD / (Float)cnt / (Float)m_MAX_Z) * 100.0f;

	//if (ZminDiffPerc >= m_skipThresh || ZmaxDiffPerc >= m_skipThresh || Z_SAD_Perc >= m_skipThresh) 
	if (Z_SAD_Perc >= m_skipThresh)
		return false;
	else 
		return true;
}

Void cSplitter::drawBlocksToYUV(UInt frame)
{
	//reset canvas
	for (UInt i = 0; i < m_depthMapBlocks.m_uiImSize; i++)
		m_depthMapBlocks.m_atY[i] = 0;

    for (const cDepthBlock<DepthPixelType>& block : m_currentFrameBlocks)
    {
        if (block.getSkipFlag() == 1) {
			DrawRect(block.getPositionTopLeft(), block.getPositionBottomRight(), m_MAX_Z);
			//DrawLine(block.getPositionTopLeft(), block.getPositionBottomRight(), m_MAX_Z);
			FillRect(block.getPositionTopLeft(), block.getPositionBottomRight(), m_MAX_Z / 2);
        }
		else {
			DrawRect(block.getPositionTopLeft(), block.getPositionBottomRight(), m_MAX_Z);
		}
    }

	m_depthMapBlocks.frameWriter(m_yuvFnSplitBlocks, frame);
}

Void cSplitter::DrawRect(const V2ui& posTL, const V2ui& posBR, DepthPixelType val)
{
	DrawLine(posTL, { posBR.x, posTL.y }, val);
	DrawLine({ posBR.x, posTL.y }, posBR, val);
	DrawLine({ posTL.x, posBR.y }, posBR, val);
	DrawLine(posTL, { posTL.x, posBR.y }, val);
}

Void cSplitter::DrawLine(const V2ui& pos1, const V2ui& pos2, DepthPixelType val)
{
	Int dx, dy;
	UInt x, y, x2, x1, y2, y1;
	x1 = pos1.x; x2 = pos2.x; y1 = pos1.y; y2 = pos2.y;
	dx = x2 - x1; dy = y2 - y1;

	if (dx == 0) { // Line is vertical
		if (y2 < y1) std::swap(y1, y2);
		for (y = y1; y <= y2; y++)
			Draw({ x1, y }, val);
		return;
	}

	if (dy == 0) { // Line is horizontal
		if (x2 < x1) std::swap(x1, x2);
		for (x = x1; x <= x2; x++) 
			Draw({ x, y1 }, val);
		return;
	}
}

Void cSplitter::Draw(const V2ui& pos, DepthPixelType val)
{
	if (pos.x >= m_frameSize.x || pos.y >= m_frameSize.y || pos.x == 0 || pos.y == 0) return;
    m_depthMapBlocks.m_atY[pos.x + m_frameSize.x * pos.y] = val;
}


Void cSplitter::FillRect(const V2ui& posTL, const V2ui& posBR, DepthPixelType val)
{
	for (UInt y = posTL.y + 1; y < posBR.y; y++) {
		for (UInt x = posTL.x + 1; x < posBR.x; x++) {
			Draw({ x,y }, val);
		}
	}
}

Void cSplitter::exportTXT(const UInt frame, const UInt numSplitLvl)
{
	cDepthBlock<DepthPixelType> block;
	stringstream ss;

	if (numSplitLvl == 2) { // current syntax
		for (auto blockIt = m_currentFrameBlocks.begin(); blockIt != m_currentFrameBlocks.end(); ) {


			block = *blockIt;
			ss.str("");

			if (block.getSplitLevel() == 0) {
				m_TXT <<
					"f" << std::setfill('0') << std::setw(2) << frame << " " <<
					"y" << std::setfill('0') << std::setw(4) << block.getPositionTopLeft().y << " " <<
					"x" << std::setfill('0') << std::setw(4) << block.getPositionTopLeft().x << " " <<
					"split_flag=0" << " ";
				if (block.getSkipFlag()) {
					m_TXT << "skip_flag=1" << "\n";
				}
				else {
					m_TXT <<
						"skip_flag=0" << " " <<
						"zmin=" << quantDn(block.getZmin()) << " " <<
						"zmax=" << quantUp(block.getZmax()) << "\n";
				}
				blockIt++;
				continue;
			}

			if (block.getSplitLevel() == 1) {
				m_TXT <<
					"f" << std::setfill('0') << std::setw(2) << frame << " " <<
					"y" << std::setfill('0') << std::setw(4) << block.getPositionTopLeft().y << " " <<
					"x" << std::setfill('0') << std::setw(4) << block.getPositionTopLeft().x << " ";
					

				if (block.getSplitType() == SPLIT_TYPE::QUAD) {
					ss <<
						"split_flag=1" << " " <<
						"quad_split_flag=1" << " ";
					UInt skips = 0;
					for (UInt n = 0; n < 4; n++) {
						if (block.getSkipFlag()) {
							ss << "[ skip_flag=1 ]" << " ";
							skips++;
						}
						else {
							ss << 
								"[ skip_flag=0" << " " <<
								"zmin=" << quantDn(block.getZmin()) << " " <<
								"zmax=" << quantUp(block.getZmax()) << " ] ";
						}
						if(n < 3)
							block = *(++blockIt);
					}
					if (skips == 4)
						m_TXT << "split_flag=0" << " " << "skip_flag=1\n";
					else
						m_TXT << ss.str() << "\n";

					blockIt++;
					continue;
				}
				
				if (block.getSplitType() == SPLIT_TYPE::H_SYM || //orientation 0
					block.getSplitType() == SPLIT_TYPE::H_ASYM_BIG ||
					block.getSplitType() == SPLIT_TYPE::H_ASYM_SMALL) {

					ss <<
						"split_flag=1" << " " <<
						"quad_split_flag=0" << " " <<
						"split_orientation_flag=0" << " ";

					if (block.getSplitType() == SPLIT_TYPE::H_SYM) { //symmetry 1
						ss << "split_symmetry_flag=1" << " ";
						UInt skips = 0;
						for (UInt n = 0; n < 2; n++) {
							if (block.getSkipFlag()) {
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}
							if (n < 1)
								block = *(++blockIt);
						}
						if (skips == 2)
							m_TXT << "split_flag=0" << " " << "skip_flag=1\n";
						else
							m_TXT << ss.str() << "\n";
						blockIt++;
						continue;
					}
					else { //symmetry 0
						ss << "split_symmetry_flag=0" << " ";
						UInt skips = 0;
						if (block.getSplitType() == SPLIT_TYPE::H_ASYM_SMALL) {
							
							ss << "split_first_block_bigger=0" << " ";
							if (block.getSkipFlag()) {
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}

							block = *(++blockIt);

							if (block.getSkipFlag()) { //bigger
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}

							if (skips == 2)
								m_TXT << "split_flag=0" << " " << "skip_flag=1\n";
							else
								m_TXT << ss.str() << "\n";

							blockIt++;
							continue;
						}
						else {
							ss << "split_first_block_bigger=1" << " ";
							if (block.getSkipFlag()) {
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}

							block = *(++blockIt);

							if (block.getSkipFlag()) {
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}
							if (skips == 2)
								m_TXT << "split_flag=0" << " " << "skip_flag=1\n";
							else
								m_TXT << ss.str() << "\n";
							
							blockIt++;
							continue;
						}
					}
				}
				if (block.getSplitType() == SPLIT_TYPE::V_SYM || // orientation 1
					block.getSplitType() == SPLIT_TYPE::V_ASYM_BIG ||
					block.getSplitType() == SPLIT_TYPE::V_ASYM_SMALL) {

					ss <<
						"split_flag=1" << " " <<
						"quad_split_flag=0" << " " <<
						"split_orientation_flag=1" << " ";

					if (block.getSplitType() == SPLIT_TYPE::V_SYM) { //symmetry 1
						ss << "split_symmetry_flag=1" << " ";
						UInt skips = 0;
						for (UInt n = 0; n < 2; n++) {
							if (block.getSkipFlag()) {
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss << 
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}
							if (n < 1)
								block = *(++blockIt);
						}
						if (skips == 2)
							m_TXT << "split_flag=0" << " " << "skip_flag=1\n";
						else
							m_TXT << ss.str() << "\n";

						blockIt++;
						continue;
					}
					else { //symmetry 0
						ss << "split_symmetry_flag=0" << " ";
						UInt skips = 0;
						if (block.getSplitType() == SPLIT_TYPE::V_ASYM_SMALL) {

							ss << "split_first_block_bigger=0" << " ";
							if (block.getSkipFlag()) {
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}

							block = *(++blockIt);

							if (block.getSkipFlag()) { //bigger
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}
							if (skips == 2)
								m_TXT << "split_flag=0" << " " << "skip_flag=1\n";
							else
								m_TXT << ss.str() << "\n";

							blockIt++;
							continue;
						}
						else {
							ss << "split_first_block_bigger=1" << " ";
							if (block.getSkipFlag()) {
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}

							block = *(++blockIt);

							if (block.getSkipFlag()) { //smaller
								ss << "[ skip_flag=1 ]" << " ";
								skips++;
							}
							else {
								ss <<
									"[ skip_flag=0" << " " <<
									"zmin=" << quantDn(block.getZmin()) << " " <<
									"zmax=" << quantUp(block.getZmax()) << " ] ";
							}
							if (skips == 2)
								m_TXT << "split_flag=0" << " " << "skip_flag=1\n";
							else
								m_TXT << ss.str() << "\n";
							blockIt++;
							continue;
						}
					}
				}
			}
		}
	}
}
