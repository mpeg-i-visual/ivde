//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/
	 

#include "cBlockMatcher.h"

Void cBlockMatcher::init(cConfig &cfg){
    cPixelMatcher::init(cfg);

    m_uiMatchingBlockSize = cfg.m_uiMatchingBlockSize;

    return;
}

Double cBlockMatcher::matchingError(cConfig &cfg, cArray<cYUV<ImagePixelType>*> &rapcInputView, UInt uiCam, UInt uiTargetCam, Int uiX, Int uiY, Int uiXInTargetCam, Int uiYInTargetCam) {
    Int iHalfBlockWidth = m_uiMatchingBlockSize >> 1;
    Int iHalfBlockHeight = m_uiMatchingBlockSize >> 1;

    Double dMatchingError = 0.0;
    UInt uiNumOfUsedPts = 0;
    for (Int ibX = -iHalfBlockWidth; ibX <= iHalfBlockWidth; ibX++){
        for (Int ibY = -iHalfBlockHeight; ibY <= iHalfBlockHeight; ibY++){
            if( (uiX+ibX)>=0 && (uiX+ibX)<= Int(cfg.getFileInputViewWidth(uiCam)) &&
                (uiY+ibY)>=0 && (uiY+ibY)<= Int(cfg.getFileInputViewHeight(uiCam)) &&
                (uiXInTargetCam+ibX)>=0 && (uiXInTargetCam+ibX)<= Int(cfg.getFileInputViewWidth(uiTargetCam)) &&
                (uiYInTargetCam+ibY)>=0 && (uiYInTargetCam+ibY)<= Int(cfg.getFileInputViewHeight(uiTargetCam))){
                    dMatchingError += cPixelMatcher::matchingError(cfg, rapcInputView, uiCam, uiTargetCam, (uiX+ibX), (uiY+ibY), (uiXInTargetCam+ibX), (uiYInTargetCam+ibY));
                    uiNumOfUsedPts++;
            }
        }
    }

    if (uiNumOfUsedPts == 0)return ((1 << cfg.m_auiBitDepthColor[uiCam]) - 1);
    return dMatchingError / (Double)uiNumOfUsedPts;

}