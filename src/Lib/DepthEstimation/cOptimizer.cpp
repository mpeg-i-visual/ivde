//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/
	 

#include "cOptimizer.h"
#include "../Common/cArray.h"
#include <cmath>

using namespace std;

cOptimizer::cOptimizer() {
    return;
}

cOptimizer::~cOptimizer() {

}

int compare(const void * a, const void * b)
{
    return (*(Double*)a - *(Double*)b);
}

Void cOptimizer::init(cConfig &cfg, cMatcher *pcMatcher, cArray<cCamParams<Double>*> &rapcCameraParameters) {

    m_uiNumberOfCameras = cfg.m_uiNumberOfCameras;
    m_uiCenterCam = cfg.m_uiCenterCam;
    m_uiStartFrame = cfg.m_uiStartFrame;

    m_dSmoothingCoeff = cfg.m_dSmoothingCoefficient*(pow(2, (cfg.m_auiBitDepthColor[0] - 8)));
    m_iMatchNeighbors = cfg.m_iMatchNeighbors;

    m_uiNumberOfLabels = cfg.m_uiNumberOfDepthSteps;

    m_pcMatcher = pcMatcher;

    m_auiLabel2Depth = pcMatcher->m_auiLabel2Depth;
    m_auiDepth2Label = pcMatcher->m_auiDepth2Label;
    m_adLabel2Z = pcMatcher->m_adLabel2Z;
    m_adLabel2InvZ = pcMatcher->m_adLabel2InvZ;

    for (UInt uiCam = 0; uiCam < cfg.m_uiNumberOfCameras; uiCam++) {
        UInt* neighbors = new UInt[cfg.m_iMatchNeighbors];
        m_auiNeighbors.push_back(neighbors);
    }

    cArray<pair<Double,UInt>> dist;
    
    for (UInt uiCam = 0; uiCam < cfg.m_uiNumberOfCameras; uiCam++) {
        
        for (UInt uiNeighCam = 0; uiNeighCam < cfg.m_uiNumberOfCameras; uiNeighCam++) {
            Double camdist = sqrt(
                pow((rapcCameraParameters[uiCam]->m_mExtMat.at(0, 3) - rapcCameraParameters[uiNeighCam]->m_mExtMat.at(0, 3)), 2) +
                pow((rapcCameraParameters[uiCam]->m_mExtMat.at(1, 3) - rapcCameraParameters[uiNeighCam]->m_mExtMat.at(1, 3)), 2) +
                pow((rapcCameraParameters[uiCam]->m_mExtMat.at(2, 3) - rapcCameraParameters[uiNeighCam]->m_mExtMat.at(2, 3)), 2)
            );

            dist.push_back(make_pair(camdist,uiNeighCam));
        }
        std::sort(dist.begin(), dist.end());

        for (UInt uiNeighCam = 0; uiNeighCam < UInt(cfg.m_iMatchNeighbors); uiNeighCam++) {
            m_auiNeighbors[uiCam][uiNeighCam] = dist[uiNeighCam+1].second;
        }

        dist.clear();
    }

    return;
}

Void cOptimizer::calcDepth(cConfig &cfg, cArray<cYUV<DepthPixelType>*> &rapcInputView, cArray<UInt*> &rapuiDepthMapLabel, cArray<cSegmentation*> &rapcSuperpixels) {

    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
        UInt m_uiViewSize = cfg.getFileInputViewWidth(uiCam)*cfg.getFileInputViewHeight(uiCam);
        UInt* puiDepthLabel = rapuiDepthMapLabel[uiCam];
        cSegmentation* pcSegments = rapcSuperpixels[uiCam];
        cYUV<DepthPixelType>* pcInputViewDepth = rapcInputView[uiCam];
        for (UInt uiPos = 0; uiPos < m_uiViewSize; uiPos++) {
            if (pcSegments->m_cYUVlabel[uiPos] < pcSegments->m_uiNumberOfSuperpixels) {
                pcInputViewDepth->m_atY[uiPos] = (DepthPixelType)m_auiLabel2Depth[puiDepthLabel[pcSegments->m_cYUVlabel[uiPos]]];
            }
        }
    }

    return;
}