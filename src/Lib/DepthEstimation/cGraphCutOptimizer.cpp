//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//  If you use this software, please cite the corresponding document:
//  "Immersive video depth estimation", ISO/IEC SC29/WG11/MPEG, M53407 document
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */


#include "cGraphCutOptimizer.h"
#include <time.h>
#include <cmath>
#include <omp.h>
#include <thread>
#include <cfloat>
#include <cstring>
#include "../Common/cMatrix.h"

Void cGraphCutOptimizer::calcImPosPersp(Double& wPos, Double& hPos, Double invTargetZ, cMatrix<Double>* XYZ, cMatrix<Double>* mat) {

    cMatrix<Double> tmpMat;
    cMatrix<Double>::MatrixMultiply(*mat, *XYZ, tmpMat);

    invTargetZ = 1 / tmpMat.at(2, 0);

    wPos = tmpMat.at(0, 0) * invTargetZ;
    hPos = tmpMat.at(1, 0) * invTargetZ;

    return;
}

Void cGraphCutOptimizer::calcXYZPersp(UInt w, UInt h, Double invZ, cMatrix<Double>* XYZ, cMatrix<Double>* mat) {

    cMatrix<Double> whz(4, 1);
    Double dZ = 1 / invZ;
    whz.at(0, 0) = w * dZ;
    whz.at(1, 0) = h * dZ;
    whz.at(2, 0) = 1 * dZ;
    whz.at(3, 0) = 1;

    cMatrix<Double>::MatrixMultiply(*mat, whz, *XYZ);

    return;
}

Void cGraphCutOptimizer::calcImPosOmni(UInt w, UInt h, Double& wPos, Double& hPos, UInt W, UInt H, Double dInvZ, UInt uiCam, UInt uiTargetCam) {

    Double dInvTargetZ;

    Double tmp0[3], tmp2[3];
    Double tmph0[3], tmph1[3], tmph2[3];

    Double tmpOW = Double(W - 1) / m_dAoVW;
    Double tmpOH = Double(H - 1) / m_dAoVH;

    Double tmpT2[3];
    Double tmpXYZ[3];

    for (UInt i = 0; i < 3; i++) tmpT2[i] = m_mmOmniT[uiCam][uiTargetCam]->at(i, 0);

    for (UInt i = 0; i < 3; i++) {
        tmph0[i] = LUT_Theta[0][h] * m_mmOmniRR[uiCam][uiTargetCam]->at(i, 0);
        tmph1[i] = LUT_Theta[1][h] * m_mmOmniRR[uiCam][uiTargetCam]->at(i, 1);
        tmph2[i] = LUT_Theta[0][h] * m_mmOmniRR[uiCam][uiTargetCam]->at(i, 2);
    }

    long double tmpPhi0 = LUT_Phi[0][w];
    long double tmpPhi1 = LUT_Phi[1][w];

    Double dZ = 1 / dInvZ;

    for (UInt i = 0; i < 3; i++) {
        tmp0[i] = tmph0[i] * tmpPhi0;
        tmp2[i] = tmph2[i] * tmpPhi1;

        tmpXYZ[i] = dZ * (tmp0[i] + tmph1[i] + tmp2[i]);
        tmpXYZ[i] += tmpT2[i];
    }

    Double X = tmpXYZ[0];
    Double Y = tmpXYZ[1];
    Double Z = tmpXYZ[2];

    dInvTargetZ = 1.0 / sqrt(X * X + Y * Y + Z * Z);

    Double phi = atan2(Z, X);
    Double theta = asin(Y * dInvTargetZ);

    wPos = (phi - m_dAoVL) * tmpOW;
    hPos = (theta - m_dAoVB) * tmpOH;

}


Void cGraphCutOptimizer::init(cConfig& rcConfig, cMatcher* pcMatcher, cArray<cCamParams<Double>*>& rapcCameraParameters) {

    cOptimizer::init(rcConfig, pcMatcher, rapcCameraParameters);

    m_uiMatchingThresh = rcConfig.m_uiMatchingThresh * (pow(2, (rcConfig.m_auiBitDepthColor[0] - 8)));
    m_uiNumberOfCycles = rcConfig.m_uiNumberOfCycles;
    m_uiNumberOfThreads = rcConfig.m_uiNumberOfThreads;
    m_uiParallelizationType = rcConfig.m_uiParallelizationType;
    m_uiNumberOfCyclesInIFrame = rcConfig.m_uiNumberOfCyclesInIFrame;
    m_uiIFramePeriod = rcConfig.m_uiTemporalEnhancementIFramePeriod;
    m_uiTemporalEnhancementMethod = rcConfig.m_uiTemporalEnhancement;
    m_uiFeatures = rcConfig.m_uiFeatures;
    m_dAoVL = rcConfig.m_dAoVL;
    m_dAoVR = rcConfig.m_dAoVR;
    m_dAoVT = rcConfig.m_dAoVT;
    m_dAoVB = rcConfig.m_dAoVB;
    m_dAoVW = rcConfig.m_dAoVW;
    m_dAoVH = rcConfig.m_dAoVH;

    LUT_Theta = new long double * [2];
    LUT_Phi = new long double* [2];

    for (UInt i = 0; i < 2; i++) LUT_Theta[i] = new long double[rcConfig.getFileInputViewHeight(0)];
    for (UInt i = 0; i < 2; i++) LUT_Phi[i] = new long double[rcConfig.getFileInputViewWidth(0)];

    for (UInt h = 0; h < rcConfig.getFileInputViewHeight(0); h++) {
        long double theta = h * m_dAoVH / (rcConfig.getFileInputViewHeight(0) - 1) + m_dAoVB;
        LUT_Theta[0][h] = cosl(theta);
        LUT_Theta[1][h] = sinl(theta);
    }
    for (UInt w = 0; w < rcConfig.getFileInputViewWidth(0); w++) {
        long double phi = w * m_dAoVW / (rcConfig.getFileInputViewWidth(0) - 1) + m_dAoVL;
        LUT_Phi[0][w] = cosl(phi);
        LUT_Phi[1][w] = sinl(phi);
    }

    for (UInt uiCam = 0; uiCam < rcConfig.m_uiNumberOfCameras; uiCam++) {

        cMatrix<Double> R2 = cMatrix<Double>(4, 4);
        for (UInt i = 0; i < 3; i++) {
            for (UInt j = 0; j < 3; j++) R2.at(i, j) = rapcCameraParameters[uiCam]->m_mCvtdExtMat.at(i, j);
        }

        cMatrix<Double> R1 = cMatrix<Double>(4, 4);
        cMatrix<Double>::invert(R2, R1);

        cArray<cMatrix<Double>*> tempRR;
        cArray<cMatrix<Double>*> tempT;

        for (UInt uiTargetCam = 0; uiTargetCam < rcConfig.m_uiNumberOfCameras; uiTargetCam++) {
            for (UInt i = 0; i < 3; i++) {
                for (UInt j = 0; j < 3; j++) R2.at(i, j) = rapcCameraParameters[uiTargetCam]->m_mCvtdExtMat.at(i, j);
            }

            cMatrix<Double>* RR = new cMatrix<Double>(4, 4);
            cMatrix<Double>::MatrixMultiply(R2, R1, *RR);

            cMatrix<Double>* T = new cMatrix<Double>(3, 1);
            for (UInt i = 0; i < 3; i++) {
                T->at(i, 0) = rapcCameraParameters[uiCam]->m_mExtMat.at(i, 3) - rapcCameraParameters[uiTargetCam]->m_mExtMat.at(i, 3);
            }

            cMatrix<Double>* T2 = new cMatrix<Double>(3, 1);
            cMatrix<Double>::MatrixMultiply(R2, *T, *T2);


            tempRR.push_back(RR);
            tempT.push_back(T2);

        }
        m_mmOmniRR.push_back(tempRR);
        m_mmOmniT.push_back(tempT);
    }

}

cGraphCutOptimizer::~cGraphCutOptimizer() {

}

#define Cost(a,b,c) (c*Abs(a-b))

Void cGraphCutOptimizer::optimize(cConfig& cfg, cArray<cYUV<ImagePixelType>*>& rapcInputView, cArray<cCamParams<Double>*>& rapcCameraParameters,
    cArray<UInt*>& rapuiDepthMapLabel, cArray<UInt*>& rapuiDepthMapLabelPrevFrame, cArray<UInt*>& rapuiInputDepthMapLabel, cArray<UInt*>& rapuiDepthMapLabelMin, cArray<UInt*>& rapuiDepthMapLabelMax,
    cArray<cArray<Bool>>& aabSkip, cArray<cSegmentation*>& rapcSuperpixels, UInt uiFrame) {

    UInt uiMaxCycle = m_uiNumberOfCyclesInIFrame;
    if ((uiFrame - m_uiStartFrame) % m_uiIFramePeriod) uiMaxCycle = m_uiNumberOfCycles;

    // Initialize depth maps for different threads
    cArray<cArray<cArray<UInt>>> rapuiDepthMapLabel_th;
    for (UInt uiThread = 0; uiThread < m_uiNumberOfThreads; uiThread++) {
        cArray<cArray<UInt>> rapuiDepthMapLabel_temp;
        rapuiDepthMapLabel_th.push_back(rapuiDepthMapLabel_temp);

        for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
            cArray<UInt> rpuiDepthLabel_temp;
            for (UInt x = 0; x < cfg.getFileInputViewWidth(uiCam) * cfg.getFileInputViewWidth(uiCam); x++) rpuiDepthLabel_temp.push_back(0);
            rapuiDepthMapLabel_th[uiThread].push_back(rpuiDepthLabel_temp);
        }
    }
    UInt uiLabelsPerThread = m_uiNumberOfLabels / m_uiNumberOfThreads;


    // Calculate min and max value from decoder-derived features
    cArray<cArray<UShort>> min_depth;
    cArray<cArray<UShort>> max_depth;

    if (m_uiFeatures) {
        for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
            cArray<UShort> min_temp;
            cArray<UShort> max_temp;
            for (UInt uiSuperpixel = 0; uiSuperpixel < rapcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSuperpixel++) {

                cSegmentation* pcSegments = rapcSuperpixels[uiCam];
                Int uiY = pcSegments->m_apdSuperpixels[uiSuperpixel][4];
                Int uiX = pcSegments->m_apdSuperpixels[uiSuperpixel][3];

                UInt min = m_uiNumberOfLabels - 1;
                UInt max = 0;

                for (Int y = uiY - Int(pcSegments->m_uiSuperpixelSize); y < uiY + Int(pcSegments->m_uiSuperpixelSize); y++) {
                    for (Int x = uiX - Int(pcSegments->m_uiSuperpixelSize); x < uiX + Int(pcSegments->m_uiSuperpixelSize); x++) {
                        if (x >= 0 && y >= 0 && y < Int(cfg.getFileInputViewHeight(uiCam)) && x < Int(cfg.getFileInputViewWidth(uiCam))) {
                            if (!aabSkip[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x]) {
                                if (pcSegments->m_cYUVlabel[y * cfg.getFileInputViewWidth(uiCam) + x] == uiSuperpixel) {
                                    if (rapuiDepthMapLabelMin[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x] < min) {
                                        min = rapuiDepthMapLabelMin[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x];
                                    }
                                    if (rapuiDepthMapLabelMax[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x] > max) {
                                        max = rapuiDepthMapLabelMax[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x];
                                    }
                                }
                            }
                        }
                    }
                }
                if (min == m_uiNumberOfLabels - 1) min = 0;
                if (max == 0) max = m_uiNumberOfLabels - 1;
                if (max > m_uiNumberOfLabels - 1) max = m_uiNumberOfLabels - 1;
                min_temp.push_back(min);
                max_temp.push_back(max);

            }
            min_depth.push_back(min_temp);
            max_depth.push_back(max_temp);
        }
    }


    // Calculate min and max value from input depth maps
    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
        cArray<UShort> min_temp;
        cArray<UShort> max_temp;
        for (UInt uiSuperpixel = 0; uiSuperpixel < rapcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                cSegmentation* pcSegments = rapcSuperpixels[uiCam];
                Int uiY = pcSegments->m_apdSuperpixels[uiSuperpixel][4];
                Int uiX = pcSegments->m_apdSuperpixels[uiSuperpixel][3];

                UInt min = m_uiNumberOfLabels - 1;
                UInt max = 0;
                if (cfg.m_uiInputDepthMap) {
                    for (Int y = uiY - Int(pcSegments->m_uiSuperpixelSize); y < uiY + Int(pcSegments->m_uiSuperpixelSize); y++) {
                        for (Int x = uiX - Int(pcSegments->m_uiSuperpixelSize); x < uiX + Int(pcSegments->m_uiSuperpixelSize); x++) {
                            if (x >= 0 && y >= 0 && y < Int(cfg.getFileInputViewHeight(uiCam)) && x < Int(cfg.getFileInputViewWidth(uiCam))) {
                                if (pcSegments->m_cYUVlabel[y * cfg.getFileInputViewWidth(uiCam) + x] == uiSuperpixel) {
                                    if (rapuiInputDepthMapLabel[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x] < min) {
                                        min = rapuiInputDepthMapLabel[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x];
                                    }
                                    if (rapuiInputDepthMapLabel[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x] > max) {
                                        max = rapuiInputDepthMapLabel[uiCam][y * cfg.getFileInputViewWidth(uiCam) + x];
                                    }
                                }
                            }
                        }
                    }
                }
                if (min == m_uiNumberOfLabels - 1) min = 0;
                if (max == 0) max = m_uiNumberOfLabels - 1;
                min_temp.push_back(min);
                max_temp.push_back(max);
        }
        min_depth.push_back(min_temp);
        max_depth.push_back(max_temp);
    }

    // Set initial depth labels for each thread
    for (UInt uiThread = 0; uiThread < m_uiNumberOfThreads; uiThread++) {
        for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
            for (UInt uiSuperpixel = 0; uiSuperpixel < rapcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                if ((m_uiTemporalEnhancementMethod == 1) && (((uiFrame - m_uiStartFrame) % m_uiIFramePeriod) > 0)) {
                    if (rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] > uiThread * uiLabelsPerThread) rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel]
                        = rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel];
                    if (rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] >= m_uiNumberOfLabels) rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel]
                        = uiThread * uiLabelsPerThread;
                }
                else {
                    if (m_uiFeatures) {
                        if ((min_depth[uiCam][uiSuperpixel] >= uiThread * uiLabelsPerThread)) {
                            if ((min_depth[uiCam][uiSuperpixel] > ((uiThread + 1) * uiLabelsPerThread - 1))) {
                                if (!cfg.m_asFileFeatures[uiCam].empty())rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = min_depth[uiCam][uiSuperpixel];
                                else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = ((uiThread + 1) * uiLabelsPerThread - 1);
                            }
                            else {
                                if (!cfg.m_asFileFeatures[uiCam].empty() && min_depth[uiCam][uiSuperpixel] > 0)rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = min_depth[uiCam][uiSuperpixel];
                                else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiThread * uiLabelsPerThread;
                            }
                        }
                        else {
                            if (!cfg.m_asFileFeatures[uiCam].empty()) {
                                if ((max_depth[uiCam][uiSuperpixel] >= uiThread * uiLabelsPerThread)) {
                                    rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiThread * uiLabelsPerThread;
                                }
                                else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = max_depth[uiCam][uiSuperpixel];
                            }
                            else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiThread * uiLabelsPerThread;
                        }
                        if ((cfg.m_uiSkipFlag || (m_uiTemporalEnhancementMethod == 2))) {
                            if ((rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) > 0)) {
                                rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel];
                            }
                        }
                    }
                    else {
                        if (cfg.m_uiInputDepthMap) {
                            if ((min_depth[uiCam][uiSuperpixel] >= uiThread * uiLabelsPerThread)) {
                                if ((min_depth[uiCam][uiSuperpixel] > ((uiThread + 1) * uiLabelsPerThread - 1))) {
                                    if (!cfg.m_asFileInputDepthMaps[uiCam].empty())rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = min_depth[uiCam][uiSuperpixel];
                                    else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = ((uiThread + 1) * uiLabelsPerThread - 1);
                                }
                                else {
                                    if (!cfg.m_asFileInputDepthMaps[uiCam].empty() && min_depth[uiCam][uiSuperpixel] > 0)rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = min_depth[uiCam][uiSuperpixel];
                                    else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiThread * uiLabelsPerThread;
                                }
                            }
                            else {
                                if (!cfg.m_asFileInputDepthMaps[uiCam].empty()) {
                                    if ((max_depth[uiCam][uiSuperpixel] >= uiThread * uiLabelsPerThread)) {
                                        rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiThread * uiLabelsPerThread;
                                    }
                                    else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = max_depth[uiCam][uiSuperpixel];
                                }
                                else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiThread * uiLabelsPerThread;
                            }
                            if ((cfg.m_uiSkipFlag || (m_uiTemporalEnhancementMethod == 2))) {
                                if ((rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) > 0)) {
                                    rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel];
                                }
                            }
                        }
                        else {
                            if (cfg.m_uiSkipFlag || (m_uiTemporalEnhancementMethod == 2)) {
                                if ((rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) > 0)) {
                                    if ((rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] > uiThread * uiLabelsPerThread)) rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel];
                                    else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel];

                                }
                            }
                            else rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiThread * uiLabelsPerThread;
                        }
                    }
                }
            }
        }
    }

    // Initialize projection matrices
    cMatrix<Double>** IPP = new cMatrix<Double>*[m_uiNumberOfCameras];
    cMatrix<Double>* PcIP = new cMatrix<Double>[m_uiNumberOfCameras];

    cCamParams<Double> pcCamParams;


    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
        PcIP[uiCam].init(4, 4);
        if (cfg.getFileInputViewType(m_uiCenterCam) == "Equirectangular") {

            pcCamParams.m_mExtMat.init(4, 4);
            pcCamParams.m_mIntMat.init(4, 4);

            for (Int h = 0; h < 3; h++) pcCamParams.m_mIntMat.at(h, 3) = 0;
            for (Int w = 0; w < 3; w++) pcCamParams.m_mIntMat.at(3, w) = 0;
            pcCamParams.m_mIntMat.at(3, 3) = 1;


            for (Int h = 0; h < 3; h++) {
                for (Int w = 0; w < 3; w++) {
                    pcCamParams.m_mIntMat.at(h, w) = rapcCameraParameters[uiCam]->m_mIntMat.at(h, w);
                }
            }

            for (Int h = 0; h < 4; h++) {
                for (Int w = 0; w < 4; w++) {
                    pcCamParams.m_mExtMat.at(h, w) = 0;
                    if (w == h)pcCamParams.m_mExtMat.at(h, w) = 1;
                }
            }

            for (Int h = 0; h < 3; h++) {
                for (Int w = 0; w < 3; w++) {
                    pcCamParams.m_mExtMat.at(h, w) = rapcCameraParameters[uiCam]->m_mExtMat.at(h, w);
                }
            }

            for (Int h = 0; h < 3; h++) {
                pcCamParams.m_mExtMat.at(h, 3) = rapcCameraParameters[cfg.m_uiNumberOfCameras]->m_mExtMat.at(h, 3);
            }

            pcCamParams.cvtExtMat();
            pcCamParams.calcProjMat();
            pcCamParams.calcInvProjMat();
            cMatrix<Double>::MatrixMultiply(pcCamParams.m_mProjMat, rapcCameraParameters[uiCam]->m_mInvProjMat, PcIP[uiCam]);
        }
        else cMatrix<Double>::MatrixMultiply(rapcCameraParameters[m_uiCenterCam]->m_mProjMat, rapcCameraParameters[uiCam]->m_mInvProjMat, PcIP[uiCam]);
        IPP[uiCam] = new cMatrix<Double>[m_uiNumberOfCameras];
        for (UInt uiTargetCam = 0; uiTargetCam < m_uiNumberOfCameras; uiTargetCam++) {
            IPP[uiCam][uiTargetCam].init(4, 4);
            cMatrix<Double>::MatrixMultiply(rapcCameraParameters[uiTargetCam]->m_mProjMat, rapcCameraParameters[uiCam]->m_mInvProjMat, IPP[uiCam][uiTargetCam]);
        }
    }

    // Initialize graph
    m_ppbGraphNodesActive = new Bool * *[m_uiNumberOfThreads];
    m_ppcGraphNodes = new graph::node_id * *[m_uiNumberOfThreads];

    for (UInt uiThread = 0; uiThread < m_uiNumberOfThreads; uiThread++) {
        m_ppbGraphNodesActive[uiThread] = new Bool * [m_uiNumberOfCameras];
        m_ppcGraphNodes[uiThread] = new graph::node_id * [m_uiNumberOfCameras];

        for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
            m_ppbGraphNodesActive[uiThread][uiCam] = new Bool[rapcSuperpixels[uiCam]->m_uiNumberOfSuperpixels];
            m_ppcGraphNodes[uiThread][uiCam] = new graph::node_id[rapcSuperpixels[uiCam]->m_uiNumberOfSuperpixels];

            for (UInt uiSuperpixel = 0; uiSuperpixel < rapcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
            }
        }
    }

    clock_t clkBegin, clkEnd;
    clkBegin = clock();

    cSegmentation* pcSegments;

    // Calculation of depth for different threads
    cArray<std::thread> atThreads;
    for (UInt uiThread_temp = 0; uiThread_temp < m_uiNumberOfThreads; uiThread_temp++) {

        atThreads.push_back(std::thread([&](int thread_temp) {
            UInt uiThread = thread_temp;


            for (UInt uiCycle = 0; uiCycle < uiMaxCycle; uiCycle++) {
                UInt uiSource, uiMax;
                UInt uiLabelsPerThread = m_uiNumberOfLabels / m_uiNumberOfThreads;
                UInt uiSourceInit = UInt(uiThread * m_uiNumberOfLabels / m_uiNumberOfThreads);
                uiMax = Int((uiThread + 1) * m_uiNumberOfLabels / m_uiNumberOfThreads);

                for (uiSource = uiSourceInit; uiSource < uiMax; uiSource++) {
                    cSegmentation* pcSegments;

                    fprintf(stdout, "\rLevel %3d\t(%3d/%3d)\tThread %d\t", uiSource, uiSource - uiThread * uiLabelsPerThread, uiLabelsPerThread, uiThread);
                    graph gGraph;

                    bool bAllInput = true;

                    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
                        if (cfg.m_asFileInputDepthMaps[uiCam].empty()) {
                            bAllInput = false;
                            break;
                        }
                    }

                    // Set active nodes in graph
                    UInt cnt = 0;
                    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
                        pcSegments = rapcSuperpixels[uiCam];
                        for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                            if (m_uiTemporalEnhancementMethod == 1) {
                                if ((rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) % m_uiIFramePeriod)) {
                                    m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
                                    m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                                }
                                else {
                                    m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = gGraph.add_node();
                                    m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = true;
                                    cnt++;
                                }
                            }
                            else {
                                if (cfg.m_uiInputDepthMap || m_uiFeatures) {
                                    if ((cfg.m_uiSkipFlag || (m_uiTemporalEnhancementMethod == 2)) && (rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) > 0)) {
                                        m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
                                        m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                                    }
                                    else {
                                        if ((uiSource >= min_depth[uiCam][uiSuperpixel]) && (uiSource <= max_depth[uiCam][uiSuperpixel])) {
                                            m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = gGraph.add_node();
                                            m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = true;
                                            cnt++;
                                        }
                                        else {
                                            m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
                                            m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                                        }
                                    }
                                }
                                else {
                                    if ((cfg.m_uiSkipFlag || (m_uiTemporalEnhancementMethod == 2)) && (rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) > 0)) {
                                        m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
                                        m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                                    }
                                    else {
                                        m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = gGraph.add_node();
                                        m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = true;
                                        cnt++;
                                    }
                                }
                            }

                        }
                    }
                    if (cnt == 0)
                    {
                        continue;
                    }

                    // Smoothing term
                    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {

                        pcSegments = rapcSuperpixels[uiCam];

                        for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {

                            if (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel]) {

                                UInt uiY = pcSegments->m_apdSuperpixels[uiSuperpixel][4];
                                UInt uiX = pcSegments->m_apdSuperpixels[uiSuperpixel][3];
                                UInt uiLabel = rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel];

                                Double dZ = m_adLabel2Z[uiLabel];
                                Double dZLabel = (dZ - PcIP[uiCam].at(2, 3)) / (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2));
                                dZ = m_adLabel2Z[uiSource];
                                Double dZSource = (dZ - PcIP[uiCam].at(2, 3)) / (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2));

                                for (UInt uiSPnc = 0; uiSPnc < pcSegments->m_apiSuperpixelNeigh.at(uiSuperpixel).size(); uiSPnc++) {
                                    UInt uiSPn = pcSegments->m_apiSuperpixelNeigh.at(uiSuperpixel).at(uiSPnc);
                                    if ((uiSPn < pcSegments->m_uiNumberOfSuperpixels) && (uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels)) {
                                        UInt uiLabelPlusOne = rapuiDepthMapLabel_th[uiThread][uiCam][uiSPn];
                                        dZ = m_adLabel2Z[uiLabelPlusOne];
                                        Double dZPlusOne = (dZ - PcIP[uiCam].at(2, 3)) / (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2));
                                        dZ = m_adLabel2Z[uiSource];
                                        Double dZSourcePlusOne = (dZ - PcIP[uiCam].at(2, 3)) / (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2));
                                        Double dSmoothingCoeff = m_dSmoothingCoeff;

                                        Double dSuperpixelDiff =
                                            (
                                                Abs(rapcInputView[uiCam]->m_atY[UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][4]) * cfg.getFileInputViewWidth(uiCam) + UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][3])] - rapcInputView[uiCam]->m_atY[UInt(pcSegments->m_apdSuperpixels[uiSPn][4]) * cfg.getFileInputViewWidth(uiCam) + UInt(pcSegments->m_apdSuperpixels[uiSPn][3])]) +
                                                Abs(rapcInputView[uiCam]->m_atU[(UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][4]) >> 1) * (cfg.getFileInputViewWidth(uiCam) >> 1) + (UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][3]) >> 1)] - rapcInputView[uiCam]->m_atU[(UInt(pcSegments->m_apdSuperpixels[uiSPn][4]) >> 1) * (cfg.getFileInputViewWidth(uiCam) >> 1) + (UInt(pcSegments->m_apdSuperpixels[uiSPn][3]) >> 1)]) +
                                                Abs(rapcInputView[uiCam]->m_atV[(UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][4]) >> 1) * (cfg.getFileInputViewWidth(uiCam) >> 1) + (UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][3]) >> 1)] - rapcInputView[uiCam]->m_atV[(UInt(pcSegments->m_apdSuperpixels[uiSPn][4]) >> 1) * (cfg.getFileInputViewWidth(uiCam) >> 1) + (UInt(pcSegments->m_apdSuperpixels[uiSPn][3]) >> 1)])
                                                );
                                        dSuperpixelDiff = dSuperpixelDiff > m_uiMatchingThresh ? m_uiMatchingThresh : dSuperpixelDiff;
                                        dSmoothingCoeff = dSuperpixelDiff > (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8))) ? dSmoothingCoeff / dSuperpixelDiff : dSmoothingCoeff;


                                        Double dEnergyAA = 0.0;
                                        Double dEnergyAB = 0.0;
                                        Double dEnergyBA = 0.0;
                                        Double dEnergyBB = 0.0;

                                        dEnergyAA = Cost(dZLabel, dZPlusOne, dSmoothingCoeff);
                                        if (m_ppbGraphNodesActive[uiThread][uiCam][uiSPn]) dEnergyAB = Cost(dZLabel, dZSourcePlusOne, dSmoothingCoeff);
                                        dEnergyBA = Cost(dZSource, dZPlusOne, dSmoothingCoeff);
                                        if (m_ppbGraphNodesActive[uiThread][uiCam][uiSPn]) dEnergyBB = Cost(dZSource, dZSourcePlusOne, dSmoothingCoeff);

                                        dEnergyAA *= 1000.0 * (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8)));
                                        dEnergyAB *= 1000.0 * (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8)));
                                        dEnergyBA *= 1000.0 * (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8)));
                                        dEnergyBB *= 1000.0 * (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8)));

                                        if (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel]) {
                                            if (m_ppbGraphNodesActive[uiThread][uiCam][uiSPn]) {
                                                gGraph.add_term2(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], m_ppcGraphNodes[uiThread][uiCam][uiSPn], dEnergyAA, dEnergyAB, dEnergyBA, dEnergyBB);
                                            }
                                            else gGraph.add_term1(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], dEnergyAA, dEnergyBA);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    cMatrix<Double> pix_dst(4, 1);
                    cMatrix<Double> label_pix_dst(4, 1);
                    Double tmpw[3], tmph[3], tmp1[3];
                    for (Int i = 0; i < 3; i++) tmpw[i] = 0;
                    Double dPosWinTargetView;
                    Double dPosHinTargetView;
                    Double dInvZ = m_adLabel2InvZ[uiSource];
                    Double dZ = m_adLabel2Z[uiSource];

                    // Inter-view matching cost
                    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
                        pcSegments = rapcSuperpixels[uiCam];
                        for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                            UInt uiY = pcSegments->m_apdSuperpixels[uiSuperpixel][4];
                            UInt uiX = pcSegments->m_apdSuperpixels[uiSuperpixel][3];

                            dInvZ = (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2)) / (dZ - PcIP[uiCam].at(2, 3));

                            UInt uiLabel = rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel];
                            Double dInvZLabel = m_adLabel2InvZ[uiLabel];
                            Double dZLabel = m_adLabel2Z[uiLabel];
                            dInvZLabel = (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2)) / (dZLabel - PcIP[uiCam].at(2, 3));

                            for (UInt uiNeighCam = 0; uiNeighCam < UInt(m_iMatchNeighbors); uiNeighCam++) {
                                UInt uiTargetCam = m_auiNeighbors[uiCam][uiNeighCam];
                                if ((uiTargetCam == uiCam) || (uiTargetCam >= (m_uiNumberOfCameras))) continue;
                                if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                                    for (Int i = 0; i < 3; i++) tmp1[i] = IPP[uiCam][uiTargetCam].at(i, 2);
                                    for (Int i = 0; i < 3; i++) tmph[i] = uiY * IPP[uiCam][uiTargetCam].at(i, 1) + tmp1[i];
                                    for (Int i = 0; i < 3; i++) tmpw[i] = uiX * IPP[uiCam][uiTargetCam].at(i, 0) + tmph[i];
                                }
                                if (uiLabel != uiSource) {

                                    // Calculate position of point in target view
                                    if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                                        for (Int i = 0; i < 3; i++) label_pix_dst.at(i, 0) = dInvZLabel * IPP[uiCam][uiTargetCam].at(i, 3) + tmpw[i];
                                        dPosWinTargetView = int(label_pix_dst.at(0, 0) / label_pix_dst.at(2, 0) + 0.5);
                                        dPosHinTargetView = int(label_pix_dst.at(1, 0) / label_pix_dst.at(2, 0) + 0.5);
                                    }
                                    if (cfg.getFileInputViewType(uiCam) == "Equirectangular") {
                                        calcImPosOmni(uiX, uiY, dPosWinTargetView, dPosHinTargetView, cfg.getFileInputViewWidth(uiCam), cfg.getFileInputViewHeight(uiCam), dInvZLabel, uiCam, uiTargetCam);
                                    }

                                    UInt uiPosWinTargetView = UInt(dPosWinTargetView);
                                    UInt uiPosHinTargetView = UInt(dPosHinTargetView);

                                    if (uiPosWinTargetView < cfg.getFileInputViewWidth(uiTargetCam) &&
                                        uiPosHinTargetView < cfg.getFileInputViewHeight(uiTargetCam)) {
                                        UInt uiSuperpixelInTargetView = rapcSuperpixels[uiTargetCam]->m_cYUVlabel[uiPosWinTargetView + uiPosHinTargetView * cfg.getFileInputViewWidth(uiTargetCam)];
                                        if ((uiSuperpixelInTargetView < rapcSuperpixels[uiTargetCam]->m_uiNumberOfSuperpixels) && (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] || m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView])) {// 

                                            // Get depth label of target point
                                            UInt uiTargetLabel = rapuiDepthMapLabel_th[uiThread][uiTargetCam][uiSuperpixelInTargetView];
                                            if ((uiTargetLabel == uiLabel) && (uiSuperpixelInTargetView < rapcSuperpixels[uiTargetCam]->m_uiNumberOfSuperpixels)) {

                                                Double dMatchingError = m_pcMatcher->matchingError(cfg, rapcInputView, uiCam, uiTargetCam, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);

                                                Double dEnergyBB = 0.0;
                                                Double dEnergyAA = Min(dMatchingError - m_uiMatchingThresh, 0);    dEnergyAA *= 10.0;
                                                Double dEnergyBA = 0.0;
                                                Double dEnergyAB = 0.0;
                                                if (!cfg.m_asFileInputDepthMaps[uiTargetCam].empty() && !bAllInput)  dEnergyAA *= 10.0;
                                                if (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel]) {
                                                    if (m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView]) {
                                                        gGraph.add_term2(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], m_ppcGraphNodes[uiThread][uiTargetCam][uiSuperpixelInTargetView], dEnergyAA, dEnergyAB, dEnergyBA, dEnergyBB);

                                                    }
                                                    else {
                                                        gGraph.add_term1(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], dEnergyAA, dEnergyBA);
                                                    }
                                                }
                                                else {
                                                    if (m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView]) {
                                                        gGraph.add_term1(m_ppcGraphNodes[uiThread][uiTargetCam][uiSuperpixelInTargetView], dEnergyAA, dEnergyAB);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // uiSource
                                {

                                    // Calculate position of point in target wiew
                                    if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                                        for (Int i = 0; i < 3; i++) pix_dst.at(i, 0) = dInvZ * IPP[uiCam][uiTargetCam].at(i, 3) + tmpw[i];
                                        dPosWinTargetView = int(pix_dst.at(0, 0) / pix_dst.at(2, 0) + 0.5);
                                        dPosHinTargetView = int(pix_dst.at(1, 0) / pix_dst.at(2, 0) + 0.5);
                                    }
                                    if (cfg.getFileInputViewType(uiCam) == "Equirectangular") {
                                        calcImPosOmni(uiX, uiY, dPosWinTargetView, dPosHinTargetView, cfg.getFileInputViewWidth(uiCam), cfg.getFileInputViewHeight(uiCam), dInvZ, uiCam, uiTargetCam);
                                    }
                                    UInt uiPosWinTargetView = UInt(dPosWinTargetView);
                                    UInt uiPosHinTargetView = UInt(dPosHinTargetView);


                                    if (uiPosWinTargetView < cfg.getFileInputViewWidth(uiTargetCam) &&
                                        uiPosHinTargetView < cfg.getFileInputViewHeight(uiTargetCam)) {
                                        UInt uiSuperpixelInTargetView = rapcSuperpixels[uiTargetCam]->m_cYUVlabel[uiPosWinTargetView + uiPosHinTargetView * cfg.getFileInputViewWidth(uiTargetCam)];
                                        if ((uiSuperpixelInTargetView < rapcSuperpixels[uiTargetCam]->m_uiNumberOfSuperpixels) && (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] || m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView])) {

                                            // Get depth label of target point
                                            UInt uiTargetLabel = rapuiDepthMapLabel_th[uiThread][uiTargetCam][uiSuperpixelInTargetView];

                                            {
                                                bool const A = m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel];
                                                bool const B = m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView];
                                                bool const T = uiTargetLabel == uiSource;
                                                bool const L = uiLabel == uiSource;
                                                bool willstore = false;
                                                if (A && (B || T))
                                                    willstore = true;
                                                else if (!A && B && L)
                                                    willstore = true;
                                                if (!willstore)
                                                    continue;
                                            }

                                            Double dEnergyAB = 0.0;
                                            Double dEnergyBA = 0.0;
                                            Double dMatchingError = m_pcMatcher->matchingError(cfg, rapcInputView, uiCam, uiTargetCam, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);
                                            Double dEnergyBB = Min(dMatchingError - m_uiMatchingThresh, 0);    dEnergyBB *= 10.0;
                                            if (!cfg.m_asFileInputDepthMaps[uiTargetCam].empty() && !bAllInput)  dEnergyBB *= 10.0;
                                            if (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel]) {
                                                if (m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView]) {

                                                    gGraph.add_term2(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], m_ppcGraphNodes[uiThread][uiTargetCam][uiSuperpixelInTargetView], 0, dEnergyAB, dEnergyBA, dEnergyBB);
                                                }
                                                else {
                                                    if (uiTargetLabel == uiSource) {

                                                        gGraph.add_term1(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], dEnergyAB, dEnergyBB);
                                                    }
                                                }
                                            }
                                            else {
                                                if (m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView]) {
                                                    if (uiLabel == uiSource) {
                                                        gGraph.add_term1(m_ppcGraphNodes[uiThread][uiTargetCam][uiSuperpixelInTargetView], dEnergyBA, dEnergyBB);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    gGraph.maxflow();
                    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
                        pcSegments = rapcSuperpixels[uiCam];
                        for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                            if ((m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel])) {
                                if (gGraph.what_segment(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel]) != graph::SOURCE) {
                                    rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiSource;
                                }
                            }
                        }
                    }

                    clkEnd = clock();
                }
            }

            }, uiThread_temp
        ));
    }

    for (UInt uiThread2 = 0; uiThread2 < m_uiNumberOfThreads; uiThread2++) if (atThreads[uiThread2].joinable()) atThreads[uiThread2].join(); //
    atThreads.clear();


    // Merging of depth maps calculated by different threads

    fprintf(stdout, "\n\nMerging of depth maps calculated by different threads\n");
    cArray<std::thread> atThreads2;
    //Int ui2Pow = 2;
    for (UInt uiNumOfIter = 0; uiNumOfIter < 1; uiNumOfIter++) {
        for (Int uiThread_temp = m_uiNumberOfThreads - 1; uiThread_temp > 0; uiThread_temp -= 1) {
            
                    UInt uiThread = 0;

                    if ((uiThread + uiThread_temp) < m_uiNumberOfThreads) {
                        for (UInt uiCycle = 0; uiCycle < 2; uiCycle++) {

                            bool bAllInput = true;

                            for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
                                if (cfg.m_asFileInputDepthMaps[uiCam].empty()) {
                                    bAllInput = false;
                                    break;
                                }
                            }

                            graph gGraph;
                            cSegmentation* pcSegments;
                            for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
                                pcSegments = rapcSuperpixels[uiCam];

                                for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                                    if ((!cfg.m_uiInputDepthMap && !m_uiFeatures) && m_uiTemporalEnhancementMethod > 0) {
                                        if ((rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) % m_uiIFramePeriod)) {
                                            m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
                                            m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                                        }
                                        else {
                                            if ((cfg.m_uiSkipFlag || m_uiTemporalEnhancementMethod == 2) && (rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) > 0)) {
                                                m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
                                                m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                                            }
                                            else {
                                                m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = gGraph.add_node();
                                                m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = true;
                                            }
                                        }
                                    }
                                    else {
                                        if (cfg.m_uiInputDepthMap || m_uiFeatures) {
                                            if (!cfg.m_asFileInputDepthMaps[uiCam].empty() && (min_depth[uiCam][uiSuperpixel] > 0) && (rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] >= min_depth[uiCam][uiSuperpixel]) && (rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] <= max_depth[uiCam][uiSuperpixel])) {

                                                m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
                                                m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                                        
                                            }
                                            else {
                                                if ((cfg.m_uiSkipFlag || (m_uiTemporalEnhancementMethod == 2)) && (rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) > 0)) {
                                                    m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = NULL;
                                                    m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = false;
                                                }
                                                else {
                                                    m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = gGraph.add_node();
                                                    m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = true;
                                                }
                                            }
                                        }
                                        else {
                                            m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel] = gGraph.add_node();
                                            m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] = true;
                                        }

                                    }
                                }
                            }

                            // Smoothing term
                            for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {

                                pcSegments = rapcSuperpixels[uiCam];

                                for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                                    if (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel]) {
                                        UInt uiY = pcSegments->m_apdSuperpixels[uiSuperpixel][4];
                                        UInt uiX = pcSegments->m_apdSuperpixels[uiSuperpixel][3];

                                        UInt uiLabel = rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel];
                                        Double dZ = m_adLabel2Z[uiLabel];
                                        Double dZLabel = (dZ - PcIP[uiCam].at(2, 3)) / (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2));

                                        UInt uiSource = rapuiDepthMapLabel_th[uiThread + uiThread_temp][uiCam][uiSuperpixel];

                                        dZ = m_adLabel2Z[uiSource];
                                        Double dZSource = (dZ - PcIP[uiCam].at(2, 3)) / (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2));

                                        for (UInt uiSPnc = 0; uiSPnc < pcSegments->m_apiSuperpixelNeigh[uiSuperpixel].size(); uiSPnc++) {
                                            UInt uiSPn = pcSegments->m_apiSuperpixelNeigh[uiSuperpixel][uiSPnc];
                                            if ((uiSPn < pcSegments->m_uiNumberOfSuperpixels) && (uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels)) {
                                                UInt uiLabelPlusOne = rapuiDepthMapLabel_th[uiThread][uiCam][uiSPn];
                                                dZ = m_adLabel2Z[uiLabelPlusOne];
                                                Double dZPlusOne = (dZ - PcIP[uiCam].at(2, 3)) / (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2));
                                                dZ = m_adLabel2Z[rapuiDepthMapLabel_th[uiThread + uiThread_temp][uiCam][uiSPn]];
                                                Double dZSourcePlusOne = (dZ - PcIP[uiCam].at(2, 3)) / (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2));
                                                Double dSmoothingCoeff = m_dSmoothingCoeff;

                                                Double dSuperpixelDiff =
                                                    (
                                                        Abs(rapcInputView[uiCam]->m_atY[UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][4]) * cfg.getFileInputViewWidth(uiCam) + UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][3])] - rapcInputView[uiCam]->m_atY[UInt(pcSegments->m_apdSuperpixels[uiSPn][4]) * cfg.getFileInputViewWidth(uiCam) + UInt(pcSegments->m_apdSuperpixels[uiSPn][3])]) +
                                                        Abs(rapcInputView[uiCam]->m_atU[(UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][4]) >> 1) * (cfg.getFileInputViewWidth(uiCam) >> 1) + (UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][3]) >> 1)] - rapcInputView[uiCam]->m_atU[(UInt(pcSegments->m_apdSuperpixels[uiSPn][4]) >> 1) * (cfg.getFileInputViewWidth(uiCam) >> 1) + (UInt(pcSegments->m_apdSuperpixels[uiSPn][3]) >> 1)]) +
                                                        Abs(rapcInputView[uiCam]->m_atV[(UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][4]) >> 1) * (cfg.getFileInputViewWidth(uiCam) >> 1) + (UInt(pcSegments->m_apdSuperpixels[uiSuperpixel][3]) >> 1)] - rapcInputView[uiCam]->m_atV[(UInt(pcSegments->m_apdSuperpixels[uiSPn][4]) >> 1) * (cfg.getFileInputViewWidth(uiCam) >> 1) + (UInt(pcSegments->m_apdSuperpixels[uiSPn][3]) >> 1)])
                                                        );
                                                dSuperpixelDiff = dSuperpixelDiff > m_uiMatchingThresh ? m_uiMatchingThresh : dSuperpixelDiff;
                                                dSmoothingCoeff = dSuperpixelDiff > (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8))) ? dSmoothingCoeff / dSuperpixelDiff : dSmoothingCoeff;


                                                Double dEnergyAA = 0.0;
                                                Double dEnergyAB = 0.0;
                                                Double dEnergyBA = 0.0;
                                                Double dEnergyBB = 0.0;


                                                dEnergyAA = Cost(dZLabel, dZPlusOne, dSmoothingCoeff);
                                                if (m_ppbGraphNodesActive[uiThread][uiCam][uiSPn]) dEnergyAB = Cost(dZLabel, dZSourcePlusOne, dSmoothingCoeff);
                                                dEnergyBA = Cost(dZSource, dZPlusOne, dSmoothingCoeff);
                                                if (m_ppbGraphNodesActive[uiThread][uiCam][uiSPn]) dEnergyBB = Cost(dZSource, dZSourcePlusOne, dSmoothingCoeff);



                                                dEnergyAA *= 1000.0 * (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8)));
                                                dEnergyAB *= 1000.0 * (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8)));
                                                dEnergyBA *= 1000.0 * (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8)));
                                                dEnergyBB *= 1000.0 * (pow(2, (cfg.m_auiBitDepthColor[uiCam] - 8)));

                                                if (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel]) {
                                                    if (m_ppbGraphNodesActive[uiThread][uiCam][uiSPn]) {
                                                        gGraph.add_term2(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], m_ppcGraphNodes[uiThread][uiCam][uiSPn], dEnergyAA, dEnergyAB, dEnergyBA, dEnergyBB);
                                                    }
                                                    else gGraph.add_term1(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], dEnergyAA, dEnergyBA);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // Interview matching cost
                            cMatrix<Double> pix_dst(4, 1);
                            cMatrix<Double> label_pix_dst(4, 1);
                            Double tmpw[3], tmph[3], tmp1[3];
                            for (Int i = 0; i < 3; i++) tmpw[i] = 0;
                            Double dPosWinTargetView;
                            Double dPosHinTargetView;
                            for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
                                pcSegments = rapcSuperpixels[uiCam];
                                for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                                    UInt uiY = pcSegments->m_apdSuperpixels[uiSuperpixel][4];
                                    UInt uiX = pcSegments->m_apdSuperpixels[uiSuperpixel][3];

                                    UInt uiSource = rapuiDepthMapLabel_th[uiThread + uiThread_temp][uiCam][uiSuperpixel];
                                    Double dInvZ = m_adLabel2InvZ[uiSource];
                                    Double dZ = m_adLabel2Z[uiSource];
                                    dInvZ = (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2)) / (dZ - PcIP[uiCam].at(2, 3));

                                    UInt uiLabel = rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel];
                                    Double dInvZLabel = m_adLabel2InvZ[uiLabel];
                                    Double dZLabel = m_adLabel2Z[uiLabel];
                                    dInvZLabel = (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2)) / (dZLabel - PcIP[uiCam].at(2, 3));

                                    for (UInt uiNeighCam = 0; uiNeighCam < UInt(m_iMatchNeighbors); uiNeighCam++) {
                                        UInt uiTargetCam = m_auiNeighbors[uiCam][uiNeighCam];

                                        if ((uiTargetCam == uiCam) || (uiTargetCam >= (m_uiNumberOfCameras))) continue;
                                        if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                                            for (Int i = 0; i < 3; i++) tmp1[i] = IPP[uiCam][uiTargetCam].at(i, 2);
                                            for (Int i = 0; i < 3; i++) tmph[i] = uiY * IPP[uiCam][uiTargetCam].at(i, 1) + tmp1[i];
                                            for (Int i = 0; i < 3; i++) tmpw[i] = uiX * IPP[uiCam][uiTargetCam].at(i, 0) + tmph[i];
                                        }
                                        if (uiLabel != uiSource) {

                                            // Calculate position of point in target view
                                            if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                                                for (Int i = 0; i < 3; i++) label_pix_dst.at(i, 0) = dInvZLabel * IPP[uiCam][uiTargetCam].at(i, 3) + tmpw[i];
                                                dPosWinTargetView = int(label_pix_dst.at(0, 0) / label_pix_dst.at(2, 0) + 0.5);
                                                dPosHinTargetView = int(label_pix_dst.at(1, 0) / label_pix_dst.at(2, 0) + 0.5);
                                            }
                                            if (cfg.getFileInputViewType(uiCam) == "Equirectangular") {
                                                calcImPosOmni(uiX, uiY, dPosWinTargetView, dPosHinTargetView, cfg.getFileInputViewWidth(uiCam), cfg.getFileInputViewHeight(uiCam), dInvZLabel, uiCam, uiTargetCam);
                                            }

                                            UInt uiPosWinTargetView = UInt(dPosWinTargetView);
                                            UInt uiPosHinTargetView = UInt(dPosHinTargetView);

                                            if (uiPosWinTargetView < cfg.getFileInputViewWidth(uiTargetCam) &&
                                                uiPosHinTargetView < cfg.getFileInputViewHeight(uiTargetCam)) {
                                                UInt uiSuperpixelInTargetView = rapcSuperpixels[uiTargetCam]->m_cYUVlabel[uiPosWinTargetView + uiPosHinTargetView * cfg.getFileInputViewWidth(uiTargetCam)];
                                                if ((uiSuperpixelInTargetView < rapcSuperpixels[uiTargetCam]->m_uiNumberOfSuperpixels) && (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] || m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView])) {

                                                    // Get depth label of target point
                                                    UInt uiTargetLabel = rapuiDepthMapLabel_th[uiThread][uiTargetCam][uiSuperpixelInTargetView];
                                                    if ((uiTargetLabel == uiLabel) && (uiSuperpixelInTargetView < rapcSuperpixels[uiTargetCam]->m_uiNumberOfSuperpixels)) {

                                                        Double dMatchingError = m_pcMatcher->matchingError(cfg, rapcInputView, uiCam, uiTargetCam, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);

                                                        Double dEnergyBB = 0.0;
                                                        Double dEnergyAA = Min(dMatchingError - m_uiMatchingThresh, 0);    dEnergyAA *= 10.0;
                                                        Double dEnergyBA = 0.0;
                                                        Double dEnergyAB = 0.0;
                                                        if (!cfg.m_asFileInputDepthMaps[uiTargetCam].empty() && !bAllInput)  dEnergyAA *= 10.0;
                                                        if (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel]) {
                                                            if (m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView]) {
                                                                gGraph.add_term2(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], m_ppcGraphNodes[uiThread][uiTargetCam][uiSuperpixelInTargetView], dEnergyAA, dEnergyAB, dEnergyBA, dEnergyBB);

                                                            }
                                                            else {
                                                                gGraph.add_term1(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], dEnergyAA, dEnergyBA);
                                                            }
                                                        }
                                                        else {
                                                            if (m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView]) {
                                                                gGraph.add_term1(m_ppcGraphNodes[uiThread][uiTargetCam][uiSuperpixelInTargetView], dEnergyAA, dEnergyAB);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        // uiSource
                                        {

                                            // Calculate position of point in target wiew
                                            if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                                                for (Int i = 0; i < 3; i++) pix_dst.at(i, 0) = dInvZ * IPP[uiCam][uiTargetCam].at(i, 3) + tmpw[i];
                                                dPosWinTargetView = int(pix_dst.at(0, 0) / pix_dst.at(2, 0) + 0.5);
                                                dPosHinTargetView = int(pix_dst.at(1, 0) / pix_dst.at(2, 0) + 0.5);
                                            }
                                            if (cfg.getFileInputViewType(uiCam) == "Equirectangular") {
                                                calcImPosOmni(uiX, uiY, dPosWinTargetView, dPosHinTargetView, cfg.getFileInputViewWidth(uiCam), cfg.getFileInputViewHeight(uiCam), dInvZ, uiCam, uiTargetCam);
                                            }
                                            UInt uiPosWinTargetView = UInt(dPosWinTargetView);
                                            UInt uiPosHinTargetView = UInt(dPosHinTargetView);


                                            if (uiPosWinTargetView < cfg.getFileInputViewWidth(uiTargetCam) &&
                                                uiPosHinTargetView < cfg.getFileInputViewHeight(uiTargetCam)) {
                                                UInt uiSuperpixelInTargetView = rapcSuperpixels[uiTargetCam]->m_cYUVlabel[uiPosWinTargetView + uiPosHinTargetView * cfg.getFileInputViewWidth(uiTargetCam)];
                                                if ((uiSuperpixelInTargetView < rapcSuperpixels[uiTargetCam]->m_uiNumberOfSuperpixels) && (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel] || m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView])) {

                                                    // Get depth label of target point
                                                    UInt uiTargetLabel = rapuiDepthMapLabel_th[uiThread + uiThread_temp][uiTargetCam][uiSuperpixelInTargetView];// 

                                                    {
                                                        bool const A = m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel];
                                                        bool const B = m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView];
                                                        bool const T = uiTargetLabel == uiSource;
                                                        bool willstore = false;
                                                        if (A && (B || T))
                                                            willstore = true;
                                                        if (!willstore)
                                                            continue;
                                                    }

                                                    Double dEnergyAB = 0.0;
                                                    Double dEnergyBA = 0.0;
                                                    Double dMatchingError = m_pcMatcher->matchingError(cfg, rapcInputView, uiCam, uiTargetCam, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);
                                                    Double dEnergyBB = Min(dMatchingError - m_uiMatchingThresh, 0);    dEnergyBB *= 10.0;
                                                    if (!cfg.m_asFileInputDepthMaps[uiTargetCam].empty() && !bAllInput)  dEnergyBB *= 10.0;
                                                    if (m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel]) {
                                                        if (m_ppbGraphNodesActive[uiThread][uiTargetCam][uiSuperpixelInTargetView]) {

                                                            gGraph.add_term2(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], m_ppcGraphNodes[uiThread][uiTargetCam][uiSuperpixelInTargetView], 0, dEnergyAB, dEnergyBA, dEnergyBB);
                                                        }
                                                        else {
                                                            if (uiTargetLabel == uiSource) {

                                                                gGraph.add_term1(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel], dEnergyAB, dEnergyBB);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            

                            gGraph.maxflow();
                            for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
                                pcSegments = rapcSuperpixels[uiCam];
                                for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                                    if ((m_ppbGraphNodesActive[uiThread][uiCam][uiSuperpixel])) {
                                        if (gGraph.what_segment(m_ppcGraphNodes[uiThread][uiCam][uiSuperpixel]) != graph::SOURCE) {
                                            UInt uiSource = rapuiDepthMapLabel_th[uiThread + uiThread_temp][uiCam][uiSuperpixel];
                                            rapuiDepthMapLabel_th[uiThread][uiCam][uiSuperpixel] = uiSource;
                                        }
                                    }
                                }

                            }
                            clkEnd = clock();
                        }
                    }
        }
    }

    cArray<UInt*> rapuiDepthMapLabel_nsda;
    if (cfg.m_uiNeighboringSegmentsDepthAnalysis) {

        fprintf(stdout, "\nNeighboring segments depth analysis\n");

        for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
            UInt* rpuiDepthMapLabel_nsda = new UInt[cfg.getFileInputViewWidth(uiCam) * cfg.getFileInputViewHeight(uiCam)];
            rapuiDepthMapLabel_nsda.push_back(rpuiDepthMapLabel_nsda);
        }


        for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
            for (UInt uiSPp = 0; uiSPp < rapcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSPp++) {
                rapuiDepthMapLabel_nsda[uiCam][uiSPp] = rapuiDepthMapLabel_th[0][uiCam][uiSPp];
            }
        }

        for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
            pcSegments = rapcSuperpixels[uiCam];
            for (UInt uiSuperpixel = 0; uiSuperpixel < pcSegments->m_uiNumberOfSuperpixels; uiSuperpixel++) {
                UInt uiY = pcSegments->m_apdSuperpixels[uiSuperpixel][4];
                UInt uiX = pcSegments->m_apdSuperpixels[uiSuperpixel][3];
                Double minError = DBL_MAX;

                cMatrix<Double> pix_dst(4, 1);
                cMatrix<Double> label_pix_dst(4, 1);
                Double tmpw[3], tmph[3], tmp1[3];
                for (Int i = 0; i < 3; i++) tmpw[i] = 0;
                Double dPosWinTargetView;
                Double dPosHinTargetView;


                UInt uiLabel = rapuiDepthMapLabel_th[0][uiCam][uiSuperpixel];
                Double dInvZLabel = m_adLabel2InvZ[uiLabel];
                Double dZLabel = m_adLabel2Z[uiLabel];
                dInvZLabel = (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2)) / (dZLabel - PcIP[uiCam].at(2, 3));

                for (UInt uiNeighCam = 0; uiNeighCam < UInt(m_iMatchNeighbors); uiNeighCam++) {
                    UInt uiTargetCam = m_auiNeighbors[uiCam][uiNeighCam];

                    if ((uiTargetCam == uiCam) || (uiTargetCam >= (m_uiNumberOfCameras))) continue;

                    if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                        for (Int i = 0; i < 3; i++) tmp1[i] = IPP[uiCam][uiTargetCam].at(i, 2);
                        for (Int i = 0; i < 3; i++) tmph[i] = uiY * IPP[uiCam][uiTargetCam].at(i, 1) + tmp1[i];
                        for (Int i = 0; i < 3; i++) tmpw[i] = uiX * IPP[uiCam][uiTargetCam].at(i, 0) + tmph[i];
                    }
                    // Calculate position of point in target view
                    if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                        for (Int i = 0; i < 3; i++) label_pix_dst.at(i, 0) = dInvZLabel * IPP[uiCam][uiTargetCam].at(i, 3) + tmpw[i];
                        dPosWinTargetView = int(label_pix_dst.at(0, 0) / label_pix_dst.at(2, 0) + 0.5);
                        dPosHinTargetView = int(label_pix_dst.at(1, 0) / label_pix_dst.at(2, 0) + 0.5);
                    }
                    if (cfg.getFileInputViewType(uiCam) == "Equirectangular") {
                        calcImPosOmni(uiX, uiY, dPosWinTargetView, dPosHinTargetView, cfg.getFileInputViewWidth(uiCam), cfg.getFileInputViewHeight(uiCam), dInvZLabel, uiCam, uiTargetCam);
                    }

                    UInt uiPosWinTargetView = UInt(dPosWinTargetView);
                    UInt uiPosHinTargetView = UInt(dPosHinTargetView);

                    if (uiPosWinTargetView < cfg.getFileInputViewWidth(uiTargetCam) &&
                        uiPosHinTargetView < cfg.getFileInputViewHeight(uiTargetCam)) {
                        UInt uiSuperpixelInTargetView = rapcSuperpixels[uiTargetCam]->m_cYUVlabel[uiPosWinTargetView + uiPosHinTargetView * cfg.getFileInputViewWidth(uiTargetCam)];
                        if ((uiSuperpixelInTargetView < rapcSuperpixels[uiTargetCam]->m_uiNumberOfSuperpixels)) {

                            Double dMatchingError = m_pcMatcher->matchingError(cfg, rapcInputView, uiCam, uiTargetCam, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);
                            if ((dMatchingError < minError)) {
                                minError = dMatchingError;
                            }
                        }
                    }
                }

                for (UInt uiSPnc = 0; uiSPnc < pcSegments->m_apiSuperpixelNeigh[uiSuperpixel].size(); uiSPnc++) {
                    UInt uiSPn = pcSegments->m_apiSuperpixelNeigh[uiSuperpixel][uiSPnc];

                    // Interview matching cost
                    cMatrix<Double> pix_dst(4, 1);
                    cMatrix<Double> label_pix_dst(4, 1);
                    Double tmpw[3], tmph[3], tmp1[3];

                    for (Int i = 0; i < 3; i++) tmpw[i] = 0;
                    Double dPosWinTargetView;
                    Double dPosHinTargetView;

                    UInt uiLabel = rapuiDepthMapLabel_th[0][uiCam][uiSPn];
                    Double dInvZLabel = m_adLabel2InvZ[uiLabel];
                    Double dZLabel = m_adLabel2Z[uiLabel];
                    dInvZLabel = (uiX * PcIP[uiCam].at(2, 0) + uiY * PcIP[uiCam].at(2, 1) + PcIP[uiCam].at(2, 2)) / (dZLabel - PcIP[uiCam].at(2, 3));

                    for (Int uiNeighCam = 0; uiNeighCam < m_iMatchNeighbors; uiNeighCam++) {
                        UInt uiTargetCam = m_auiNeighbors[uiCam][uiNeighCam];

                        if ((uiTargetCam == uiCam) || (uiTargetCam >= (m_uiNumberOfCameras))) continue;

                        if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                            for (Int i = 0; i < 3; i++) tmp1[i] = IPP[uiCam][uiTargetCam].at(i, 2);
                            for (Int i = 0; i < 3; i++) tmph[i] = uiY * IPP[uiCam][uiTargetCam].at(i, 1) + tmp1[i];
                            for (Int i = 0; i < 3; i++) tmpw[i] = uiX * IPP[uiCam][uiTargetCam].at(i, 0) + tmph[i];
                        }
                        // Calculate position of point in target view
                        if (cfg.getFileInputViewType(uiCam) == "Perspective") {
                            for (Int i = 0; i < 3; i++) label_pix_dst.at(i, 0) = dInvZLabel * IPP[uiCam][uiTargetCam].at(i, 3) + tmpw[i];
                            dPosWinTargetView = int(label_pix_dst.at(0, 0) / label_pix_dst.at(2, 0) + 0.5);
                            dPosHinTargetView = int(label_pix_dst.at(1, 0) / label_pix_dst.at(2, 0) + 0.5);
                        }
                        if (cfg.getFileInputViewType(uiCam) == "Equirectangular") {
                            calcImPosOmni(uiX, uiY, dPosWinTargetView, dPosHinTargetView, cfg.getFileInputViewWidth(uiCam), cfg.getFileInputViewHeight(uiCam), dInvZLabel, uiCam, uiTargetCam);
                        }

                        UInt uiPosWinTargetView = UInt(dPosWinTargetView);
                        UInt uiPosHinTargetView = UInt(dPosHinTargetView);

                        if (uiPosWinTargetView < cfg.getFileInputViewWidth(uiTargetCam) &&
                            uiPosHinTargetView < cfg.getFileInputViewHeight(uiTargetCam)) {
                            UInt uiSuperpixelInTargetView = rapcSuperpixels[uiTargetCam]->m_cYUVlabel[uiPosWinTargetView + uiPosHinTargetView * cfg.getFileInputViewWidth(uiTargetCam)];
                            if ((uiSuperpixelInTargetView < rapcSuperpixels[uiTargetCam]->m_uiNumberOfSuperpixels)) {

                                Double dMatchingError = m_pcMatcher->matchingError(cfg, rapcInputView, uiCam, uiTargetCam, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);
                                if ((dMatchingError < minError) && (Abs((Int)rapuiDepthMapLabel_th[0][uiTargetCam][uiSuperpixelInTargetView] - (Int)uiLabel) < 2)) {
                                    rapuiDepthMapLabel_nsda[uiCam][uiSuperpixel] = uiLabel;
                                    minError = dMatchingError;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    clkEnd = clock();
    fprintf(stdout, "\nTime per frame %7.3f sec.\n", ((clkEnd - clkBegin) / Double(CLOCKS_PER_SEC)));

    for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
        for (UInt uiSuperpixel = 0; uiSuperpixel < rapcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSuperpixel++) {
            if (((m_uiTemporalEnhancementMethod == 1) && (rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && (((uiFrame - m_uiStartFrame) % m_uiIFramePeriod))) ||
                ((cfg.m_uiSkipFlag > 0 || (m_uiTemporalEnhancementMethod == 2)) && (rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel] < m_uiNumberOfLabels) && ((uiFrame - m_uiStartFrame) > 0))) // && ((min_depth[uiCam][uiSuperpixel] == 0) && cfg.m_uiInputDepthMap)
            {
                rapuiDepthMapLabel[uiCam][uiSuperpixel] = rapuiDepthMapLabelPrevFrame[uiCam][uiSuperpixel];
            }
            else {
                if (cfg.m_uiNeighboringSegmentsDepthAnalysis) rapuiDepthMapLabel[uiCam][uiSuperpixel] = rapuiDepthMapLabel_nsda[uiCam][uiSuperpixel];
                else rapuiDepthMapLabel[uiCam][uiSuperpixel] = rapuiDepthMapLabel_th[0][uiCam][uiSuperpixel];
            }
        }
    }

    for (UInt uiThread = 0; uiThread < m_uiNumberOfThreads; uiThread++) {
        for (UInt uiCam = 0; uiCam < m_uiNumberOfCameras; uiCam++) {
            delete[] m_ppbGraphNodesActive[uiThread][uiCam];
            delete[] m_ppcGraphNodes[uiThread][uiCam];
        }
        delete[] m_ppbGraphNodesActive[uiThread];
        delete[] m_ppcGraphNodes[uiThread];
    }
    delete m_ppbGraphNodesActive;
    delete m_ppcGraphNodes;

    return;
}
