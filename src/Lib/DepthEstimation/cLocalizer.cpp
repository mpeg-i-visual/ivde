//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/
	 

#include "cLocalizer.h"

Void cLocalizer::init(cArray<cCamParams<Double>*> &camparams, cConfig &cfg) {

	m_uiNumOfCams = cfg.m_uiNumberOfCameras;
	m_uiCenterCam = cfg.m_uiCenterCam;

	m_dZfar = cfg.m_dFarthestDepthValue;
	m_dZnear = cfg.m_dNearestDepthValue;

	m_apCamRParams = new cCamParams<Double>*[m_uiNumOfCams + 2];
	m_pCamVParams = new cCamParams<Double>;

	for (UInt c = 0; c < m_uiNumOfCams + 1; c++) {
		m_apCamRParams[c] = camparams[c];
	}

	return;
}

Void cLocalizer::localize(cArray<cYUV<DepthPixelType>*> &apcYUVinputDepth, UInt uiCam, cConfig& cParams) {

    cMatrix<Double> PIP;
    PIP.init(4, 4);

    cCamParams<Double> pcCamParams;
    pcCamParams.m_mExtMat.init(4, 4);
    pcCamParams.m_mIntMat.init(4, 4);

    for (Int h = 0; h < 3; h++) pcCamParams.m_mIntMat.at(h, 3) = 0;
    for (Int w = 0; w < 3; w++) pcCamParams.m_mIntMat.at(3, w) = 0;
    pcCamParams.m_mIntMat.at(3, 3) = 1;


    for (Int h = 0; h < 3; h++) {
        for (Int w = 0; w < 3; w++) {
            pcCamParams.m_mIntMat.at(h, w) = m_apCamRParams[uiCam]->m_mIntMat.at(h, w);
        }
    }

    for (Int h = 0; h < 4; h++) {
        for (Int w = 0; w < 4; w++) {
            pcCamParams.m_mExtMat.at(h, w) = 0;
            if (w == h)pcCamParams.m_mExtMat.at(h, w) = 1;
        }
    }

    for (Int h = 0; h < 3; h++) {
        for (Int w = 0; w < 3; w++) {
            pcCamParams.m_mExtMat.at(h, w) = m_apCamRParams[uiCam]->m_mExtMat.at(h, w);
        }
    }

    for (Int h = 0; h < 3; h++) {
        pcCamParams.m_mExtMat.at(h, 3) = m_apCamRParams[cParams.m_uiNumberOfCameras]->m_mExtMat.at(h, 3);
    }

    pcCamParams.cvtExtMat();
    pcCamParams.calcProjMat();
    pcCamParams.calcInvProjMat();

	cMatrix<Double> PcIP;
	PcIP.init(4, 4);
    if (cParams.getFileInputViewType(m_uiCenterCam) == "Equirectangular")cMatrix<Double>::MatrixMultiply(pcCamParams.m_mProjMat, m_apCamRParams[uiCam]->m_mInvProjMat, PcIP);
    else cMatrix<Double>::MatrixMultiply(m_apCamRParams[m_uiCenterCam]->m_mProjMat, m_apCamRParams[uiCam]->m_mInvProjMat, PcIP);

	cMatrix<Double> pixDst(4, 1);

	Double dInvZfar = 1.0 / m_dZfar;
	Double dInvZnear = 1.0 / m_dZnear;

	cMatrix<Double>::MatrixMultiply(m_apCamRParams[uiCam]->m_mProjMat, m_apCamRParams[uiCam]->m_mInvProjMat, PIP);

	Double dSourceD, dTargetD, dTargetZ;
	Double dInvZ;

	Double tmpw[4], tmph[4], tmp1[4];

	for (Int i = 0; i < 4; i++) tmp1[i] = PIP.at(i, 2);
	for (UInt h = 0; h < cParams.getFileInputViewHeight(uiCam); h++) {
		for (Int i = 0; i < 4; i++) tmph[i] = h * PIP.at(i, 1) + tmp1[i];
		for (UInt w = 0; w < cParams.getFileInputViewWidth(uiCam); w++) {
			for (Int i = 0; i < 4; i++) tmpw[i] = w * PIP.at(i, 0) + tmph[i];

			UInt pp = h*cParams.getFileInputViewWidth(uiCam) + w;

			dSourceD = Double(apcYUVinputDepth[uiCam]->m_atY[pp]) / (pow(2,cParams.m_auiBitDepthDepth[uiCam])-1);
			dInvZ = dSourceD * (dInvZnear - dInvZfar) + dInvZfar;

			dInvZ = (w*PcIP.at(2, 0) + h * PcIP.at(2, 1) + PcIP.at(2, 2)) / (1 / (dInvZ)-PcIP.at(2, 3)); //new

			for (Int i = 0; i < 4; i++) pixDst.at(i, 0) = dInvZ * PIP.at(i, 3) + tmpw[i];

			dTargetZ = 1.0 / (pixDst.at(3, 0) / pixDst.at(2, 0));
			dTargetD = -(pow(2, cParams.m_auiBitDepthDepth[uiCam]) - 1) * (1.0/ cParams.m_adFarthestDepthValue[uiCam] *dTargetZ - 1) / ((1.0/ cParams.m_adNearestDepthValue[uiCam] - 1.0/ cParams.m_adFarthestDepthValue[uiCam]) * dTargetZ);

			if (dTargetD < 0) dTargetD = 0;
            if (dTargetD > pow(2, cParams.m_auiBitDepthDepth[uiCam]) - 1) dTargetD = pow(2, cParams.m_auiBitDepthDepth[uiCam]) - 1;

			apcYUVinputDepth[uiCam]->m_atY[pp] = DepthPixelType(dTargetD);
		}
	}

	return;
}

Void cLocalizer::globalize(cArray<cYUV<DepthPixelType>*> &apcYUVinputDepth, UInt uiCam, cConfig& cParams) {

    cMatrix<Double> PIP;
    PIP.init(4, 4);

    cCamParams<Double> pcCamParams;
    pcCamParams.m_mExtMat.init(4, 4);
    pcCamParams.m_mIntMat.init(4, 4);
    
    for (Int h = 0; h < 3; h++) pcCamParams.m_mIntMat.at(h, 3) = 0;
    for (Int w = 0; w < 3; w++) pcCamParams.m_mIntMat.at(3, w) = 0;
    pcCamParams.m_mIntMat.at(3, 3) = 1;
    
    
    for (Int h = 0; h < 3; h++) {
        for (Int w = 0; w < 3; w++) {
            pcCamParams.m_mIntMat.at(h, w) = m_apCamRParams[uiCam]->m_mIntMat.at(h, w);
        }
    }
    
    for (Int h = 0; h < 4; h++) {
        for (Int w = 0; w < 4; w++) {
            pcCamParams.m_mExtMat.at(h, w) = 0;
            if (w == h)pcCamParams.m_mExtMat.at(h, w) = 1;
        }
    }
    
    for (Int h = 0; h < 3; h++) {
        for (Int w = 0; w < 3; w++) {
            pcCamParams.m_mExtMat.at(h, w) = m_apCamRParams[uiCam]->m_mExtMat.at(h, w);
        }
    }
    
    for (Int h = 0; h < 3; h++) {
        pcCamParams.m_mExtMat.at(h, 3) = m_apCamRParams[m_uiCenterCam]->m_mExtMat.at(h, 3);
    }
    
    pcCamParams.cvtExtMat();
    pcCamParams.calcProjMat();
    pcCamParams.calcInvProjMat();
    
    cMatrix<Double> PcIP;
    PcIP.init(4, 4);

    cMatrix<Double> pixDst(4, 1);

    Double dInvZfar = 1.0 / cParams.m_adFarthestDepthValue[uiCam];
    Double dInvZnear = 1.0 / cParams.m_adNearestDepthValue[uiCam];

    if (cParams.getFileInputViewType(m_uiCenterCam) == "Equirectangular")cMatrix<Double>::MatrixMultiply(pcCamParams.m_mProjMat, m_apCamRParams[uiCam]->m_mInvProjMat, PIP);
    else cMatrix<Double>::MatrixMultiply(m_apCamRParams[m_uiCenterCam]->m_mProjMat, m_apCamRParams[uiCam]->m_mInvProjMat, PIP);

    Double dSourceD, dTargetD, dTargetZ;
    Double dInvZ;

    Double tmpw[4], tmph[4], tmp1[4];

    for (Int i = 0; i < 4; i++) tmp1[i] = PIP.at(i, 2);
    for (UInt h = 0; h < cParams.getFileInputViewHeight(uiCam); h++) {
        for (Int i = 0; i < 4; i++) tmph[i] = h * PIP.at(i, 1) + tmp1[i];
        for (UInt w = 0; w < cParams.getFileInputViewWidth(uiCam); w++) {
            for (Int i = 0; i < 4; i++) tmpw[i] = w * PIP.at(i, 0) + tmph[i];

            UInt pp = h * cParams.getFileInputViewWidth(uiCam) + w;

            dSourceD = Double(apcYUVinputDepth[uiCam]->m_atY[pp]) / Double(pow(2, cParams.m_uiBitDepthFeatures) - 1);
            dInvZ = dSourceD * (dInvZnear - dInvZfar) + dInvZfar;

            dInvZ = (w*PcIP.at(2, 0) + h * PcIP.at(2, 1) + PcIP.at(2, 2)) / (1 / (dInvZ)-PcIP.at(2, 3));

            for (Int i = 0; i < 4; i++) pixDst.at(i, 0) = dInvZ * PIP.at(i, 3) + tmpw[i];

            dTargetZ = 1.0 / (pixDst.at(3, 0) / pixDst.at(2, 0));
            dTargetD = -(pow(2, cParams.m_auiBitDepthDepth[uiCam]) - 1) * (1.0 / m_dZfar * dTargetZ - 1) / ((1.0 / m_dZnear - 1.0 / m_dZfar) * dTargetZ);

            if (dTargetD < 0) dTargetD = 0;
            if (dTargetD > pow(2,cParams.m_auiBitDepthDepth[uiCam]) - 1) dTargetD = pow(2, cParams.m_auiBitDepthDepth[uiCam]) - 1;

            apcYUVinputDepth[uiCam]->m_atY[pp] = DepthPixelType(dTargetD);
        }
    }

    return;
}