﻿//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/


#include "cTemporal.h"

Void initTemporal(cArray<cSegmentation*> &apcSuperpixelsPrevFrame, cArray<cSegmentation*> &apcSuperpixelsIFrame, cConfig &cParams) {
	apcSuperpixelsPrevFrame.reserve(cParams.m_uiNumberOfCameras);
	for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
		cSegmentation* pcSuperpixels;
		if (cParams.m_sSuperpixelSegmentationType == "SNIC") pcSuperpixels = new cSNICSuperpixel;
		else pcSuperpixels = new cSNICSuperpixel; // For future testing of different methods
		pcSuperpixels->init(cParams.getFileInputViewWidth(uiCam), cParams.getFileInputViewHeight(uiCam), cParams.m_dSuperpixelColorCoeff, cParams.m_uiNumberOfSuperpixels*2, cParams.getFileInputViewType(uiCam));
		apcSuperpixelsPrevFrame.push_back(pcSuperpixels);
	}
	apcSuperpixelsIFrame.reserve(cParams.m_uiNumberOfCameras);
	for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
		cSegmentation* pcSuperpixels;
		if (cParams.m_sSuperpixelSegmentationType == "SNIC") pcSuperpixels = new cSNICSuperpixel;
		else pcSuperpixels = new cSNICSuperpixel; // For future testing of different methods
		pcSuperpixels->init(cParams.getFileInputViewWidth(uiCam), cParams.getFileInputViewHeight(uiCam), cParams.m_dSuperpixelColorCoeff, cParams.m_uiNumberOfSuperpixels*2, cParams.getFileInputViewType(uiCam));
		apcSuperpixelsIFrame.push_back(pcSuperpixels);
	}
	for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
		apcSuperpixelsPrevFrame[uiCam]->init_temp();
		apcSuperpixelsIFrame[uiCam]->init_temp();
	}
}

Void calcTemporal(cArray<cSegmentation*> &apcSuperpixels, cArray<cSegmentation*> &apcSuperpixelsPrevFrame, cArray<cSegmentation*> &apcSuperpixelsIFrame, cArray<UInt*> &apuiDepthMapLabel, cArray<UInt*> &apuiDepthMapLabelPrevFrame, cArray<UInt*> &apuiDepthMapLabelIFrame, UInt &uiFrame, cConfig &cParams)
{
	if ((uiFrame - cParams.m_uiStartFrame) % cParams.m_uiTemporalEnhancementIFramePeriod) {
#pragma omp parallel for num_threads(cParams.m_uiNumberOfThreads)
		for (Int iCam = 0; iCam < Int(cParams.m_uiNumberOfCameras); iCam++) {
			UInt uiSPcnt = 0;
			for (UInt uiSPp = 0; uiSPp < apcSuperpixels[iCam]->m_uiNumberOfSuperpixels; uiSPp++) {
				UInt uiSY = apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][4];
				UInt uiSX = apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][3];

				if (apcSuperpixelsIFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX] < apcSuperpixelsIFrame[iCam]->m_uiNumberOfSuperpixels) {
					if ((Abs(apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][0] - apcSuperpixelsIFrame[iCam]->m_apdSuperpixels[apcSuperpixelsIFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX]][0]) < cParams.m_dTemporalEnhancementThresh*(pow(2, (cParams.m_auiBitDepthColor[iCam] - 8)))) &&
						(Abs(apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][1] - apcSuperpixelsIFrame[iCam]->m_apdSuperpixels[apcSuperpixelsIFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX]][1]) < cParams.m_dTemporalEnhancementThresh*(pow(2, (cParams.m_auiBitDepthColor[iCam] - 8)))) &&
						(Abs(apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][2] - apcSuperpixelsIFrame[iCam]->m_apdSuperpixels[apcSuperpixelsIFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX]][2]) < cParams.m_dTemporalEnhancementThresh*(pow(2, (cParams.m_auiBitDepthColor[iCam] - 8))))
						) {
						apuiDepthMapLabelPrevFrame[iCam][uiSPp] = apuiDepthMapLabelIFrame[iCam][apcSuperpixelsIFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX]];
						continue;
					}
				}

				if (apcSuperpixelsPrevFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX] < apcSuperpixelsPrevFrame[iCam]->m_uiNumberOfSuperpixels) {
					if ((Abs(apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][0] - apcSuperpixelsPrevFrame[iCam]->m_apdSuperpixels[apcSuperpixelsPrevFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX]][0]) < cParams.m_dTemporalEnhancementThresh*(pow(2, (cParams.m_auiBitDepthColor[iCam] - 8)))) &&
						(Abs(apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][1] - apcSuperpixelsPrevFrame[iCam]->m_apdSuperpixels[apcSuperpixelsPrevFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX]][1]) < cParams.m_dTemporalEnhancementThresh*(pow(2, (cParams.m_auiBitDepthColor[iCam] - 8)))) &&
						(Abs(apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][2] - apcSuperpixelsPrevFrame[iCam]->m_apdSuperpixels[apcSuperpixelsPrevFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX]][2]) < cParams.m_dTemporalEnhancementThresh*(pow(2, (cParams.m_auiBitDepthColor[iCam] - 8))))
						) {
						apuiDepthMapLabelPrevFrame[iCam][uiSPp] = apuiDepthMapLabel[iCam][apcSuperpixelsPrevFrame[iCam]->m_cYUVlabel[uiSY*cParams.getFileInputViewWidth(iCam) + uiSX]];
						continue;
					}
				}

				uiSPcnt++;
				apuiDepthMapLabelPrevFrame[iCam][uiSPp] = cParams.m_uiNumberOfDepthSteps;

			}
			fprintf(stdout, "\nNumber of superpixels in graph for cam  %d: %d", iCam, uiSPcnt);

		}
	}
}

Void calcTemporalFromFeatures(cArray<cSegmentation*> &apcSuperpixels, cArray<cSegmentation*> &apcSuperpixelsPrevFrame, cArray<UInt*> &apuiDepthMapLabel, cArray<UInt*> &apuiDepthMapLabelPrevFrame, UInt &uiFrame, cConfig &cParams, cArray<cArray<Bool>> &aabSkip)
{
    if ((uiFrame - cParams.m_uiStartFrame) > 0) {
#pragma omp parallel for num_threads(cParams.m_uiNumberOfThreads)
        for (Int iCam = 0; iCam < Int(cParams.m_uiNumberOfCameras); iCam++) {
			String fname = cParams.m_asFileFeatures.at(iCam);
			UInt uiSPcnt = 0;
			//if (fname.empty()) {
			if(true) {
				for (UInt uiSPp = 0; uiSPp < apcSuperpixels[iCam]->m_uiNumberOfSuperpixels; uiSPp++) {
					UInt uiSY = apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][4];
					UInt uiSX = apcSuperpixels[iCam]->m_apdSuperpixels[uiSPp][3];
					if (uiSY < cParams.getFileInputViewHeight(iCam) && uiSX < cParams.getFileInputViewWidth(iCam)) {
						if (apcSuperpixelsPrevFrame[iCam]->m_cYUVlabel[uiSY * cParams.getFileInputViewWidth(iCam) + uiSX] < apcSuperpixelsPrevFrame[iCam]->m_uiNumberOfSuperpixels) {
							bool skip = true;
							for (Int y = Int(uiSY) - Int(apcSuperpixels[iCam]->m_uiSuperpixelSize); y < Int(uiSY) + Int(apcSuperpixels[iCam]->m_uiSuperpixelSize); y++) {
								for (Int x = Int(uiSX) - Int(apcSuperpixels[iCam]->m_uiSuperpixelSize); x < Int(uiSX) + Int(apcSuperpixels[iCam]->m_uiSuperpixelSize); x++) {
									if (x >= 0 && y >= 0 && y < Int(cParams.getFileInputViewHeight(iCam)) && x < Int(cParams.getFileInputViewWidth(iCam))) {
										if (apcSuperpixels[iCam]->m_cYUVlabel[y * cParams.getFileInputViewWidth(iCam) + x] == uiSPp) {
											if (aabSkip[iCam][y * cParams.getFileInputViewWidth(iCam) + x] == false) skip = false;
										}
									}
								}
							}
							if (skip) {
								apuiDepthMapLabelPrevFrame[iCam][uiSPp] = apuiDepthMapLabel[iCam][apcSuperpixelsPrevFrame[iCam]->m_cYUVlabel[uiSY * cParams.getFileInputViewWidth(iCam) + uiSX]];
								continue;
							}
						}
					}
					uiSPcnt++;
					apuiDepthMapLabelPrevFrame[iCam][uiSPp] = cParams.m_uiNumberOfDepthSteps;

				}
			}
            fprintf(stdout, "\nNumber of superpixels in graph for cam  %d: %d", iCam, uiSPcnt);

        }
    }
}

Void saveTemporal(cArray<cSegmentation*> &apcSuperpixels, cArray<cSegmentation*> &apcSuperpixelsPrevFrame, cArray<cSegmentation*> &apcSuperpixelsIFrame, cArray<UInt*> &apuiDepthMapLabel, cArray<UInt*> &apuiDepthMapLabelPrevFrame, cArray<UInt*> &apuiDepthMapLabelIFrame, UInt &uiFrame, cConfig &cParams)
{
	for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {

		apcSuperpixelsPrevFrame[uiCam]->m_uiNumberOfSuperpixels = apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels;

		// Save superpixels data
		for (UInt uiSPp = 0; uiSPp < apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSPp++) {
			apcSuperpixelsPrevFrame[uiCam]->m_apdSuperpixels[uiSPp][0] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][0];
			apcSuperpixelsPrevFrame[uiCam]->m_apdSuperpixels[uiSPp][1] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][1];
			apcSuperpixelsPrevFrame[uiCam]->m_apdSuperpixels[uiSPp][2] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][2];
			apcSuperpixelsPrevFrame[uiCam]->m_apdSuperpixels[uiSPp][3] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][3];
			apcSuperpixelsPrevFrame[uiCam]->m_apdSuperpixels[uiSPp][4] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][4];
			apuiDepthMapLabelPrevFrame[uiCam][uiSPp] = apuiDepthMapLabel[uiCam][uiSPp];
		}

		if (((uiFrame - cParams.m_uiStartFrame) % cParams.m_uiTemporalEnhancementIFramePeriod) == 0) {
			apcSuperpixelsIFrame[uiCam]->m_uiNumberOfSuperpixels = apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels;
			for (UInt uiSPp = 0; uiSPp < apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSPp++) {
				apcSuperpixelsIFrame[uiCam]->m_apdSuperpixels[uiSPp][0] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][0];
				apcSuperpixelsIFrame[uiCam]->m_apdSuperpixels[uiSPp][1] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][1];
				apcSuperpixelsIFrame[uiCam]->m_apdSuperpixels[uiSPp][2] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][2];
				apcSuperpixelsIFrame[uiCam]->m_apdSuperpixels[uiSPp][3] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][3];
				apcSuperpixelsIFrame[uiCam]->m_apdSuperpixels[uiSPp][4] = apcSuperpixels[uiCam]->m_apdSuperpixels[uiSPp][4];
				apuiDepthMapLabelIFrame[uiCam][uiSPp] = apuiDepthMapLabel[uiCam][uiSPp];
			}
		}

		// Save labeling of pixels
		for (UInt pp = 0; pp < cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam); pp++) {
			if (apcSuperpixels[uiCam]->m_cYUVlabel[pp] < apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels)apcSuperpixelsPrevFrame[uiCam]->m_cYUVlabel[pp] = apcSuperpixels[uiCam]->m_cYUVlabel[pp];
			else apcSuperpixelsPrevFrame[uiCam]->m_cYUVlabel[pp] = apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels;
		}

		if (((uiFrame - cParams.m_uiStartFrame) % cParams.m_uiTemporalEnhancementIFramePeriod) == 0) {
			for (UInt pp = 0; pp < cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam); pp++) {
				if (apcSuperpixels[uiCam]->m_cYUVlabel[pp] < apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels)apcSuperpixelsIFrame[uiCam]->m_cYUVlabel[pp] = apcSuperpixels[uiCam]->m_cYUVlabel[pp];
				else apcSuperpixelsIFrame[uiCam]->m_cYUVlabel[pp] = apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels;
			}
		}
	}
}
