//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/
	 

#include "cMatcher.h"

cMatcher::cMatcher() {
    m_adLabel2InvZ = NULL;
    m_adLabel2Z = NULL;
    m_auiLabel2Depth = NULL;
    m_auiDepth2Label = NULL;
    return;
}

cMatcher::~cMatcher() {
    freeLabel2Depth();
    freeLabel2Z();
}

Void cMatcher::init(cConfig &cfg) {
    m_uiNumberOfCameras = cfg.m_uiNumberOfCameras;
    m_uiCenterCam = cfg.m_uiCenterCam;
    m_iMatchNeighbors = cfg.m_iMatchNeighbors;

    m_dNearestDepthValue = cfg.m_dNearestDepthValue;
    m_dFarthestDepthValue = cfg.m_dFarthestDepthValue;
    m_uiNumberOfLabels = UInt(cfg.m_uiNumberOfDepthSteps);

    m_dInvZnearMinusInvZfarDivMaxDepth = (1.0 / m_dNearestDepthValue - 1.0 / m_dFarthestDepthValue) / (pow(2, cfg.m_auiBitDepthDepth[0]) - 1);
    m_dInvZfar = 1.0 / m_dFarthestDepthValue;

    initLabel2Depth(cfg);
    initDepth2Label(cfg);
    initLabel2Z();

    m_dMaxError = ((1 << cfg.m_auiBitDepthColor[0]) - 1);;

    return;
}

Void cMatcher::initLabel2Depth(cConfig &cfg){
    m_auiLabel2Depth = new UInt[m_uiNumberOfLabels];

    for (UInt uiLabel = 0; uiLabel < m_uiNumberOfLabels; uiLabel++) {
        m_auiLabel2Depth[uiLabel] = uiLabel * (pow(2, cfg.m_auiBitDepthDepth[0]) - 1) / (m_uiNumberOfLabels-1) + 0.5;
    }

    return;
}

Void cMatcher::initDepth2Label(cConfig &cfg) {
    m_auiDepth2Label = new UInt[Int(pow(2, cfg.m_auiBitDepthDepth[0]) - 1)];

    for (UInt uiDepth = 0; uiDepth < (pow(2, cfg.m_auiBitDepthDepth[0]) - 1); uiDepth++) {
        m_auiDepth2Label[uiDepth] = int(round(uiDepth * (m_uiNumberOfLabels - 1) / double(pow(2, cfg.m_auiBitDepthDepth[0]) - 1)));
    }

    return;
}

Void cMatcher::initLabel2Z() {
    m_adLabel2InvZ = new Double[m_uiNumberOfLabels];
    m_adLabel2Z = new Double[m_uiNumberOfLabels];

    for (UInt uiLabel = 0; uiLabel < m_uiNumberOfLabels; uiLabel++) {
        m_adLabel2InvZ[uiLabel] = (Double(m_auiLabel2Depth[uiLabel]) * m_dInvZnearMinusInvZfarDivMaxDepth  + m_dInvZfar);
        m_adLabel2Z[uiLabel] = 1.0 / m_adLabel2InvZ[uiLabel];
    }

    return;
}

Void cMatcher::freeLabel2Depth() {
    if (m_auiLabel2Depth)
        delete m_auiLabel2Depth;
}

Void cMatcher::freeLabel2Z() {
    if (m_adLabel2Z)
        delete m_adLabel2Z;
    if (m_adLabel2InvZ)
        delete m_adLabel2InvZ;
}
