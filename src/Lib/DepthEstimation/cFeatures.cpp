﻿//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/

#include "cFeatures.h"
#include "../Common/Json.h"
#include <climits>
#include <cstring>

using namespace TMIV::Common;
using namespace std;


cBlock::cBlock(UInt x, UInt y, UInt minVal_, UInt maxVal_, size_t size_)
{
	posW = x;
	posH = y;
	minVal = minVal_;
	maxVal = maxVal_;
	size = size_;
	costVolume = (maxVal - minVal) * size * size;
}

Bool cBlock::operator==(const cBlock& rhs) const
{
	return  (this->posW == rhs.posW && this->posH == rhs.posH && this->size == rhs.size);
}

bool cSplitter::canSkip(
	const sFeaturesParams& featuresParams,
	const cYUV<ImagePixelType>* const	prevFrame,
	const cYUV<ImagePixelType>* const	currFrame, 
	const cBlock&						currBlock)
{
	//precondition: blocks have the same size and positions
	UInt sad = 0;
	UInt cnt = 0;
	UInt maxRange = pow(2, featuresParams.viewBPS);

	for (size_t H = currBlock.posH; H < currBlock.posH + currBlock.size; H++) {
		for (size_t W = currBlock.posW; W < currBlock.posW + currBlock.size; W++) {
			if (H >= currFrame->m_uiHeight || W >= currFrame->m_uiWidth) continue;
			sad += abs(
				currFrame->m_atY[W + currFrame->m_uiWidth * H] 
				- prevFrame->m_atY[W + currFrame->m_uiWidth * H]
			);
			cnt++;
		}
	}
	Float sadPerc = ((Float)sad / (Float)cnt / (Float)maxRange) * 100.0f;

	if (sadPerc >= featuresParams.skipThresh)
		return false;
	else
		return true;
}

bool cSplitter::split(
	sFeaturesParams						splitParams, 
	const cYUV<ImagePixelType>* const	inputView, 
	cArray<cBlock>&						outputBlocks)
{
	if (splitParams.numberOfSplitLevels == 0) return false;
	UInt minVal = INT_MAX, maxVal = 0, minTmp = INT_MAX, maxTmp = 0;
	cBlock block;
	bool splitted = false;

	for (size_t H = splitParams.startAtH; H < splitParams.endAtH; H+=splitParams.blockSize) {
		for (size_t W = splitParams.startAtW; W < splitParams.endAtW; W+=splitParams.blockSize) {

			for (size_t bh = H; bh <  H + splitParams.blockSize && bh < inputView->m_uiHeight; bh++) {
				for (size_t bw = W; bw < W + splitParams.blockSize && bw < inputView->m_uiWidth; bw++) {

					minTmp = inputView->m_atY[bw + inputView->m_uiWidth * bh];
					maxTmp = minTmp;

					minVal = min(minVal, minTmp);
					maxVal = max(maxVal, maxTmp);
				}
			}

			block = cBlock(W, H, minVal, maxVal, splitParams.blockSize);

			if(block.maxVal - block.minVal > splitParams.splitThresh){
				sFeaturesParams newSplitParams = splitParams;
				newSplitParams.blockSize /= 2;
				newSplitParams.numberOfSplitLevels--;
				newSplitParams.startAtW = W;
				newSplitParams.startAtH = H;
				newSplitParams.endAtW = W + splitParams.blockSize;
				newSplitParams.endAtH = H + splitParams.blockSize;
				
				splitted = split(newSplitParams, inputView, outputBlocks);
				if (!splitted) {
					outputBlocks.push_back(block);
				}
			}
			else {
				outputBlocks.push_back(block);
			}
		}
	}
	return true;
}

void cSplitter::skip(
	const sFeaturesParams&				featuresParams,
	const cYUV<ImagePixelType>* const	prevFrame,
	const cArray<cBlock>&				prevFrameBlocks,
	const cYUV<ImagePixelType>* const	currFrame, 
	cArray<cBlock>&						currFrameBlocks)
{
	int skips = 0;
	int noskips = 0;
	//brute force search
	for (cBlock& currBlock : currFrameBlocks) {
		auto prevBlock = find(prevFrameBlocks.begin(), prevFrameBlocks.end(), currBlock);
		if (prevBlock != prevFrameBlocks.end() && canSkip(featuresParams, prevFrame, currFrame, currBlock)) {
			skips++;
			currBlock.isSkipped = true;
		}
		else {
			noskips++;
		}
	}
	std::cout << "skip: " << skips << ", noskip: " << noskips;
}


cFeatures::cFeatures(const sFeaturesParams& featuresParams)
{
	m_params = featuresParams;
	m_prevFrame = new cYUV<ImagePixelType>(m_params.endAtW, m_params.endAtH, m_params.viewBPS, m_params.viewCS);
}

void cFeatures::extract(const cYUV<ImagePixelType>* const currFrame, cArray<Bool>& outputSkipMask, UInt frame)
{
	frame -= m_params.startFrame;

	m_blocks.push_back({});

	assert(m_blocks.size()-1 == frame);
	assert(outputSkipMask.size() == m_params.endAtH * m_params.endAtW);

	cSplitter::split(m_params, currFrame, m_blocks[frame]);

	if (frame > 0) 
		cSplitter::skip(m_params, m_prevFrame, m_blocks[frame - 1], currFrame, m_blocks[frame]);

	for (cBlock& block : m_blocks[frame]) {
		for (size_t h = block.posH ; h < block.posH + block.size; h++) {
			for (size_t w = block.posW; w < block.posW + block.size; w++) {
				if (h >= currFrame->m_uiHeight || w >= currFrame->m_uiWidth) continue;
				outputSkipMask[w + currFrame->m_uiWidth * h] = block.isSkipped;
			}
		}
	}
	UInt uiBytes = ceil(m_params.viewBPS / 8.0);
	memcpy((void*)m_prevFrame->m_atY, (void*)currFrame->m_atY, uiBytes * currFrame->m_uiHeight * currFrame->m_uiWidth);
}

