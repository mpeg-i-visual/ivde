//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */


#include "cEstimator.h"
#include <cstring>
#include <iostream>
#include <fstream>
#include <string.h>
#include "../Common/Json.h"

using namespace TMIV::Common;
using namespace std;

cEstimator::cEstimator(cConfig &cParams) {

    // Camera parameters
    cParams.m_uiCenterCam = cCamParams<Double>::readParamsFromFile(cParams.m_sFileCameraParameter, m_apcCameraParameters, cParams.m_asCameraNames);

    // Calculate z near and z far 
    if(cParams.m_uiAutomaticDepthRange) calcZnearZfar(cParams);

    // Update sequence parameters JSON
    if (cParams.m_uiAutomaticDepthRange || cParams.m_uiBitDepthDepthUpdated || cParams.m_uiDepthColorSpaceUpdated) updateParamsInFile(cParams.m_sFileCameraParameter, cParams);

    // Global depth levels to local depth initialization
    m_localizer = new cLocalizer;
    m_localizer->init(m_apcCameraParameters, cParams);

    // Input views initialization
    initInputViews(cParams);

    //Input depth map initialization
    initInputDepthMaps(cParams);

    // Depth maps labels initialization
    initDepthMapLabels(cParams);

    // Output depth maps initialization
    initOutputDepthMaps(cParams);

    // Matcher initialization
    if (cParams.m_sMatcher == "Block")
        m_matcher = new cBlockMatcher;
    else
        m_matcher = new cPixelMatcher;
    m_matcher->init(cParams);

    // Optimizer choice
    m_optimizer = new cGraphCutOptimizer;

    // Segmentation initialization 
    initSegmentation(cParams);

    // Optimizer initialization
    m_optimizer->init(cParams, m_matcher, m_apcCameraParameters);

    //Internal feature extractor initialization
    if (cParams.m_uiTemporalEnhancement == 2) initFeatures(cParams);

    return;
}

void cEstimator::initInputViews(cConfig &cParams) {

    m_apcInputView.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        cYUV<ImagePixelType>* pcInputView = new cYUV<ImagePixelType>(cParams.getFileInputViewWidth(uiCam), cParams.getFileInputViewHeight(uiCam), cParams.m_auiBitDepthColor[uiCam], cParams.m_asColorSpace[uiCam]);
        m_apcInputView.push_back(pcInputView);
    }

    if (cParams.m_uiTotalNumberOfFrames > 1) {
        m_apcInputViewPrevFrame.reserve(cParams.m_uiNumberOfCameras);
        for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
            cYUV<ImagePixelType>* pcInputViewPrevFrame = new cYUV<ImagePixelType>(cParams.getFileInputViewWidth(uiCam), cParams.getFileInputViewHeight(uiCam), cParams.m_auiBitDepthColor[uiCam], cParams.m_asColorSpace[uiCam]);
            m_apcInputViewPrevFrame.push_back(pcInputViewPrevFrame);
        }
    }

    return;
}

void cEstimator::initDepthMapLabels(cConfig &cParams) {

    m_apuiDepthMapLabel.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        UInt* puiDepthMapLabel = new UInt[cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam)];
        m_apuiDepthMapLabel.push_back(puiDepthMapLabel);
    }

    m_apuiInputDepthMapLabel.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        UInt* puiInputDepthMapLabel = new UInt[cParams.getFileInputViewWidth(uiCam) * cParams.getFileInputViewHeight(uiCam)];
        m_apuiInputDepthMapLabel.push_back(puiInputDepthMapLabel);
    }

    m_apuiDepthMapLabelMin.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        UInt* puiDepthMapLabelMin = new UInt[cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam)];
        m_apuiDepthMapLabelMin.push_back(puiDepthMapLabelMin);
    }

    m_apuiDepthMapLabelMax.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        UInt* puiDepthMapLabelMax = new UInt[cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam)];
        m_apuiDepthMapLabelMax.push_back(puiDepthMapLabelMax);
    }

    if ((cParams.m_uiTemporalEnhancement > 0) || (cParams.m_uiSkipFlag > 0)) {
        m_apuiDepthMapLabelPrevFrame.reserve(cParams.m_uiNumberOfCameras);
        for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
            UInt* puiDepthMapLabelPrevFrame = new UInt[cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam)];
            m_apuiDepthMapLabelPrevFrame.push_back(puiDepthMapLabelPrevFrame);
            memset(m_apuiDepthMapLabelPrevFrame[uiCam], 0, cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam) * sizeof(UInt));
        }
    }

    if ((cParams.m_uiTemporalEnhancement > 0) || (cParams.m_uiSkipFlag > 0)) {
        m_apuiDepthMapLabelIFrame.reserve(cParams.m_uiNumberOfCameras);
        for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
            UInt* puiDepthMapLabelIFrame = new UInt[cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam)];
            m_apuiDepthMapLabelIFrame.push_back(puiDepthMapLabelIFrame);
            memset(m_apuiDepthMapLabelIFrame[uiCam], 0, cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam) * sizeof(UInt));
        }
    }

    return;
}
//initInputDepthMaps
void cEstimator::initInputDepthMaps(cConfig &cParams) {
    if (cParams.m_uiInputDepthMap) {
        m_apcInputDepthMap.reserve(cParams.m_uiNumberOfCameras);
        for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
            if (!cParams.m_asFileInputDepthMaps[uiCam].empty()) {
                cYUV<DepthPixelType>* pcInputDepthMap = new cYUV<DepthPixelType>(cParams.getFileInputViewWidth(uiCam), cParams.getFileInputViewHeight(uiCam),
                    cParams.m_auiBitDepthDepth[uiCam], cParams.m_asDepthColorSpace[uiCam]);
                m_apcInputDepthMap.push_back(pcInputDepthMap);
                
            }
            else m_apcInputDepthMap.push_back(nullptr);
        }

    }
    else return;
}

void cEstimator::initOutputDepthMaps(cConfig &cParams) {
    
    m_apcDepthMap.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        cYUV<DepthPixelType>* pcDepthMap = new cYUV<DepthPixelType>(cParams.getFileInputViewWidth(uiCam), cParams.getFileInputViewHeight(uiCam), 
            cParams.m_auiBitDepthDepth[uiCam], cParams.m_asDepthColorSpace[uiCam]);
        m_apcDepthMap.push_back(pcDepthMap);
    }

    m_apcZMin.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        cYUV<DepthPixelType>* pcZMin = new cYUV<DepthPixelType>(cParams.getFileInputViewWidth(uiCam), cParams.getFileInputViewHeight(uiCam), cParams.m_auiBitDepthDepth[uiCam], cParams.m_asDepthColorSpace[uiCam]);
        m_apcZMin.push_back(pcZMin);
    }

    m_apcZMax.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        cYUV<DepthPixelType>* pcZMax = new cYUV<DepthPixelType>(cParams.getFileInputViewWidth(uiCam), cParams.getFileInputViewHeight(uiCam), cParams.m_auiBitDepthDepth[uiCam], cParams.m_asDepthColorSpace[uiCam]);
        m_apcZMax.push_back(pcZMax);
    }

    m_aabSkip.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        cArray<Bool> abSkip;
        //abSkip.reserve(cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam));
        for (UInt x = 0; x < cParams.getFileInputViewWidth(uiCam); x++) {
            for (UInt y = 0; y < cParams.getFileInputViewHeight(uiCam); y++) {
                abSkip.push_back(false);
            }
        }
        m_aabSkip.push_back(abSkip);
    }
    return;
}

void cEstimator::calcZnearZfar(cConfig& cParams) {

    cArray<UInt*> m_auiNeighbors;

    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        UInt* neighbors = new UInt[2];
        m_auiNeighbors.push_back(neighbors);
    }

    cArray<pair<Double, UInt>> dist;
    Double maxDist = 0.0;

    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {

        for (UInt uiNeighCam = 0; uiNeighCam < cParams.m_uiNumberOfCameras; uiNeighCam++) {
            Double camdist = sqrt(
                pow((m_apcCameraParameters[uiCam]->m_mExtMat.at(0, 3) - m_apcCameraParameters[uiNeighCam]->m_mExtMat.at(0, 3)), 2) +
                pow((m_apcCameraParameters[uiCam]->m_mExtMat.at(1, 3) - m_apcCameraParameters[uiNeighCam]->m_mExtMat.at(1, 3)), 2) +
                pow((m_apcCameraParameters[uiCam]->m_mExtMat.at(2, 3) - m_apcCameraParameters[uiNeighCam]->m_mExtMat.at(2, 3)), 2)
            );
            maxDist = camdist > maxDist ? camdist : maxDist;
            dist.push_back(make_pair(camdist, uiNeighCam));
        }
        std::sort(dist.begin(), dist.end());

        for (UInt uiNeighCam = 0; uiNeighCam < UInt(2); uiNeighCam++) {
            m_auiNeighbors[uiCam][uiNeighCam] = dist[uiNeighCam + 1].second;
        }

        dist.clear();
    }

    if (cParams.getFileInputViewType(0) == "Perspective") {
        Double pix_dst[3];
        Double tmpw[3];

        Double zTemp = 0.3;
        Double eps = 1;

        cMatrix<Double> PIP;
        PIP.init(4, 4);

        Double MinZ = 1000;
        Double MaxZ = 0;

        for (UInt camS = 0; camS < UInt(cParams.m_uiNumberOfCameras); camS++) {

            int H = cParams.getFileInputViewHeight(camS);
            int W = cParams.getFileInputViewWidth(camS);

            int hPos[1] = { H / 2 };
            int wPos[2] = { W, W / 2 };

            for (UInt uiNeighCam = 0; uiNeighCam < UInt(2); uiNeighCam++) {
                UInt camT = m_auiNeighbors[camS][uiNeighCam];
                if (camS == camT) continue;

                cMatrix<Double>::MatrixMultiply(m_apcCameraParameters[camT]->m_mProjMat, m_apcCameraParameters[camS]->m_mInvProjMat, PIP);

                for (auto h : hPos) {
                    for (auto w : wPos) {
                        for (Int i = 0; i < 3; i++) tmpw[i] = w * PIP.at(i, 0) + h * PIP.at(i, 1) + PIP.at(i, 2);

                        Double Min = 1000;
                        Double dist = -1;
                        if (w == W / 2 && h == H / 2) {
                            for (Double Z = 0.3; Z < 1000.0; Z += 0.1) {
                                for (Int i = 0; i < 3; i++) pix_dst[i] = (1.0 / Z) * PIP.at(i, 3) + tmpw[i];
                                Double xc = pix_dst[0] / pix_dst[2];
                                Double yc = pix_dst[1] / pix_dst[2];

                                dist = sqrt((xc - w)*(xc - w) + (yc - h)*(yc - h));
                                if ((dist > 10 - eps) && (dist < 10 + eps)) {
                                    zTemp = Z;
                                    break;
                                }
                            }
                            if (zTemp < MinZ && zTemp > 0.3)MinZ = zTemp;
                            if (zTemp > MaxZ && zTemp < 1000)MaxZ = zTemp;
                        }
                        else {
                            Min = 1000;
                            dist = -1;
                            for (Double Z = 0.3; Z < 1000.0; Z += 0.1) {
                                for (Int i = 0; i < 3; i++) pix_dst[i] = (1.0 / Z) * PIP.at(i, 3) + tmpw[i];
                                Double xc = pix_dst[0] / pix_dst[2];
                                Double yc = pix_dst[1] / pix_dst[2];

                                dist = abs(xc - W - 1);
                                if ((dist < Min) && (yc > 0 && yc < H) && (dist > W / 10) && (xc > 0 && xc < W)) {
                                    zTemp = Z;
                                    Min = min(Min, dist);
                                }
                            }
                            if (zTemp < MinZ && zTemp > 0.3)MinZ = zTemp;
                            if (zTemp > MaxZ && zTemp < 1000)MaxZ = zTemp;
                        }
                    }
                }
            }

        }
        cParams.m_dFarthestDepthValue = MaxZ;
        cParams.m_dNearestDepthValue = MinZ;
        cParams.m_dSmoothingCoefficient = 7.0 / cParams.m_dFarthestDepthValue;

    }
    else {
        cParams.m_dNearestDepthValue = maxDist > 0.3 ? maxDist : 0.3;
        cParams.m_dFarthestDepthValue = maxDist / cParams.m_dAoVW * cParams.getFileInputViewWidth(0);
        cParams.m_dSmoothingCoefficient = 7.0 / cParams.m_dFarthestDepthValue * 1.5;
    }
    cout << endl << "Calculated Z range: [" << cParams.m_dNearestDepthValue << ", " << cParams.m_dFarthestDepthValue << "]" << endl;

    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        cParams.m_adNearestDepthValue.push_back(cParams.m_dNearestDepthValue);
        cParams.m_adFarthestDepthValue.push_back(cParams.m_dFarthestDepthValue);
    }
    return;
}

void cEstimator::updateParamsInFile(String sFilename, cConfig& cParams) {
    string temp;
    ifstream stream{ sFilename };
    string cfgSeqNewFilename = cParams.m_uiAutomaticDepthRange ? sFilename.insert(sFilename.length() - 5, "_autoDepthRange") : sFilename.insert(sFilename.length() - 5, "_bitDepthDepth");
    ofstream out{cfgSeqNewFilename};

    while (getline(stream, temp)) {
        if (temp.find("Depth_range") != string::npos) {
            out << "        \"Depth_range\": [" << cParams.m_dNearestDepthValue << ", " << cParams.m_dFarthestDepthValue << "]," << endl;
            continue;
        }
        if (temp.find("BitDepthColor") != string::npos) {
            out << temp << endl;
            out << "        \"BitDepthDepth\": " << cParams.m_auiBitDepthDepth[0] << "," << endl;
            continue;
        }
        if (temp.find("ColorSpace") != string::npos) {
            out << temp << endl;
            out << "        \"DepthColorSpace\": \"" << cParams.m_asDepthColorSpace[0] << "\"," << endl;
            continue;
        }
        else {
            out << temp << endl;
        }
    }
    return;
}


void cEstimator::initSegmentation(cConfig &cParams) {

    m_apcSuperpixels.reserve(cParams.m_uiNumberOfCameras);
    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        cSegmentation* pcSuperpixels;
        if (cParams.m_sSuperpixelSegmentationType == "SNIC") pcSuperpixels = new cSNICSuperpixel;
        else pcSuperpixels = new cSNICSuperpixel; // For future testing of different methods
        m_apcSuperpixels.push_back(pcSuperpixels);
    }

    //Temporal enhancement: segmentation of previous frames initialization
    if ((cParams.m_uiTemporalEnhancement > 0) || (cParams.m_uiSkipFlag > 0)) initTemporal(m_apcSuperpixelsPrevFrame, m_apcSuperpixelsIFrame, cParams);

    return;
}

void cEstimator::initFeatures(cConfig& cParams)
{
    m_featuresWorkers.reserve(cParams.m_uiNumberOfCameras);

    cParams.m_featuresParams.startFrame = cParams.m_uiStartFrame;
    cParams.m_featuresParams.startAtH = 0;
    cParams.m_featuresParams.startAtW = 0;
    cParams.m_featuresParams.endAtH = cParams.m_auiFileInputViewsHeight[0];
    cParams.m_featuresParams.endAtW = cParams.m_auiFileInputViewsWidth[0];
    cParams.m_featuresParams.viewBPS = cParams.m_auiBitDepthColor[0];
    cParams.m_featuresParams.viewCS = cParams.m_asColorSpace[0];

    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        
        m_featuresWorkers.push_back(new cFeatures(cParams.m_featuresParams));
    }
    return;
}

void cEstimator::readInputViews(cConfig &cParams, UInt uiFrame) {

#pragma omp parallel for num_threads(cParams.m_uiNumberOfThreads)
    for (Int iCam = 0; iCam < Int(cParams.m_uiNumberOfCameras); iCam++) {
        if ((uiFrame - cParams.m_uiStartFrame) % cParams.m_uiTemporalEnhancementIFramePeriod) {
            for (UInt pp = 0; pp < cParams.getFileInputViewWidth(iCam)*cParams.getFileInputViewHeight(iCam); pp++) {
                m_apcInputViewPrevFrame[iCam]->m_atY[pp] = m_apcInputView[iCam]->m_atY[pp];
            }
            for (UInt pp = 0; pp < (cParams.getFileInputViewWidth(iCam)*cParams.getFileInputViewHeight(iCam)
                / ((cParams.m_asColorSpace[0] == "YUV420") ? 4 : 1)); pp++) {
                m_apcInputViewPrevFrame[iCam]->m_atU[pp] = m_apcInputView[iCam]->m_atU[pp];
                m_apcInputViewPrevFrame[iCam]->m_atV[pp] = m_apcInputView[iCam]->m_atV[pp];
            }
        }
        m_apcInputView[iCam]->frameReader(cParams.getFileInputView(iCam), uiFrame);

    }

    return;
}

void cEstimator::readInputDepthMaps(cConfig& cParams, UInt uiFrame) {
 
    if (cParams.m_uiInputDepthMap) {
#pragma omp parallel for num_threads(cParams.m_uiNumberOfThreads)  
        for (Int iCam = 0; iCam < Int(cParams.m_uiNumberOfCameras); iCam++) {
        
            if (!cParams.m_asFileInputDepthMaps[iCam].empty()) {
                m_apcInputDepthMap[iCam]->frameReader(cParams.getFileInputDepthMaps(iCam), uiFrame);

                m_localizer->globalize(m_apcInputDepthMap, iCam, cParams);
                
                for (UInt x = 0; x < cParams.getFileInputViewWidth(iCam); x++) {
                    for (UInt y = 0; y < cParams.getFileInputViewHeight(iCam); y++) {
                        m_apuiInputDepthMapLabel[iCam][y * cParams.getFileInputViewWidth(iCam) + x] = m_optimizer->m_auiDepth2Label[m_apcInputDepthMap[iCam]->m_atY[y * cParams.getFileInputViewWidth(iCam) + x]];                        
                    }
                }
               
            }
            else {
                for (UInt x = 0; x < cParams.getFileInputViewWidth(iCam); x++) {
                    for (UInt y = 0; y < cParams.getFileInputViewHeight(iCam); y++) {
                        m_apuiInputDepthMapLabel[iCam][y * cParams.getFileInputViewWidth(iCam) + x] = 0;
                    }
                }
            }
        }
    }
    return;
   
}

void cEstimator::prefilterTextures(cConfig& cParams) {

    if (cParams.m_iTexturePrefilteringBlockWidth < 2)  return;

    int B = cParams.m_iTexturePrefilteringBlockWidth / 2;
    for (Int iCam = 0; iCam < Int(cParams.m_uiNumberOfCameras); iCam++) {
        int W = cParams.getFileInputViewWidth(iCam);
        int H = cParams.getFileInputViewHeight(iCam);

        int WUV = W / 2;
        int HUV = H / 2;

        int* tmpFrame = new int[W * H];

        for (int h = 0, pp = 0; h < H; h++) {
            for (int w = 0; w < W; w++, pp++) {

                int avg = 0, cnt = 0;
                for (int hh = h - B; hh <= h + B; hh++) {
                    if (hh < 0 || hh >= H) continue;
                    for (int ww = w - B; ww <= w + B; ww++) {
                        if (ww < 0 || ww >= W) continue;
                        int ppp = hh * W + ww;

                        avg += int(m_apcInputView[iCam]->m_atY[ppp]);
                        cnt++;
                    }//ww
                }//hh

                tmpFrame[pp] = avg / cnt;

            }//w
        }//h

        for (int h = 0, pp = 0; h < H; h++) {
            for (int w = 0; w < W; w++, pp++) {
                m_apcInputView[iCam]->m_atY[pp] = tmpFrame[pp];
            }
        }

        for (int h = 0, pp = 0; h < HUV; h++) {
            for (int w = 0; w < WUV; w++, pp++) {

                int cnt = 0;
                int avgU = 0;
                int avgV = 0;
                for (int hh = h - B; hh <= h + B; hh++) {
                    if (hh < 0 || hh >= HUV) continue;
                    for (int ww = w - B; ww <= w + B; ww++) {
                        if (ww < 0 || ww >= WUV) continue;
                        int ppp = hh * WUV + ww;

                            avgU += int(m_apcInputView[iCam]->m_atU[ppp]);
                            avgV += int(m_apcInputView[iCam]->m_atV[ppp]);
                            cnt++;
                    }//ww
                }//hh

                tmpFrame[pp] = avgU / cnt;
                tmpFrame[WUV * HUV + pp] = avgV / cnt;

            }//w
        }//h

        for (int h = 0, pp = 0; h < HUV; h++) {
            for (int w = 0; w < WUV; w++, pp++) {
                m_apcInputView[iCam]->m_atU[pp] = tmpFrame[pp];
                m_apcInputView[iCam]->m_atV[pp] = tmpFrame[WUV * HUV + pp];
            }
        }

        delete tmpFrame;
    }

    return;
}

void cEstimator::readFeatures(cConfig& cParams, UInt uiFrame) {
    std::cout << "\nReading external features for cam";
    for (Int iCam = 0; iCam < Int(cParams.m_uiNumberOfCameras); iCam++) {
        std::cout << " " << iCam;
        for (UInt x = 0; x < cParams.getFileInputViewWidth(iCam); x++) {
            for (UInt y = 0; y < cParams.getFileInputViewHeight(iCam); y++) {
                m_apcZMax[iCam]->m_atY[y * cParams.getFileInputViewWidth(iCam) + x] = pow(2, cParams.m_auiBitDepthDepth[iCam]) - 1;
                m_apcZMin[iCam]->m_atY[y * cParams.getFileInputViewWidth(iCam) + x] = 0;
                m_aabSkip[iCam][y * cParams.getFileInputViewWidth(iCam) + x] = false;
            }
        }
        String sFeaturesFname = cParams.m_asFileFeatures.at(iCam);

        if (!sFeaturesFname.empty()) {

            ifstream file(sFeaturesFname);
            string str;
            string file_contents;
            UInt blksize = 0;
            UInt sf = 0;

            string zero = "0";

            while (getline(file, str))
            {
                str.erase(remove(str.begin(), str.end(), '['), str.end());
                str.erase(remove(str.begin(), str.end(), ']'), str.end());

                stringstream ss(str);
                string substring;

                while (getline(ss, substring, ' ')) {
                    string word;
                    stringstream w(substring);
                    while (getline(w, word, '=')) {
                        if (word == "blksz") {
                            getline(w, word, '=');
                            blksize = atoi(word.c_str());
                        }
                        if (word == "sf") {
                            getline(w, word, '=');
                            sf = atoi(word.c_str());

                            if (sf + uiFrame > 9)
                                zero = "";
                        }
                    }

                    if (substring == "f" + zero + to_string(sf+uiFrame)) {


                        getline(ss, substring, ' ');
                        UInt y = atoi((substring.erase(0, 1)).c_str());

                        getline(ss, substring, ' ');
                        UInt x = atoi((substring.erase(0, 1)).c_str());

                        //parse flags
                        parseFeaturesFlags(cParams, iCam, ss, x, y, blksize, blksize);
                    }
                }
            }
            m_localizer->globalize(m_apcZMin, iCam, cParams);
            m_localizer->globalize(m_apcZMax, iCam, cParams);
        }

        for (UInt x = 0; x < cParams.getFileInputViewWidth(iCam); x++) {
            for (UInt y = 0; y < cParams.getFileInputViewHeight(iCam); y++) {

                if (sFeaturesFname.empty()) {
                    m_apuiDepthMapLabelMin[iCam][y * cParams.getFileInputViewWidth(iCam) + x] = 0;
                    m_apuiDepthMapLabelMax[iCam][y * cParams.getFileInputViewWidth(iCam) + x] = cParams.m_uiNumberOfDepthSteps - 1;
                }
                else {
                    m_apuiDepthMapLabelMin[iCam][y * cParams.getFileInputViewWidth(iCam) + x] = m_optimizer->m_auiDepth2Label[m_apcZMin[iCam]->m_atY[y * cParams.getFileInputViewWidth(iCam) + x]];
                    m_apuiDepthMapLabelMax[iCam][y * cParams.getFileInputViewWidth(iCam) + x] = m_optimizer->m_auiDepth2Label[m_apcZMax[iCam]->m_atY[y * cParams.getFileInputViewWidth(iCam) + x]];
                }
            }
        }
    }
    return;
}

void cEstimator::parseFeaturesFlags(cConfig& cParams, Int iCam, stringstream& ss, UInt x, UInt y, UInt blkszX, UInt blkszY)
{
    if (ss.eof())
        return;

    string read_word;
    string read_flag;

    //splitFlag
    getline(ss, read_word, '=');
    getline(ss, read_flag, ' ');

    if (read_flag == "1") {

        //skip flag 
        //here is always 0
        getline(ss, read_word, '=');
        getline(ss, read_flag, ' ');
        /* if (read_flag == "1") {

         }*/

         //quad split
        getline(ss, read_word, '=');
        getline(ss, read_flag, ' ');
        if (read_flag == "1") {

            parseFeaturesFlags(cParams, iCam, ss, x, y, floor(blkszX / 2.0f), floor(blkszY / 2.0f)); //Xd, Yd
            parseFeaturesFlags(cParams, iCam, ss, x + floor(blkszX / 2.0f), y, ceil(blkszX / 2.0f), floor(blkszY / 2.0f)); //Xu, Yd
            parseFeaturesFlags(cParams, iCam, ss, x, y + floor(blkszY / 2.0f), floor(blkszX / 2.0f), ceil(blkszY / 2.0f)); //Xd, Yu
            parseFeaturesFlags(cParams, iCam, ss, x + floor(blkszX / 2.0f), y + floor(blkszY / 2.0f), ceil(blkszX / 2.0f), ceil(blkszY / 2.0f)); //Xu, Yu
        }
        else {
            //split orientation
            getline(ss, read_word, '=');
            getline(ss, read_flag, ' ');
            if (read_flag == "1") {
                //vertically
                //symmetry
                getline(ss, read_word, '=');
                getline(ss, read_flag, ' ');
                if (read_flag == "1") {
                    parseFeaturesFlags(cParams, iCam, ss, x, y, floor(blkszX / 2.0f), blkszY);
                    parseFeaturesFlags(cParams, iCam, ss, x + floor(blkszX / 2.0f), y, blkszX - floor(blkszX / 2.0f), blkszY);
                }
                else {
                    //first bigger
                    getline(ss, read_word, '=');
                    getline(ss, read_flag, ' ');
                    if (read_flag == "1") {
                        parseFeaturesFlags(cParams, iCam, ss, x, y, ((float)blkszX / 100.0f) * 75.0f, blkszY);
                        parseFeaturesFlags(cParams, iCam, ss, x + (UInt)(((float)blkszX / 100.0f) * 75.0f), y, blkszX - (UInt)(((float)blkszX / 100.0f) * 75.0f), blkszY);
                    }
                    else {
                        parseFeaturesFlags(cParams, iCam, ss, x, y, ((float)blkszX / 100.0f) * 25.0f, blkszY);
                        parseFeaturesFlags(cParams, iCam, ss, x + (UInt)(((float)blkszX / 100.0f) * 25.0f), y, blkszX - (UInt)(((float)blkszX / 100.0f) * 25.0f), blkszY);
                    }
                }
            }
            else {
                //horizontally
                //symmetry
                getline(ss, read_word, '=');
                getline(ss, read_flag, ' ');
                if (read_flag == "1") {
                    parseFeaturesFlags(cParams, iCam, ss, x, y, blkszX, floor(blkszY / 2.0f));
                    parseFeaturesFlags(cParams, iCam, ss, x, y + floor(blkszY / 2.0f), blkszX, blkszY - floor(blkszY / 2.0f));
                }
                else {
                    //first bigger
                    getline(ss, read_word, '=');
                    getline(ss, read_flag, ' ');
                    if (read_flag == "1") {
                        parseFeaturesFlags(cParams, iCam, ss, x, y, blkszX, ((float)blkszY / 100.0f) * 75.0f);
                        parseFeaturesFlags(cParams, iCam, ss, x, y + (UInt)(((float)blkszY / 100.0f) * 75.0f), blkszX, blkszY - (UInt)(((float)blkszY / 100.0f) * 75.0f));
                    }
                    else {
                        parseFeaturesFlags(cParams, iCam, ss, x, y, blkszX, ((float)blkszY / 100.0f) * 25.0f);
                        parseFeaturesFlags(cParams, iCam, ss, x, y + (UInt)(((float)blkszY / 100.0f) * 25.0f), blkszX, blkszY - (UInt)(((float)blkszY / 100.0f) * 25.0f));
                    }
                }
            }
        }
    }
    else {
        //skip flag
        getline(ss, read_word, '=');
        getline(ss, read_flag, ' ');
        if (read_flag == "1") {
            for (UInt yy = y; yy < y + blkszY; yy++) {
                for (UInt xx = x; xx < x + blkszX; xx++) {
                    if ((yy * cParams.getFileInputViewWidth(iCam) + xx) < cParams.getFileInputViewWidth(iCam) * cParams.getFileInputViewHeight(iCam)) {
                        m_aabSkip[iCam][yy * cParams.getFileInputViewWidth(iCam) + xx] = true;
                    }
                }
            }
        }
        else {
            string read_value;
            UInt zmin = 0;
            UInt zmax = 0;

            getline(ss, read_word, '='); //zmin
            getline(ss, read_value, ' '); //zmin
            zmin = stoi(read_value);

            getline(ss, read_word, '='); //zmax
            getline(ss, read_value, ' '); //zmax
            zmax = stoi(read_value);

            for (UInt yy = y; yy < y + blkszY; yy++) {
                for (UInt xx = x; xx < x + blkszX; xx++) {
                    if ((yy * cParams.getFileInputViewWidth(iCam) + xx) < cParams.getFileInputViewWidth(iCam) * cParams.getFileInputViewHeight(iCam)) {
                        m_apcZMin[iCam]->m_atY[yy * cParams.getFileInputViewWidth(iCam) + xx] = zmin;
                        if (zmax > pow(2, cParams.m_uiBitDepthFeatures) - 1) zmax = pow(2, cParams.m_uiBitDepthFeatures) - 1;
                        m_apcZMax[iCam]->m_atY[yy * cParams.getFileInputViewWidth(iCam) + xx] = zmax;
                        m_aabSkip[iCam][yy * cParams.getFileInputViewWidth(iCam) + xx] = false;

                    }
                }
            }
        }
    }
    return;
}

void cEstimator::calcSegmentation(cConfig &cParams, UInt uiFrame) {

    fprintf(stdout, "Calculating superpixels for cam ");
#pragma omp parallel for num_threads(cParams.m_uiNumberOfThreadsSegm)
    for (Int iCam = 0; iCam < Int(cParams.m_uiNumberOfCameras); iCam++) {
        m_apcSuperpixels[iCam]->init(cParams.getFileInputViewWidth(iCam), cParams.getFileInputViewHeight(iCam), cParams.m_dSuperpixelColorCoeff, cParams.m_uiNumberOfSuperpixels, cParams.getFileInputViewType(iCam));
        fprintf(stdout, "%d ", iCam);

        m_apcSuperpixels[iCam]->compute(*m_apcInputView[iCam], cParams);
        m_apcSuperpixels[iCam]->compute_neigh();
    }

    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        for (UInt pp = 0; pp < cParams.getFileInputViewWidth(uiCam)*cParams.getFileInputViewHeight(uiCam); pp++) {
            if (m_apcSuperpixels[uiCam]->m_cYUVlabel[pp] == m_apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels) m_apcSuperpixels[uiCam]->m_cYUVlabel[pp] = 0;
        }
    }

    // Calculate for which segments depth should be estimated
    if ((cParams.m_uiTemporalEnhancement == 1)) {
        calcTemporal(m_apcSuperpixels, m_apcSuperpixelsPrevFrame, m_apcSuperpixelsIFrame, m_apuiDepthMapLabel, m_apuiDepthMapLabelPrevFrame, m_apuiDepthMapLabelIFrame, uiFrame, cParams);
    }
    return;
}

void cEstimator::calcDepth(cConfig &cParams, UInt uiFrame) {

    // Optimization
    fprintf(stdout, "\n\nCalculating depth map using graph cut optimization\n");

    m_optimizer->optimize(cParams, m_apcInputView, m_apcCameraParameters, m_apuiDepthMapLabel, m_apuiDepthMapLabelPrevFrame, m_apuiInputDepthMapLabel, m_apuiDepthMapLabelMin, m_apuiDepthMapLabelMax, m_aabSkip, m_apcSuperpixels, uiFrame);

    // Calculate depth labels to depth map
    m_optimizer->calcDepth(cParams, m_apcDepthMap, m_apuiDepthMapLabel, m_apcSuperpixels);

    return;
}

void cEstimator::calcTextureFeatures(const cConfig& cParams, UInt uiFrame){
//#pragma omp parallel for num_threads(cParams.m_uiNumberOfThreads)
    if(uiFrame - cParams.m_uiStartFrame == 0)  
        fprintf(stdout, "\nCalculating texture features for cam");

    for (Int uiCam = 0; uiCam < Int(cParams.m_uiNumberOfCameras); uiCam++) {
        String fname = cParams.m_asFileFeatures.at(uiCam);
        if (fname.empty()) {
            if (uiFrame - cParams.m_uiStartFrame == 0)
                fprintf(stdout, " %d", uiCam);
            else
                fprintf(stdout, "\nCalculating texture features for cam %d: ", uiCam);

            m_featuresWorkers[uiCam]->extract(m_apcInputView[uiCam], m_aabSkip[uiCam], uiFrame);
        }
    }
}

void cEstimator::writeOutputDepths(cConfig &cParams, UInt uiFrame) {

    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        m_localizer->localize(m_apcDepthMap, uiCam, cParams); // Global depth levels to local depth 
        for (UInt i = 0; i < m_apcDepthMap[uiCam]->m_uiViewSizeUV; i++) {
            m_apcDepthMap[uiCam]->m_atU[i] = pow(2, m_apcDepthMap[uiCam]->m_uiBitsPerSample) / 2;
            m_apcDepthMap[uiCam]->m_atV[i] = pow(2, m_apcDepthMap[uiCam]->m_uiBitsPerSample) / 2;
        }
        m_apcDepthMap[uiCam]->frameWriter(cParams.getFileOutputDepthMap(uiCam), (uiFrame != cParams.m_uiStartFrame));
    }

    return;
}

void cEstimator::keepDataFromCurrentFrame(cConfig &cParams, UInt uiFrame) {

    if ((cParams.m_uiTemporalEnhancement > 0) || (cParams.m_uiSkipFlag > 0)) {
        if (cParams.m_iTexturePrefilteringOrder == 3) readInputViews(cParams, uiFrame);
        saveTemporal(m_apcSuperpixels, m_apcSuperpixelsPrevFrame, m_apcSuperpixelsIFrame, m_apuiDepthMapLabel, m_apuiDepthMapLabelPrevFrame, m_apuiDepthMapLabelIFrame, uiFrame, cParams);
    }
    return;
}

void cEstimator::clearSegmentation(cConfig &cParams) {

    for (UInt uiCam = 0; uiCam < cParams.m_uiNumberOfCameras; uiCam++) {
        for (UInt uiSuperpixel = 0; uiSuperpixel < m_apcSuperpixels[uiCam]->m_uiNumberOfSuperpixels; uiSuperpixel++) {
            m_apcSuperpixels[uiCam]->m_apiSuperpixelNeigh[uiSuperpixel].clear();
            m_apcSuperpixels[uiCam]->m_apdSuperpixels.clear();
        }
        m_apcSuperpixels[uiCam]->m_apiSuperpixelNeigh.clear();
    }

    return;
}

void cEstimator::estimate(cConfig &cParams) {
    clock_t clkBegin, clkEnd;
    clkBegin = clock();

    for (UInt uiFrame = cParams.m_uiStartFrame; uiFrame < cParams.m_uiTotalNumberOfFrames + cParams.m_uiStartFrame; uiFrame++) {
        fprintf(stdout, "\nFrame %d of %d\n", uiFrame, cParams.m_uiTotalNumberOfFrames + cParams.m_uiStartFrame);

        // Input views read
        readInputViews(cParams, uiFrame);

        // Input Depth Maps read
        readInputDepthMaps(cParams, uiFrame);

        if (cParams.m_iTexturePrefilteringOrder == 1) prefilterTextures(cParams);

        // Segmentation calculation
        calcSegmentation(cParams, uiFrame);

        if (cParams.m_iTexturePrefilteringOrder == 2) prefilterTextures(cParams);

        // Encoder-derived features read
        if (cParams.m_uiFeatures) readFeatures(cParams, uiFrame);

        // Internal texture features calculation
        if (cParams.m_uiTemporalEnhancement == 2) calcTextureFeatures(cParams, uiFrame);

        if (cParams.m_uiSkipFlag || cParams.m_uiTemporalEnhancement == 2) calcTemporalFromFeatures(m_apcSuperpixels, m_apcSuperpixelsPrevFrame, m_apuiDepthMapLabel, m_apuiDepthMapLabelPrevFrame, uiFrame, cParams, m_aabSkip);

        if (cParams.m_iTexturePrefilteringOrder == 3) prefilterTextures(cParams);

        // Depth calculation
        calcDepth(cParams, uiFrame);

        // Write depth maps to file
        writeOutputDepths(cParams, uiFrame);

        // Save depth labels for temporal enhancement of future frames
        keepDataFromCurrentFrame(cParams, uiFrame);

        // Clear segmentation of current frame
        clearSegmentation(cParams);

    }

    clkEnd = clock();
    Double dClockResult = (clkEnd - clkBegin) / CLOCKS_PER_SEC;
    fprintf(stdout, "\nOverall time: %7.3f sec.\n", dClockResult);
}
