//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
  /*Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met
  *This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  Neither the name of PUT or ETRI nor the names of its contributors may
  be used to endorse or promote products derived from this software
  without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  */


#ifndef __CESTIMATOR_H__
#define __CESTIMATOR_H__

#include <iostream>
#include <fstream>
#include <time.h>
#include "../Common/CommonDef.h"
#include "paramsDE.h"

#include "../Segmentation/cSegmentation.h"
#include "../Segmentation/cSNICSuperpixel.h"

#include "cConfig.h"
#include "../Common/cCamParams.h"
#include "../Common/cYUV.h"
#include "../Common/cArray.h"

#include "cPixelMatcher.h"
#include "cBlockMatcher.h"
#include "cLocalizer.h"
#include "cGraphCutOptimizer.h"
#include "cTemporal.h"
#include "cFeatures.h"



class cEstimator {

  cArray<cCamParams<Double>*> m_apcCameraParameters;

  cLocalizer *m_localizer;
  cMatcher *m_matcher;
  cOptimizer *m_optimizer;

  cArray<cYUV<ImagePixelType>*> m_apcInputView;
  cArray<cYUV<ImagePixelType>*> m_apcInputViewPrevFrame;

  cArray<UInt*> m_apuiDepthMapLabel;
  cArray<UInt*> m_apuiDepthMapLabelPrevFrame;
  cArray<UInt*> m_apuiDepthMapLabelIFrame;
  cArray<UInt*> m_apuiDepthMapLabelMin;
  cArray<UInt*> m_apuiDepthMapLabelMax;
  cArray<UInt*> m_apuiInputDepthMapLabel;
  cArray<cArray<Bool>> m_aabSkip;

  cArray<cSegmentation*> m_apcSuperpixels;
  cArray<cSegmentation*> m_apcSuperpixelsPrevFrame;
  cArray<cSegmentation*> m_apcSuperpixelsIFrame;

  cArray<cYUV<DepthPixelType>*> m_apcDepthMap;
  cArray<cYUV<DepthPixelType>*> m_apcInputDepthMap;
  cArray<cYUV<DepthPixelType>*> m_apcZMin;
  cArray<cYUV<DepthPixelType>*> m_apcZMax;

  cArray<cFeatures*> m_featuresWorkers;

public:
  cEstimator(cConfig &cParams);

  void estimate(cConfig &cParams);

  void initInputViews(cConfig &cParams);
  void initDepthMapLabels(cConfig &cParams);
  void initInputDepthMaps(cConfig& cParams);
  void initOutputDepthMaps(cConfig &cParams);
  void initSegmentation(cConfig &cParams);
  void initFeatures(cConfig& cParams);
  void calcZnearZfar(cConfig& cParams);
  void updateParamsInFile(String sFilename, cConfig& cParams);

  void readInputViews(cConfig &cParams, UInt uiFrame);
  void readInputDepthMaps(cConfig& cParams, UInt uiFrame);
  void readFeatures(cConfig& cParams, UInt uiFrame);
  void parseFeaturesFlags(cConfig& cParams, Int iCam, std::stringstream& ss, UInt x, UInt y, UInt blkszX, UInt blkszY);
  void writeOutputDepths(cConfig &cParams, UInt uiFrame);

  void prefilterTextures(cConfig& cParams);

  void calcSegmentation(cConfig &cParams, UInt uiFrame);
  void calcDepth(cConfig &cParams, UInt uiFrame);
  void calcTextureFeatures(const cConfig& cParams, UInt uiFrame);

  void keepDataFromCurrentFrame(cConfig &cParams, UInt uiFrame);
  void clearSegmentation(cConfig &cParams);
};

#endif