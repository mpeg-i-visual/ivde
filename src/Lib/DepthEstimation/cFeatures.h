﻿//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/

#ifndef __CFEATURES_H__
#define __CFEATURES_H__

#include "../Common/CommonDef.h"
#include "../Common/cArray.h"
#include "../Common/cYUV.h"

#include "paramsDE.h"

#include <vector>


struct sFeaturesParams {
	sFeaturesParams() = default;

	size_t endAtW{ 0 };
	size_t endAtH{ 0 };

	size_t startAtW{ 0 };
	size_t startAtH{ 0 };

	size_t blockSize{ 0 };

	size_t splitThresh{ 0 };
	Float skipThresh{ 0.5 };
	size_t numberOfSplitLevels{ 1 };
	
	std::string viewCS{};

	UInt viewBPS{ 0 };
	UInt startFrame{ 0 };
};

class cBlock {
public:
	cBlock() = default;
	cBlock(UInt x, UInt y, UInt minVal_, UInt maxVal_, size_t blkSize_);
public:
	bool isSkipped{ false };

	UInt posW{ 0 }; 
	UInt posH{ 0 }; 

	UInt costVolume{ 0 };

	UInt minVal{ 0 };
	UInt maxVal{ 0 };

	size_t size{ 0 };
public:
	Bool operator == (const cBlock& rhs) const ;
};

class cSplitter {
private:
	static bool canSkip(const sFeaturesParams& featuresParams, const cYUV<ImagePixelType>* const prevFrame, const cYUV<ImagePixelType>* const currFrame, const cBlock& currBlock);
public:
	static bool split(sFeaturesParams featuresParams, const cYUV<ImagePixelType>* const inputView, cArray<cBlock>& outputBlocks);
	static void skip(const sFeaturesParams& featuresParams, const cYUV<ImagePixelType>* const prevFrame, const cArray<cBlock>& prevFrameBlocks, const cYUV<ImagePixelType>* const currFrame, cArray<cBlock>& currFrameBlocks);
private:
	cSplitter() {}
};


class cFeatures {
private:	
	cYUV<ImagePixelType>* m_prevFrame{ nullptr };
	cArray<cArray<cBlock>> m_blocks;
	sFeaturesParams m_params;

public:
	cFeatures() = default;
	cFeatures(const sFeaturesParams& featuresParams);

	void extract(const cYUV<ImagePixelType>* const currFrame,  cArray<Bool>& outputSkipMask, UInt frame);
};

#endif
