//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */


#ifndef __CGRAPHCUTOPTIMIZER_H__
#define __CGRAPHCUTOPTIMIZER_H__

#include "../Common/TypeDef.h"
#include "cOptimizer.h"
#include "graph.h"
#include <cmath>
#include "../Common/cMatrix.h"

class cGraphCutOptimizer : public cOptimizer {

    UInt m_iOcculsion;
    UInt m_uiTemporalEnhancementMethod;
    UInt m_uiNumberOfCyclesInIFrame;
    UInt m_uiNumberOfCycles;
    UInt m_uiMatchingThresh;
    UInt m_uiHorizontalPrecision;
    UInt m_uiVerticalPrecision;
    UInt m_uiNumberOfThreads;
    UInt m_uiParallelizationType;
    UInt  m_uiFeatures;

    Bool ***m_ppbGraphNodesActive;

    graph::node_id ***m_ppcGraphNodes;

    cArray<cMatrix<Double>*> m_apmatIPP;

    long double **LUT_Theta;
    long double **LUT_Phi;
    cArray<cArray<cMatrix<Double>*>> m_mmOmniRR;
    cArray<cArray<cMatrix<Double>*>> m_mmOmniT;

    Double m_dAoVL;
    Double m_dAoVR;
    Double m_dAoVT;
    Double m_dAoVB;

    Double m_dAoVW;
    Double m_dAoVH;

    ~cGraphCutOptimizer();
public:


    Void init(cConfig &rcConfig, cMatcher *pcMatcher, cArray<cCamParams<Double>*> &rapcCameraParameters);
    Void optimize(cConfig &cfg, cArray<cYUV<ImagePixelType>*> &rapcInputView, cArray<cCamParams<Double>*> &rapcCameraParameters, cArray<UInt*> &rapuiDepthMapLabel, cArray<UInt*> &rapuiDepthMapLabelPrevFrame, cArray<UInt*> &rapuiInputDepthMapLabel, cArray<UInt*> &rapuiDepthMapLabelMin, cArray<UInt*> &rapuiDepthMapLabelMax, cArray<cArray<Bool>> &aabSkip, cArray<cSegmentation*> &rapcSuperpixels, UInt uiFrame);
    Void calcImPosPersp(Double &wPos, Double &hPos, Double invTargetZ, cMatrix<Double> *XYZ, cMatrix<Double> *mat);
    Void calcXYZPersp(UInt w, UInt h, Double invZ, cMatrix<Double> *XYZ, cMatrix<Double> *mat);
    Void calcImPosOmni(UInt w, UInt h, Double &wPos, Double &hPos, UInt W, UInt H, Double dInvZ, UInt uiCam, UInt uiTargetCam);
};

#endif