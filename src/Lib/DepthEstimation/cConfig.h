//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/
	 

#ifndef __CCONFIG_H__
#define __CCONFIG_H__

#include "../Common/Json.h"
#include "../Common/TypeDef.h"
#include "../Common/CommonDef.h"
#include "../Common/cArray.h"
#include "cFeatures.h"
#include <filesystem>
#include "paramsDE.h"

class cConfig{
public:
    UInt m_uiNumberOfThreads;
    UInt m_uiNumberOfThreadsSegm;
    UInt m_uiParallelizationType;
    UInt m_uiStartFrame;
    UInt m_uiTotalNumberOfFrames;

    UInt m_uiNumberOfCameras;
    UInt m_uiCenterCam;

    cArray<String> m_asCameraNames;
    cArray<String> m_asFileInputViews;
    cArray<String> m_asFileInputViewsType;
    cArray<UInt> m_auiFileInputViewsHeight;
    cArray<UInt> m_auiFileInputViewsWidth;
    cArray<Double> m_adNearestDepthValue;
    cArray<Double> m_adFarthestDepthValue;
    cArray<UInt> m_auiFileInputViewsNumOfSuperpixels;
    cArray<String> m_asFileFeatures;
    cArray<String> m_asFileOutputDepthMaps;
    cArray<UInt> m_auiBitDepthColor;
    cArray<UInt> m_auiBitDepthDepth;
    cArray<String> m_asColorSpace;
    cArray<String> m_asDepthColorSpace;
    cArray<String> m_asFileInputDepthMaps;

    Double m_dAoVL;
    Double m_dAoVR;
    Double m_dAoVT;
    Double m_dAoVB;

    Double m_dAoVW;
    Double m_dAoVH;

    UInt m_uiInputBitDepth;

    String m_sFileCameraParameter;

    Double m_dNearestDepthValue;
    Double m_dFarthestDepthValue;
    UInt m_uiNumberOfDepthSteps;

    String m_sMatcher;
    Int m_iMatchNeighbors;

    UInt m_uiMatchingBlockSize;

    Double m_dSmoothingCoefficient;

    UInt m_uiMatchingThresh;

    UInt m_uiSuperpixelSmoothing;
    String m_sSuperpixelSegmentationType;
    Double m_dSuperpixelColorCoeff;
    UInt m_uiNumberOfSuperpixels;

    UInt m_uiNumberOfCycles;
    UInt m_uiNumberOfCyclesInIFrame;

    UInt m_uiTemporalEnhancement = 0;
    UInt m_uiTemporalEnhancementIFramePeriod;
    Double m_dTemporalEnhancementThresh;

    UInt m_uiNeighboringSegmentsDepthAnalysis;
    UInt m_uiPoint2BlockMatching = false;
    UInt m_uiAutomaticDepthRange = false;
    UInt m_uiFeatures = false;
    UInt m_uiSkipFlag = false;
    UInt m_uiInputDepthMap = false;
    UInt m_uiBitDepthFeatures = 16;

    Int m_iTexturePrefilteringOrder = 1;
    Int m_iTexturePrefilteringBlockWidth = 1;

    sFeaturesParams m_featuresParams;

    UInt m_uiBitDepthDepthUpdated = false;
    UInt m_uiDepthColorSpaceUpdated = false;

    Bool m_validateError = false;
    Bool m_validateWarning = false;
    std::ostringstream m_errorStream;
    std::ostringstream m_warningStream;

    cConfig(String cfgFilename, String cfgSeqFilename, String cfgFilenamesFilename);

    Void displayParams(String cfgFilename);
    String getFileInputView(UInt uiCam)         { return m_asFileInputViews[uiCam]; };
    String getFileInputViewType(UInt uiCam)     { return m_asFileInputViewsType[uiCam]; };
    String getFileInputDepthMaps(UInt uiCam)    { return m_asFileInputDepthMaps[uiCam]; }
    UInt getFileInputViewHeight(UInt uiCam)     { return m_auiFileInputViewsHeight[uiCam]; };
    UInt getFileInputViewWidth(UInt uiCam)      { return m_auiFileInputViewsWidth[uiCam]; };
    String getFileOutputDepthMap(UInt uiCam)    { return m_asFileOutputDepthMaps[uiCam]; };

    Void fileExists(const String& filename);
};

#endif
