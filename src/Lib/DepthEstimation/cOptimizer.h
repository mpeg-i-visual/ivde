//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */


#ifndef __COPTIMIZER_H__
#define __COPTIMIZER_H__

#include "../Common/TypeDef.h"
#include "paramsDE.h"
#include "../Common/cArray.h"
#include "../DepthEstimation/cMatcher.h"
#include "../Common/cYUV.h"
#include "../Segmentation/cSegmentation.h"

class cOptimizer {
public:
    UInt m_uiNumberOfCameras;
    UInt m_uiCenterCam;
    UInt m_uiStartFrame;

    Double m_dSmoothingCoeff;
    Int m_iMatchNeighbors;
    cArray<UInt*> m_auiNeighbors;
    UInt m_uiIFramePeriod;

    UInt m_uiNumberOfLabels;
    UInt *m_auiLabel2Depth;
    UInt *m_auiDepth2Label;
    Double *m_adLabel2Z;
    Double *m_adLabel2InvZ;

    cMatcher* m_pcMatcher;

    cOptimizer();
    virtual ~cOptimizer();

    virtual Void init(cConfig &cfg, cMatcher *matcher, cArray<cCamParams<Double>*> &rapcCameraParameters) = 0;
    Void calcDepth(cConfig &cfg, cArray<cYUV<DepthPixelType>*> &YUV_dm, cArray<UInt*> &rapuiDepthMapLabel, cArray<cSegmentation*> &rapcSuperpixels);

    virtual Void optimize(cConfig &cfg, cArray<cYUV<ImagePixelType>*> &rapcInputView, cArray<cCamParams<Double>*> &rapcCameraParameters, cArray<UInt*> &rapuiDepthMapLabel, cArray<UInt*> &rapuiDepthMapLabelPrevFrame, cArray<UInt*> &rapuiInputDepthMapLabel, cArray<UInt*> &rapuiDepthMapLabelMin, cArray<UInt*> &rapuiDepthMapLabelMax, cArray<cArray<Bool>> &aabSkip, cArray<cSegmentation*> &rapcSuperpixels, UInt uiFrame) = 0;
};

#endif