//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */


#include "cConfig.h"
#include "../Common/Json.h"

#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;
using namespace TMIV::Common;

cConfig::cConfig(String cfgFilename, String cfgSeqFilename, String cfgFilenamesFilename) {

    fprintf(stdout, "####  IVDE v7.0  ####\n\n");

    fileExists(cfgFilename);
    fileExists(cfgSeqFilename);
    fileExists(cfgFilenamesFilename);

    if (m_validateError) {
        fprintf(stdout, "%s", m_errorStream.str().c_str());
        exit(1);
    }

    fprintf(stdout, "reading %s \n", cfgFilename.c_str());
    ifstream stream{ cfgFilename };
    Json params;// = Json{ stream };               
    try {
        params = Json{ stream };
    }
    catch (const std::runtime_error& e) {
        fprintf(stdout, "ERROR: %s", e.what());
    }


    fprintf(stdout, "reading %s \n", cfgSeqFilename.c_str());
    ifstream Seqstream{ cfgSeqFilename };
    Json Seqparams;// = Json{ Seqstream };
    try {
        Seqparams = Json{ Seqstream };
    }
    catch (const std::runtime_error& e) {
        fprintf(stdout, "ERROR: %s", e.what());
    }


    fprintf(stdout, "reading %s \n\n", cfgFilenamesFilename.c_str());
    ifstream Filenamesstream{ cfgFilenamesFilename };
    Json Filenames;// = Json{ Filenamesstream };
    try {
        Filenames = Json{ Filenamesstream };
    }
    catch (const std::runtime_error& e) {
        fprintf(stdout, "ERROR: %s", e.what());
    }



    //Default Value
    m_uiNumberOfThreads = 1;
    m_uiParallelizationType = 1;
    m_uiNumberOfSuperpixels = 1000;
    m_dSuperpixelColorCoeff = 0.1;
    m_uiSuperpixelSmoothing = 0;
    m_sSuperpixelSegmentationType = "SNIC";
    m_uiNumberOfCycles = 1;
    m_uiTemporalEnhancement = 0;
    m_uiNeighboringSegmentsDepthAnalysis = 0;
    m_uiTemporalEnhancementIFramePeriod = 1;
    m_dTemporalEnhancementThresh = 0;
    m_iMatchNeighbors = 1;
    m_uiMatchingThresh = 20;

    m_featuresParams.blockSize = 64;
    m_featuresParams.skipThresh = 0.5;
    m_featuresParams.splitThresh = 1024;
    m_featuresParams.numberOfSplitLevels = 1;

    m_dAoVL = -PI;
    m_dAoVR = PI;
    m_dAoVT = PI_HALF;
    m_dAoVB = -PI_HALF;

    ///////////////
    // Estimation params
    m_asCameraNames.clear();
    m_asFileInputViews.clear();
    m_asFileInputViewsType.clear();
    m_asFileInputDepthMaps.clear();
    m_uiNumberOfThreads = params.require("NumOfThreads").asInt();
    m_uiStartFrame = params.require("StartFrame").asInt();
    m_uiTotalNumberOfFrames = params.require("TotalNumberOfFrames").asInt();
    m_sFileCameraParameter = cfgSeqFilename;
    m_uiNumberOfDepthSteps = params.require("NumberOfZSteps").asInt();
    m_sMatcher = params.require("Matcher").asString();
    m_iMatchNeighbors = params.require("MatchNeighbors").asInt();
    m_uiMatchingThresh = params.require("MatchThresh").asDouble();
    m_uiMatchingBlockSize = params.require("MatchingBlockSize").asInt();
    m_sSuperpixelSegmentationType = params.require("SuperpixelSegmentationType").asString();
    m_uiNumberOfSuperpixels = params.require("NumberOfSuperpixels").asInt();
    m_dSuperpixelColorCoeff = params.require("SuperpixelColorCoeff").asDouble();
    m_uiNumberOfCycles = params.require("NumberOfCycles").asInt();
    m_uiNumberOfCyclesInIFrame = params.require("NumberOfCyclesInIFrame").asInt();
    m_uiTemporalEnhancement = params.require("TemporalEnhancement").asInt();
    m_uiTemporalEnhancementIFramePeriod = params.require("TemporalEnhancementIFramePeriod").asInt();
    m_dTemporalEnhancementThresh = params.require("TemporalEnhancementThresh").asDouble();
    m_uiNeighboringSegmentsDepthAnalysis = params.require("NeighboringSegmentsDepthAnalysis").asBool();
    m_uiNumberOfThreadsSegm = m_uiNumberOfThreads;
    // Optional estimation params
    if (params.isPresent("NumOfThreadsSegm")) m_uiNumberOfThreadsSegm = params.require("NumOfThreadsSegm").asInt();
    if (params.isPresent("Point2BlockMatching")) m_uiPoint2BlockMatching = params.require("Point2BlockMatching").asBool();
    if (params.isPresent("AutomaticDepthRange")) m_uiAutomaticDepthRange = params.require("AutomaticDepthRange").asBool();
    if (params.isPresent("UseFeatures")) {
        m_uiFeatures = params.require("UseFeatures").asBool();
        m_uiBitDepthFeatures = params.require("BitDepthFeatures").asInt();
    }
    if (params.isPresent("UseSkipFlag"))                    m_uiSkipFlag = params.require("UseSkipFlag").asBool();
    if (params.isPresent("TexturePrefilteringOrder"))       m_iTexturePrefilteringOrder = params.require("TexturePrefilteringOrder").asInt();
    if (params.isPresent("TexturePrefilteringBlockWidth"))  m_iTexturePrefilteringBlockWidth = params.require("TexturePrefilteringBlockWidth").asInt();
    if (params.isPresent("UseInputDepthMap")) {
        m_uiInputDepthMap = params.require("UseInputDepthMap").asBool();
        m_uiBitDepthFeatures = params.require("BitDepthFeatures").asInt();
    }
    
    if ((m_uiTemporalEnhancement == 2) && params.isPresent("FeaturesSplitThresh"))            m_featuresParams.splitThresh = params.require("FeaturesSplitThresh").asInt();
    if ((m_uiTemporalEnhancement == 2) && params.isPresent("FeaturesSkipThresh"))             m_featuresParams.skipThresh = params.require("FeaturesSkipThresh").asFloat();
    if ((m_uiTemporalEnhancement == 2) && params.isPresent("FeaturesBlockSize"))              m_featuresParams.blockSize = params.require("FeaturesBlockSize").asInt();
    if ((m_uiTemporalEnhancement == 2) && params.isPresent("FeaturesNumberOfSplitLevels"))    m_featuresParams.numberOfSplitLevels = params.require("FeaturesNumberOfSplitLevels").asInt();
    
    // Sequence params
    UInt uiCamTemp = 0;
    for (UInt uiCam = 0; uiCam < Seqparams.require("cameras").size(); uiCam++) {
        if ((Seqparams.require("cameras").at(uiCam).require("Name").asString() == "viewport") || 
            (Seqparams.require("cameras").at(uiCam).require("Name").asString() == "center")) continue;
        
            m_asCameraNames.push_back(Seqparams.require("cameras").at(uiCam).require("Name").asString());

            m_asFileInputViews.push_back(Filenames.require("filenames").at(uiCamTemp).require("InputView").asString());
            fileExists(m_asFileInputViews.back());

            m_asFileInputViewsType.push_back(Seqparams.require("cameras").at(uiCam).require("Projection").asString());

            if (Filenames.require("filenames").at(uiCamTemp).isPresent("InputDepthMap")) {
                m_asFileInputDepthMaps.push_back(Filenames.require("filenames").at(uiCamTemp).require("InputDepthMap").asString());
                fileExists(m_asFileInputDepthMaps.back());
            }
            else m_asFileInputDepthMaps.push_back("");

            m_auiFileInputViewsHeight.push_back(Seqparams.require("cameras").at(uiCam).require("Resolution").asIntVector<2>()[1]);
            m_auiFileInputViewsWidth.push_back(Seqparams.require("cameras").at(uiCam).require("Resolution").asIntVector<2>()[0]);

            if (!m_uiAutomaticDepthRange) m_adNearestDepthValue.push_back(Seqparams.require("cameras").at(uiCam).require("Depth_range").asDoubleVector<2>()[0]);
            if (!m_uiAutomaticDepthRange) m_adFarthestDepthValue.push_back(Seqparams.require("cameras").at(uiCam).require("Depth_range").asDoubleVector<2>()[1]);
            
            m_asFileOutputDepthMaps.push_back(Filenames.require("filenames").at(uiCamTemp).require("OutputDepthMap").asString());
            
            if ((m_uiFeatures) && Filenames.require("filenames").at(uiCamTemp).isPresent("Features")) {
                m_asFileFeatures.push_back(Filenames.require("filenames").at(uiCamTemp).require("Features").asString());
                fileExists(m_asFileFeatures.back());
            }
            else m_asFileFeatures.push_back("");

            m_auiBitDepthColor.push_back(Seqparams.require("cameras").at(uiCam).require("BitDepthColor").asInt());
            if (Seqparams.require("cameras").at(uiCam).isPresent("BitDepthDepth")) {
                m_auiBitDepthDepth.push_back(Seqparams.require("cameras").at(uiCam).require("BitDepthDepth").asInt());
            }
            else if (params.isPresent("BitDepthDepth")) {
                m_auiBitDepthDepth.push_back(params.require("BitDepthDepth").asInt());
                m_uiBitDepthDepthUpdated = true;
            }
            else {
                cout << endl << "!BitDepthDepth is not present in configuration files!" << endl << endl;
                exit(1);
            }
            
            m_asColorSpace.push_back(Seqparams.require("cameras").at(uiCam).require("ColorSpace").asString());

            if (Seqparams.require("cameras").at(uiCam).isPresent("DepthColorSpace")) {
                m_asDepthColorSpace.push_back(Seqparams.require("cameras").at(uiCam).require("DepthColorSpace").asString());
            }
            else if (params.isPresent("DepthColorSpace")) {
                m_asDepthColorSpace.push_back(params.require("DepthColorSpace").asString());
                m_uiDepthColorSpaceUpdated = true;
            }
            else {
                cout << endl << "!DepthColorSpace is not present in configuration files!" << endl << endl;
                exit(1);
            }

            uiCamTemp++;
    }

    if (!m_uiAutomaticDepthRange) {
        m_dNearestDepthValue = m_adNearestDepthValue[0];
        m_dFarthestDepthValue = m_adFarthestDepthValue[0];

        uiCamTemp = 0;
        for (UInt uiCam = 0; uiCam < Seqparams.require("cameras").size(); uiCam++) {
            if ((Seqparams.require("cameras").at(uiCam).require("Name").asString() == "viewport") || (Seqparams.require("cameras").at(uiCam).require("Name").asString() == "center")) continue;
            if (abs(m_adNearestDepthValue[uiCamTemp]) < abs(m_dNearestDepthValue))m_dNearestDepthValue = m_adNearestDepthValue[uiCamTemp];
            if (abs(m_adFarthestDepthValue[uiCamTemp]) > abs(m_dFarthestDepthValue))m_dFarthestDepthValue = m_adFarthestDepthValue[uiCamTemp];
            uiCamTemp++;
        }
    }

    if (!m_uiAutomaticDepthRange)m_dSmoothingCoefficient = 7.0 / m_dFarthestDepthValue;

    uiCamTemp = 0;
    for (UInt uiCam = 0; uiCam < Seqparams.require("cameras").size(); uiCam++) {
        if ((Seqparams.require("cameras").at(uiCam).require("Name").asString() == "viewport") || (Seqparams.require("cameras").at(uiCam).require("Name").asString() == "center")) continue;
        if (m_asFileInputViewsType[uiCamTemp] == "Equirectangular") {
            if (!m_uiAutomaticDepthRange)m_dSmoothingCoefficient *= 1.5;
            m_dAoVL = Seqparams.require("cameras").at(uiCam).require("Hor_range").asDoubleVector<2>()[0] / 180.0 * PI;
            m_dAoVR = Seqparams.require("cameras").at(uiCam).require("Hor_range").asDoubleVector<2>()[1] / 180.0 * PI;
            m_dAoVT = Seqparams.require("cameras").at(uiCam).require("Ver_range").asDoubleVector<2>()[1] / 180.0 * PI;
            m_dAoVB = Seqparams.require("cameras").at(uiCam).require("Ver_range").asDoubleVector<2>()[0] / 180.0 * PI;
            break;
        }
        uiCamTemp++;
    }

    if (Seqparams.isPresent("Smoothing")) {
        fprintf(stdout, "\tAuto smoothing\t\t\t%f\n", m_dSmoothingCoefficient);
        m_dSmoothingCoefficient = Seqparams.require("Smoothing").asDouble();
        fprintf(stdout, "\treplaced by config smoothing\t%f\n", m_dSmoothingCoefficient);
    }

    m_uiNumberOfCameras = (UInt)m_asCameraNames.size();
    m_dAoVW = m_dAoVR - m_dAoVL;
    m_dAoVH = m_dAoVT - m_dAoVB;

    if (m_validateWarning) fprintf(stdout, "%s", m_warningStream.str().c_str());
    if (m_validateError) {
        fprintf(stdout, "%s", m_errorStream.str().c_str());
        exit(1);
    }

    if (m_uiTemporalEnhancement == 1 && m_uiSkipFlag) {
        m_uiTemporalEnhancement = 0;
        cout << endl << "!TemporalEnhancement was turned off because SkipFlag is used!" << endl << endl;
    }

    if (m_uiFeatures) {
        if (m_uiTemporalEnhancement == 1) {
            m_uiTemporalEnhancement = 0;
            cout << endl << "!TemporalEnhancement was turned off because encoder-derived features are used!" << endl << endl;
        }
        if (m_uiSkipFlag == 0) {
            m_uiSkipFlag = 1;
            cout << endl << "!SkipFlag was turned on because encoder-derived features are used!" << endl << endl;
        }
    }

    return;
}

Void cConfig::displayParams(String cfgFilename) {
    fprintf(stdout, "####  CONFIG PARAMETERS  ####\n");

    fprintf(stdout, "\n");

    fprintf(stdout, "\tStart frame\t\t\t%d\n", m_uiStartFrame);
    fprintf(stdout, "\tTotal number of frames\t\t%d\n", m_uiTotalNumberOfFrames);

    fprintf(stdout, "\n");

    fprintf(stdout, "\tNumber of cameras\t\t%d\n", m_uiNumberOfCameras);
    fprintf(stdout, "\tConfig file\t\t\t%s\n", cfgFilename.c_str());
    fprintf(stdout, "\tSequence parameters file\t\t%s\n", m_sFileCameraParameter.c_str());

    fprintf(stdout, "\n");

}

Void cConfig::fileExists(const String& filename)
{
    std::filesystem::path path(filename);
    if (!std::filesystem::exists(path)) {
        m_errorStream << "ERROR: No such file or directoy: \t" << path << std::endl;
        m_validateError = true;
    }
    else return;

}
