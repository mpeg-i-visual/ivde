//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */


#include "cPixelMatcher.h"
#include <limits>

const double MAX_DOUBLE = std::numeric_limits< double >::max();

Void cPixelMatcher::init(cConfig &cfg) {
    cMatcher::init(cfg);

    m_dMaxError = ((1 << cfg.m_auiBitDepthColor[0]) - 1);;
    return;
}

Double cPixelMatcher::matchingError(cConfig &cfg, cArray<cYUV<ImagePixelType>*> &rapcInputView, UInt uiCam, UInt uiTargetCam, Int uiX, Int uiY, Int uiXInTargetCam, Int uiYInTargetCam) {

    cYUV<ImagePixelType>* pcInputViewSourceView = rapcInputView[uiCam];
    cYUV<ImagePixelType>* pcInputViewTargetView = rapcInputView[uiTargetCam];

#if CHROMA_MATCH
    UInt uiChromaHShift = pcInputViewSourceView->getChrominanceHorizontalShift();
    UInt uiChromaVShift = pcInputViewSourceView->getChrominanceVerticalShift();
#endif

    UInt uiPosHinSourceView = uiY;
    UInt uiPosWinSourceView = uiX;
    UInt uiPixelPositionInSourceView = uiPosHinSourceView * cfg.getFileInputViewWidth(uiCam) + uiPosWinSourceView;

    Int uiPosHinTargetView = uiYInTargetCam;
    Int uiPosWinTargetView = uiXInTargetCam;
    UInt uiPixelPositionInTargetView = uiPosHinTargetView * cfg.getFileInputViewWidth(uiTargetCam) + uiPosWinTargetView;

    Double dMatchingError = ((1 << cfg.m_auiBitDepthColor[uiCam]) - 1);

    Int blockSize = 1;
    Int range = 0;

    if (cfg.m_uiPoint2BlockMatching) {
        blockSize = 3;
        range = blockSize / 2;
    }

    cArray<cArray<Double>> dMatchingErrorArray;

    dMatchingErrorArray.resize(blockSize);
    for (Int uiTempY = 0; uiTempY < blockSize; uiTempY++) {
        dMatchingErrorArray[uiTempY].resize(blockSize);
    }

    for (Int uiTempY = 0; uiTempY < blockSize; uiTempY++) {
        for (Int uiTempX = 0; uiTempX < blockSize; uiTempX++) {
            dMatchingErrorArray[uiTempY][uiTempX] = 0.0;
        }
    }

    //Double dMatchingErrorArray[blockSize][blockSize] = { 0 };
    int counterX = 0;
    int counterY = 0;

    for (uiPosHinTargetView = uiYInTargetCam - range; uiPosHinTargetView <= uiYInTargetCam + range; uiPosHinTargetView++)
    {
        for (uiPosWinTargetView = uiXInTargetCam - range; uiPosWinTargetView <= uiXInTargetCam + range; uiPosWinTargetView++)
        {

            if (uiPosWinTargetView<0 || uiPosHinTargetView<0 || uiPosWinTargetView>Int(cfg.getFileInputViewWidth(uiCam)) || uiPosHinTargetView>Int(cfg.getFileInputViewHeight(uiTargetCam)))
            {
                dMatchingError = MAX_DOUBLE;
            }
            else
            {

                if (uiPosWinTargetView >= 0 && uiPosWinTargetView < Int(cfg.getFileInputViewWidth(uiTargetCam)) &&
                    uiPosHinTargetView >= 0 && uiPosHinTargetView < Int(cfg.getFileInputViewHeight(uiTargetCam))) {
                    uiPixelPositionInTargetView = uiPosHinTargetView * cfg.getFileInputViewWidth(uiTargetCam) + uiPosWinTargetView;
                    dMatchingError = Abs(pcInputViewTargetView->m_atY[uiPixelPositionInTargetView] -
                        pcInputViewSourceView->m_atY[uiPixelPositionInSourceView]);
#if GRADIENT_MATCH

                    if (uiPosWinSourceView >= 1 && uiPosWinTargetView >= 1) {
                        dMatchingError += Abs(pcInputViewTargetView->m_atY[uiPixelPositionInTargetView] -
                            pcInputViewSourceView->m_atY[uiPixelPositionInSourceView] -
                            pcInputViewTargetView->m_atY[uiPixelPositionInTargetView - 1] +
                            pcInputViewSourceView->m_atY[uiPixelPositionInSourceView - 1]);
                    }
                    if (uiPosHinSourceView >= 1 && uiPosHinTargetView >= 1) {
                        dMatchingError += Abs(pcInputViewTargetView->m_atY[uiPixelPositionInTargetView] -
                            pcInputViewSourceView->m_atY[uiPixelPositionInSourceView] -
                            pcInputViewTargetView->m_atY[uiPixelPositionInTargetView - cfg.getFileInputViewWidth(uiTargetCam)] +
                            pcInputViewSourceView->m_atY[uiPixelPositionInSourceView - cfg.getFileInputViewWidth(uiCam)]);
                    }
#endif
#if CHROMA_MATCH
                    UInt uiPosWinSourceViewUV = uiPosWinSourceView >> uiChromaHShift;
                    UInt uiPosHinSourceViewUV = uiPosHinSourceView >> uiChromaVShift;
                    UInt uiPixelPositionInSourceViewUV = uiPosHinSourceViewUV * (cfg.getFileInputViewWidth(uiCam) >> uiChromaHShift) + uiPosWinSourceViewUV;

                    UInt uiPosWinTargetViewUV = uiPosWinTargetView >> uiChromaHShift;
                    UInt uiPosHinTargetViewUV = uiPosHinTargetView >> uiChromaVShift;
                    UInt uiPixelPositionInTargetViewUV = uiPosHinTargetViewUV * (cfg.getFileInputViewWidth(uiTargetCam) >> uiChromaHShift) + uiPosWinTargetViewUV;

                    dMatchingError += Abs(pcInputViewTargetView->m_atU[uiPixelPositionInTargetViewUV] -
                        pcInputViewSourceView->m_atU[uiPixelPositionInSourceViewUV]);
                    dMatchingError += Abs(pcInputViewTargetView->m_atV[uiPixelPositionInTargetViewUV] -
                        pcInputViewSourceView->m_atV[uiPixelPositionInSourceViewUV]);
#endif
#if GRADIENT_MATCH && CHROMA_MATCH

                    if (uiPosWinSourceViewUV >= 1 && uiPosWinTargetViewUV >= 1) {
                        dMatchingError += Abs(pcInputViewTargetView->m_atU[uiPixelPositionInTargetViewUV] -
                            pcInputViewSourceView->m_atU[uiPixelPositionInSourceViewUV] -
                            pcInputViewTargetView->m_atU[uiPixelPositionInTargetViewUV - 1] +
                            pcInputViewSourceView->m_atU[uiPixelPositionInSourceViewUV - 1]);
                        dMatchingError += Abs(pcInputViewTargetView->m_atV[uiPixelPositionInTargetViewUV] -
                            pcInputViewSourceView->m_atV[uiPixelPositionInSourceViewUV] -
                            pcInputViewTargetView->m_atV[uiPixelPositionInTargetViewUV - 1] +
                            pcInputViewSourceView->m_atV[uiPixelPositionInSourceViewUV - 1]);
                    }
                    if (uiPosHinSourceViewUV >= 1 && uiPosHinTargetViewUV >= 1) {
                        dMatchingError += Abs(pcInputViewTargetView->m_atU[uiPixelPositionInTargetViewUV] -
                            pcInputViewSourceView->m_atU[uiPixelPositionInSourceViewUV] -
                            pcInputViewTargetView->m_atU[uiPixelPositionInTargetViewUV - (cfg.getFileInputViewWidth(uiTargetCam) >> uiChromaHShift)] +
                            pcInputViewSourceView->m_atU[uiPixelPositionInSourceViewUV - (cfg.getFileInputViewWidth(uiCam) >> uiChromaHShift)]);
                        dMatchingError += Abs(pcInputViewTargetView->m_atV[uiPixelPositionInTargetViewUV] -
                            pcInputViewSourceView->m_atV[uiPixelPositionInSourceViewUV] -
                            pcInputViewTargetView->m_atV[uiPixelPositionInTargetViewUV - (cfg.getFileInputViewWidth(uiTargetCam) >> uiChromaHShift)] +
                            pcInputViewSourceView->m_atV[uiPixelPositionInSourceViewUV - (cfg.getFileInputViewWidth(uiCam) >> uiChromaHShift)]);
                    }
#endif

                }

                dMatchingErrorArray[counterX][counterY] = dMatchingError;
            }

            if (counterX < (blockSize - 1)) counterX++;
            else
            {
                counterY++;
                counterX = 0;
            }

        }

    }

    dMatchingError = ((1 << cfg.m_auiBitDepthColor[uiCam]) - 1);

    for (Int y = 0; y < blockSize; y++)
    {
        for (Int x = 0; x < blockSize; x++)
        {
            if (dMatchingErrorArray[x][y] < dMatchingError)
                dMatchingError = dMatchingErrorArray[x][y];
        }
    }

    return dMatchingError;
}