#include "cSNICSuperpixel.h"
#include <cmath>
#include <math.h>

#include "../Common/TypeDef.h"
#include "../Common/cYUV.h"
#include "../Common/cArray.h"
#include "../DepthEstimation/paramsDE.h"

//=================================================================================
//  snic_mex.cpp
//
//
//  AUTORIGHTS
//  Copyright (C) 2016 Ecole Polytechnique Federale de Lausanne (EPFL), Switzerland.
//
//  Created by Radhakrishna Achanta on 05/November/16 (firstname.lastname@epfl.ch)
//
//
//  Code released for research purposes only. For commercial purposes, please
//  contact the author.
//=================================================================================
/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of EPFL nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <cmath>
#include <cfloat>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

//===========================================================================
/// FindSeeds
//===========================================================================
Void cSNICSuperpixel::FindSeedsOmni(int width, int height, int& numk, std::vector<int>& kx, std::vector<int>& ky, cConfig &cParams)
{
    const Double Perimeter = width * cParams.m_dAoVW / (2.0 * PI);
    const Double Radius = Perimeter * (Double)1.0 / (Double)(2.0 * PI);
    const Double AreaSphere = (Double)(4 * PI)* (Radius*Radius);
    const Double AreaSegment = AreaSphere / numk;
    const Double GridStep = sqrt(AreaSegment) / (cParams.m_dAoVW / (2.0 * PI)) / (cParams.m_dAoVH / PI);

    Double LongitudeGridStep = GridStep * (2 * cParams.m_dAoVH / PI * height) / (width);
    Double LongitudeSteps = round(height / LongitudeGridStep);
    LongitudeGridStep = height / LongitudeSteps;
    Double LongitudeStartStep = (height - LongitudeGridStep * (LongitudeSteps - 1)) / 2;

    int IdxK = 0;
    for (Double y = LongitudeStartStep; y < height; y += LongitudeGridStep)
    {
        Double Latitude = (0.5f - ((y) / height)) * cParams.m_dAoVH;

        Double ScaleFactor = cos(Latitude);

        Double LatitudalGridStep = GridStep / ScaleFactor;
        int LatitudalSteps = round(width / LatitudalGridStep);
        LatitudalGridStep = width / LatitudalSteps;
        Double LatitudalStartStep = UInt(LatitudalGridStep / 2);// (width - LatitudalGridStep * (LatitudalSteps - 1)) / 2;
        for (int x = LatitudalStartStep; x < width; x += LatitudalGridStep)
        {
            IdxK++;
        }
    }
    kx.resize(IdxK); ky.resize(IdxK);
    IdxK = 0;
    for (Double y = LongitudeStartStep; y < height; y += LongitudeGridStep)
    {
        Double Latitude = (0.5f - ((y) / height)) * cParams.m_dAoVH;
        Double ScaleFactor = cos(Latitude);
        Double LatitudalGridStep = GridStep / ScaleFactor;
        int LatitudalSteps = round(width / LatitudalGridStep);
        LatitudalGridStep = width / LatitudalSteps;
        Double LatitudalStartStep = UInt(LatitudalGridStep / 2);// (width - LatitudalGridStep * (LatitudalSteps - 1)) / 2;

        int CoordY = int(y);
        for (Double x = LatitudalStartStep; x < width; x += LatitudalGridStep)
        {
            int CoordX = (int)(x);
            kx[IdxK] = CoordX;
            ky[IdxK] = CoordY;
            IdxK++;
        }
    }
    numk = IdxK-1;
    kx.resize(IdxK); ky.resize(IdxK);
    return;
}

Void cSNICSuperpixel::FindSeeds(const int width, const int height, int& numk, std::vector<int>& kx, std::vector<int>& ky)
{
    const int sz = width*height;
    int gridstep = (int)(sqrt(double(sz) / double(numk)) + 0.5);
    int halfstep = gridstep / 2;
    double h = height; double w = width;

    int xsteps = int(width / gridstep);
    int ysteps = int(height / gridstep);
    int err1 = abs(xsteps*ysteps - numk);
    int err2 = abs(int(width / (gridstep - 1))*int(height / (gridstep - 1)) - numk);
    if (err2 < err1)
    {
        gridstep -= 1;
        xsteps = width / (gridstep);
        ysteps = height / (gridstep);
    }

    numk = (xsteps*ysteps);
    kx.resize(numk); ky.resize(numk);
    int n = 0;
    for (int y = halfstep, rowstep = 0; y < height && n < numk; y += gridstep, rowstep++)
    {
        for (int x = halfstep; x < width && n < numk; x += gridstep)
        {
            if (y <= h - halfstep && x <= w - halfstep)
            {
                kx[n] = x;
                ky[n] = y;
                n++;
            }
        }
    }
}

Void cSNICSuperpixel::compute(cYUV<ImagePixelType> &YUV, cConfig &cParams) {

    const int w = m_uiSourceWidth;
    const int h = m_uiSourceHeight;
    const int sz = w*h;
    const int dx8[8] = { -1,  0, 1, 0, -1,  1, 1, -1 };//for 4 or 8 connectivity
    const int dy8[8] = { 0, -1, 0, 1, -1, -1, 1,  1 };//for 4 or 8 connectivity
    const int dn8[8] = { -1, -w, 1, w, -1 - w,1 - w,1 + w,-1 + w };

    const bool isOmni = (m_sCamType == "Equirectangular");

    struct NODE
    {
        unsigned int i; // the x and y values packed into one
        unsigned int k; // the label
        double d;       // the distance
    };
    struct compare
    {
        bool operator()(const NODE& one, const NODE& two)
        {
            return one.d > two.d;//for increasing order of distances
        }
    };
    //-------------
    // Find seeds
    //-------------
    vector<int> cx(0), cy(0);
    int numk = m_uiNumberOfSuperpixels;
    if(isOmni)FindSeedsOmni(w, h, numk, cx, cy, cParams);//the function may modify numk from its initial value
    else FindSeeds(w, h, numk, cx, cy);
    //-------------
    // Initialize
    //-------------
    m_uiNumberOfSuperpixels = numk;
    for (UInt h = 0, pp = 0; h < m_uiSourceHeight; h++) {
        for (UInt w = 0; w < m_uiSourceWidth; w++, pp++) {
            m_cYUVlabel[pp] = m_uiNumberOfSuperpixels;
            m_cYUVdist[pp] = DBL_MAX;
        }
    }
    for (UInt i = 0; i < m_uiNumberOfSuperpixels; i++) {
        cArray<Double> Superpix;
        for (UInt s = 0; s < 6; s++) {
            Double temp = 0;
            Superpix.push_back(temp);
        }
        m_apdSuperpixels.push_back(Superpix);
    }
    NODE tempnode;
    priority_queue<NODE, vector<NODE>, compare> pq;
    for (int k = 0; k < numk; k++)
    {
        NODE tempnode;
        tempnode.i = cx[k] << 16 | cy[k];
        tempnode.k = k;
        tempnode.d = 0;
        pq.push(tempnode);
    }

    for (UInt k = 0; k < m_uiNumberOfSuperpixels; k++) {
        m_apdSuperpixels[k][0] = 0;
        m_apdSuperpixels[k][1] = 0;
        m_apdSuperpixels[k][2] = 0;
        m_apdSuperpixels[k][3] = 0;
        m_apdSuperpixels[k][4] = 0;
    }
    vector<double> ksize(m_uiNumberOfSuperpixels, 0);

    const int CONNECTIVITY = 8;//values can be 4 or 8
    const double M = m_dM * (pow(2, (cParams.m_auiBitDepthColor[0] - 8)));//10.0;
    const double invwt = (M*M*numk) / double(sz);

    int qlength = (int)(pq.size());
    int pixelcount = 0;
    int xx(0), yy(0), ii(0), iiUV(0);
    double Ydiff(0), Udiff(0), Vdiff(0), xdiff(0), ydiff(0), colordist(0), xydist(0), slicdist(0);

    //-------------
    // Run main loop
    //-------------
    while (qlength > 0) 
    {
        NODE node = pq.top(); pq.pop(); qlength--;
        const int k = node.k;
        const int x = node.i >> 16 & 0xffff;
        const int y = node.i & 0xffff;
        const int i = y*w + x;
        int iUV = (y >> 1)*(w >> 1) + (x >> 1);
        if (YUV.m_uiChromaSubsampling == 420)iUV = ((y) >> 1)*(w >> 1) + ((x) >> 1);
        else iUV = i;

        if (m_cYUVlabel[i] == m_uiNumberOfSuperpixels)
        {
            m_cYUVlabel[i] = k; pixelcount++;
            m_apdSuperpixels[k][0] += YUV.m_atY[i];
            m_apdSuperpixels[k][1] += YUV.m_atU[iUV];
            m_apdSuperpixels[k][2] += YUV.m_atV[iUV];
            m_apdSuperpixels[k][3] += x;
            m_apdSuperpixels[k][4] += y;
            ksize[k] += 1.0;

            for (int p = 0; p < CONNECTIVITY; p++)
            {
                xx = x + dx8[p];
                yy = y + dy8[p];
                if (!(xx < 0 || xx >= w || yy < 0 || yy >= h))
                {
                    ii = i + dn8[p];
                    if(YUV.m_uiChromaSubsampling==420)iiUV = ((yy) >> 1)*(w >> 1) + ((xx) >> 1);
                    else iiUV = ii;
                    if (m_cYUVlabel[ii] == m_uiNumberOfSuperpixels)//create new nodes
                    {
                        Ydiff = m_apdSuperpixels[k][0] - YUV.m_atY[ii] * ksize[k];
                        Udiff = m_apdSuperpixels[k][1] - YUV.m_atU[iiUV] * ksize[k];
                        Vdiff = m_apdSuperpixels[k][2] - YUV.m_atV[iiUV] * ksize[k];
                        xdiff = m_apdSuperpixels[k][3] - xx*ksize[k];
                        ydiff = m_apdSuperpixels[k][4] - yy*ksize[k];

                        colordist = (Ydiff*Ydiff+ Udiff*Udiff+ Vdiff*Vdiff);

                        
                        if (isOmni) {
                            xydist = xCalcSphericalDistance(m_apdSuperpixels[k][3] / ksize[k], m_apdSuperpixels[k][4] / ksize[k], xx, yy, m_uiSourceWidth, m_uiSourceHeight, cParams);
                            xydist = xydist * xydist;
                            slicdist = (colordist / (ksize[k] * ksize[k]) + xydist * invwt);
                        }
                        else {
                            xydist = xdiff * xdiff + ydiff * ydiff;
                            slicdist = (colordist + xydist * invwt) / (ksize[k] * ksize[k]);//late normalization by ksize[k], to have only one division operation
                        }
                                        
                        
                        tempnode.i = xx << 16 | yy;
                        tempnode.k = k;
                        tempnode.d = slicdist;
                        pq.push(tempnode); qlength++;

                    }
                }
            }
        }
    }
    m_uiNumberOfSuperpixels = numk;

    for (UInt k = 0; k < m_uiNumberOfSuperpixels; k++) {
        Double InvSizeK = 1.0 / ksize[k];
        m_apdSuperpixels[k][0] *= InvSizeK;
        m_apdSuperpixels[k][1] *= InvSizeK;
        m_apdSuperpixels[k][2] *= InvSizeK;
        m_apdSuperpixels[k][3] *= InvSizeK;
        m_apdSuperpixels[k][4] *= InvSizeK;

        if (m_apdSuperpixels[k][3] >= m_uiSourceWidth || m_apdSuperpixels[k][4] >= m_uiSourceHeight || m_apdSuperpixels[k][3] < 0 || m_apdSuperpixels[k][4] < 0) {
            m_apdSuperpixels[k][3] = 0;
            m_apdSuperpixels[k][4] = 0;
        }
    }

}

