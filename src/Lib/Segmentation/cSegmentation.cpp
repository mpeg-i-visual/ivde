//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */


#include "cSegmentation.h"
#include <cmath>
#include <math.h>

using namespace std;

Void cSegmentation::init(UInt SourceWidth, UInt SourceHeight, Double M, UInt K, String sCamType) {
    m_uiSourceWidth = SourceWidth;
    m_uiSourceHeight = SourceHeight;
    m_dM = M;
    m_dK = K;
    m_sCamType = sCamType;

    m_uiSuperpixelSize = UInt(sqrt(m_uiSourceHeight*m_uiSourceWidth / Double(m_dK)));

    m_cYUVlabel.reserve(m_uiSourceWidth* m_uiSourceHeight);
    m_cYUVdist.reserve(m_uiSourceWidth* m_uiSourceHeight);

    m_uiNumberOfSuperpixels = 0;
    for (UInt h = m_uiSuperpixelSize / 2; h < m_uiSourceHeight; h += m_uiSuperpixelSize) {
        for (UInt w = m_uiSuperpixelSize / 2; w < m_uiSourceWidth; w += m_uiSuperpixelSize) {
            m_uiNumberOfSuperpixels++;
        }
    }
    return;
}

Void cSegmentation::init_temp() {
    for (UInt i = 0; i < m_uiNumberOfSuperpixels; i++) {
        cArray<Double> Superpix;
        for (UInt s = 0; s < 5; s++) {
            Double temp = 0;
            Superpix.push_back(temp);
        }
        m_apdSuperpixels.push_back(Superpix);
    }
    return;
}

Void cSegmentation::compute_neigh() {
    for (UInt s = 0; s < m_uiNumberOfSuperpixels; s++) {
        cArray<UInt> nei;
        m_apiSuperpixelNeigh.push_back(nei);
    }
    for (Int h = 0; h < Int(m_uiSourceHeight); h++) {
        for (Int w = 0; w < Int(m_uiSourceWidth); w++) {
            UInt cur_label = m_cYUVlabel[h*m_uiSourceWidth + w];
            for (Int y = h - 1; y < h + 2; y++) {
                for (Int x = w - 1; x < w + 2; x++) {
                    if (h - 1 > 0 && w - 1 > 0 && h + 2 < Int(m_uiSourceHeight) && w + 2 < Int(m_uiSourceWidth)) {
                        if (cur_label != m_cYUVlabel[y*m_uiSourceWidth + x]) {
                            Bool present = false;

                            for (UInt s = 0; s < m_apiSuperpixelNeigh.at(cur_label).size(); s++) {
                                if (m_apiSuperpixelNeigh.at(cur_label).at(s) == m_cYUVlabel[y*m_uiSourceWidth + x]) {
                                    present = true;
                                    break;
                                }
                            }
                            if ((!present)) {
                                m_apiSuperpixelNeigh.at(cur_label).push_back(m_cYUVlabel[y*m_uiSourceWidth + x]);
                            }
                        }
                    }
                }
            }
        }
    }
}

Void cSegmentation::qSort(Int* tab, Int left, Int right) {
    Int i = left;
    Int j = right;
    Int x = tab[(left + right) / 2];
    do {
        while (tab[i] < x) i++;
        while (tab[j] > x) j--;
        if (i <= j)    std::swap(tab[i++], tab[j--]);
    } while (i <= j);

    if (left < j) qSort(tab, left, j);
    if (right > i) qSort(tab, i, right);
}

Void cSegmentation::write_labels(String filename, Bool append) {

    cYUV<UShort> cYUVwritelabel;
    cYUVwritelabel.init(m_uiSourceWidth, m_uiSourceHeight, 16, "YUV400");
    Double uiNorm = 65535.0 / m_uiNumberOfSuperpixels;
    for (UInt y = 0; y < m_uiSourceHeight; y++) {
        for (UInt x = 0; x < m_uiSourceWidth; x++) {
            cYUVwritelabel.m_atY[y*m_uiSourceWidth + x] = 1000 * (m_cYUVlabel[y*m_uiSourceWidth + x]) * uiNorm;
        }
    }
    cYUVwritelabel.frameWriter(filename, append);
    return;
}
