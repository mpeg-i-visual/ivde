//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2022 Poznań University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
	/*Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met
	*This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.
	Neither the name of PUT or ETRI nor the names of its contributors may
	be used to endorse or promote products derived from this software
	without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/
	 

#ifndef __CSNICSUPERPIXEL_H__
#define __CSNICSUPERPIXEL_H__

#include "../Common/TypeDef.h"
#include "../Common/cYUV.h"
#include "../Common/cArray.h"
#include "../DepthEstimation/paramsDE.h"
#include "cSegmentation.h"
#include <cmath>

class cSNICSuperpixel final : public cSegmentation {
public:
  Void compute(cYUV<ImagePixelType> &YUV, cConfig &cParams) override;

protected:
  Void FindSeedsOmni(const int width, const int height, int& numk, std::vector<int>& kx, std::vector<int>& ky, cConfig &cParams);
  Void FindSeeds    (const int width, const int height, int& numk, std::vector<int>& kx, std::vector<int>& ky);

protected:
  static inline Double xCalcUtoLon(Double u,   Double Width , cConfig &cParams) { return ((u / Width) - 0.5) * (Double)(cParams.m_dAoVW); }
  static inline Double xCalcVtoLat(Double v,   Double Height, cConfig &cParams) { return (0.5 - (v / Height)) * (Double)(cParams.m_dAoVH); }
  static inline Double xCalcLonToU(Double Lon, Double Width , cConfig &cParams) { return (Lon * 1.0/(Double)(cParams.m_dAoVW) + 0.5) * Width; }
  static inline Double xCalcLatToV(Double Lat, Double Height, cConfig &cParams) { return (0.5 - Lat * 1.0 / (Double)(cParams.m_dAoVH)) * Height; }

	static inline Double xCalcEquirectangularApproximation(Double Lon0, Double Lat0, Double Lon1, Double Lat1, Double Radius)
	{
		Double DeltaLon = Lon1 - Lon0;
		Double X        = DeltaLon * cos((Double)0.5 * (Lat0 + Lat1));
		Double Y        = Lat1 - Lat0;
		Double Distance = Radius * sqrt((X * X) + (Y * Y));
		return Distance;
	}
	static inline Double xCalcSphericalDistance(Double X0, Double Y0, Double X1, Double Y1, Double Width, Double Height, cConfig &cParams)
	{
		Double Lon0 = xCalcUtoLon(X0, Width , cParams);
		Double Lat0 = xCalcVtoLat(Y0, Height, cParams);
		Double Lon1 = xCalcUtoLon(X1, Width , cParams);
		Double Lat1 = xCalcVtoLat(Y1, Height, cParams);

		Double Distance = xCalcEquirectangularApproximation(Lon0, Lat0, Lon1, Lat1, (Double)Width * 1.0 / (Double)(cParams.m_dAoVW));
		return Distance;
	}
};


#endif