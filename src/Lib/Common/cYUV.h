//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2020 Pozna� University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */

#ifndef __CYUV_H__
#define __CYUV_H__

#include "TypeDef.h"
#include "CommonDef.h"
#include <iostream>
#include <fstream>


template <class PixelType>
class cYUV{
public:
    Char m_sYUVFilename[MAX_TEXT_LENGTH];

    UInt m_uiWidth;
    UInt m_uiHeight;
    UInt m_uiWidthUV;
    UInt m_uiHeightUV;

    UInt m_uiOriginalWidth;
    UInt m_uiOriginalHeight;

    UInt m_uiViewSize;
    UInt m_uiViewSizeUV;

    UInt m_uiBitsPerSample;
    UInt m_uiChromaSubsampling;

    Int m_iFilterType;
    Double m_uiHorizontalPrecision;
    Double m_uiVerticalPrecision;

    UShort *m_atY;
    UShort *m_atU;
    UShort *m_atV;

    UChar *m_acY;
    UChar *m_acU;
    UChar *m_acV;

    ~cYUV();
    cYUV();
    cYUV(Int w, Int h, Int bps = 8, String cs = "YUV420");
    Void setOriginalSize();
    Void init(Int w, Int h, Int bps = 8, String cs = "YUV420");
    Void frameReader(String sFileName, Int iFrame);
    Void frameWriter(String sFileName, Bool bAppend);

    UInt getChrominanceHorizontalShift();
    UInt getChrominanceVerticalShift();
};

#ifndef TLibCommon_cYUV_IMPLEMENTATION
extern template class cYUV<UChar >;
extern template class cYUV<UShort>;
extern template class cYUV<UInt  >;
extern template class cYUV<Double>;
#endif // !TLibCommon_cYUV_IMPLEMENTATION

#endif //__CYUV_H__