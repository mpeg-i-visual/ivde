//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2020 Pozna� University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */

#define _CRT_SECURE_NO_WARNINGS
#define TLibCommon_cCamParams_IMPLEMENTATION
#include "cCamParams.h"
#include "../Common/Json.h"
#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;
using namespace TMIV::Common;

template <class MatrixType>
Void cCamParams<MatrixType>::cvtExtMat() {

    cMatrix<MatrixType> R(3, 3);
    cMatrix<MatrixType> T(3, 1);

    m_mCvtdExtMat.init(4, 4);

    for (UInt h = 0; h < 3; h++) {
        for (UInt w = 0; w < 3; w++) {
            m_mCvtdExtMat.at(h, w) = m_mExtMat.at(h, w);
            R.at(h, w) = m_mExtMat.at(h, w);
        }
        T.at(h, 0) = m_mExtMat.at(h, 3);
    }

    cMatrix<MatrixType> mRT;
    mRT.init(3, 1);
    cMatrix<MatrixType>::MatrixMultiply(R, T, mRT);

    for (UInt w = 0; w < 3; w++) m_mCvtdExtMat.at(3, w) = 0;
    for (UInt h = 0; h < 3; h++) m_mCvtdExtMat.at(h, 3) = -mRT.at(h, 0);
    m_mCvtdExtMat.at(3, 3) = 1;

    return;
}

template <class MatrixType>
Void cCamParams<MatrixType>::calcProjMat() {

    m_mProjMat.init(4, 4);
    cMatrix<MatrixType>::MatrixMultiply(m_mIntMat, m_mCvtdExtMat, m_mProjMat);

    return;
}

template <class MatrixType>
Void cCamParams<MatrixType>::calcInvProjMat() {

    m_mInvProjMat.init(4, 4);
    cMatrix<MatrixType>::invert(m_mProjMat, m_mInvProjMat);

    return;
}


template <class MatrixType>
UInt cCamParams<MatrixType>::readParamsFromFile(String sFilename, cArray<cCamParams<MatrixType>*> &rapcCamParams, cArray<String> cfgCamnames) {

    cCamParams<MatrixType>* pcCamParams = NULL;

    for (UInt i = 0; i < cfgCamnames.size() + 1; i++) {
        pcCamParams = new cCamParams<MatrixType>(); rapcCamParams.push_back(pcCamParams);
    }

    pcCamParams = NULL;

    ifstream stream{ sFilename };
    Json params = Json{ stream };

    UInt uiCamTemp = 0;
    for (UInt uiCam = 0; uiCam < params.require("cameras").size(); uiCam++) {
        if ((params.require("cameras").at(uiCam).require("Name").asString() == "viewport") || (params.require("cameras").at(uiCam).require("Name").asString() == "center")) continue;
        pcCamParams = rapcCamParams[uiCamTemp];
        uiCamTemp++;
        pcCamParams->m_mExtMat.init(4, 4);
        pcCamParams->m_mIntMat.init(4, 4);

        for (Int h = 0; h < 3; h++) pcCamParams->m_mIntMat.at(h, 3) = 0;
        for (Int w = 0; w < 3; w++) pcCamParams->m_mIntMat.at(3, w) = 0;
        pcCamParams->m_mIntMat.at(3, 3) = 1;

        if (params.require("cameras").at(uiCam).require("Projection").asString() == "Perspective") {
            pcCamParams->m_mIntMat.at(0, 0) = params.require("cameras").at(uiCam).require("Focal").asDoubleVector<2>()[0];
            pcCamParams->m_mIntMat.at(1, 1) = params.require("cameras").at(uiCam).require("Focal").asDoubleVector<2>()[1];

            pcCamParams->m_mIntMat.at(0, 2) = params.require("cameras").at(uiCam).require("Principle_point").asDoubleVector<2>()[0];
            pcCamParams->m_mIntMat.at(1, 2) = params.require("cameras").at(uiCam).require("Principle_point").asDoubleVector<2>()[1];
        }
        else {
            pcCamParams->m_mIntMat.at(0, 0) = 1;
            pcCamParams->m_mIntMat.at(1, 1) = 1;

            pcCamParams->m_mIntMat.at(0, 2) = 1;
            pcCamParams->m_mIntMat.at(1, 2) = 1;
        }
        for (Int w = 0; w < 3; w++) pcCamParams->m_mExtMat.at(3, w) = 0;
        pcCamParams->m_mExtMat.at(3, 3) = 1;


        constexpr auto radperdeg{ 0.01745329251994329576923690768489F };
        stack::Array<Double, 3> Rot = radperdeg * params.require("cameras").at(uiCam).require("Rotation").asDoubleVector<3>();

        if (params.require("cameras").at(uiCam).require("Projection").asString() == "Perspective") {
            pcCamParams->m_mExtMat.at(0, 3) = -params.require("cameras").at(uiCam).require("Position").asDoubleVector<3>()[1];
            pcCamParams->m_mExtMat.at(1, 3) = -params.require("cameras").at(uiCam).require("Position").asDoubleVector<3>()[2];
            pcCamParams->m_mExtMat.at(2, 3) = params.require("cameras").at(uiCam).require("Position").asDoubleVector<3>()[0];
            pcCamParams->m_mExtMat.at(0, 0) = sin(Rot[0])*sin(Rot[1]) * sin(Rot[2]) + cos(Rot[0])*cos(Rot[2]);
            pcCamParams->m_mExtMat.at(1, 0) = (sin(Rot[0])*sin(Rot[1]) * cos(Rot[2]) - cos(Rot[0])*sin(Rot[2]));
            pcCamParams->m_mExtMat.at(2, 0) = -sin(Rot[0])*cos(Rot[1]);
            pcCamParams->m_mExtMat.at(0, 1) = cos(Rot[1])*sin(Rot[2]);
            pcCamParams->m_mExtMat.at(1, 1) = cos(Rot[1])*cos(Rot[2]);
            pcCamParams->m_mExtMat.at(2, 1) = sin(Rot[1]);
            pcCamParams->m_mExtMat.at(0, 2) = -(cos(Rot[0])*sin(Rot[1]) * sin(Rot[2]) - sin(Rot[0])*cos(Rot[2]));
            pcCamParams->m_mExtMat.at(1, 2) = -(cos(Rot[0])*sin(Rot[1]) * cos(Rot[2]) + sin(Rot[0])*sin(Rot[2]));
            pcCamParams->m_mExtMat.at(2, 2) = cos(Rot[0])*cos(Rot[1]);
        }
        else {
            pcCamParams->m_mExtMat.at(0, 3) = params.require("cameras").at(uiCam).require("Position").asDoubleVector<3>()[0];
            pcCamParams->m_mExtMat.at(1, 3) = -params.require("cameras").at(uiCam).require("Position").asDoubleVector<3>()[2];
            pcCamParams->m_mExtMat.at(2, 3) = -params.require("cameras").at(uiCam).require("Position").asDoubleVector<3>()[1];
            pcCamParams->m_mExtMat.at(0, 0) = cos(Rot[0])*cos(Rot[1]);
            pcCamParams->m_mExtMat.at(1, 0) = -(cos(Rot[0])*sin(Rot[1]) * cos(Rot[2]) + sin(Rot[0])*sin(Rot[2]));
            pcCamParams->m_mExtMat.at(2, 0) = -(cos(Rot[0])*sin(Rot[1]) * sin(Rot[2]) - sin(Rot[0])*cos(Rot[2]));
            pcCamParams->m_mExtMat.at(0, 1) = sin(Rot[1]);
            pcCamParams->m_mExtMat.at(1, 1) = cos(Rot[1])*cos(Rot[2]);
            pcCamParams->m_mExtMat.at(2, 1) = cos(Rot[1])*sin(Rot[2]);
            pcCamParams->m_mExtMat.at(0, 2) = -sin(Rot[0])*cos(Rot[1]);
            pcCamParams->m_mExtMat.at(1, 2) = (sin(Rot[0])*sin(Rot[1]) * cos(Rot[2]) - cos(Rot[0])*sin(Rot[2]));
            pcCamParams->m_mExtMat.at(2, 2) = sin(Rot[0])*sin(Rot[1]) * sin(Rot[2]) + cos(Rot[0])*cos(Rot[2]);
        }

        pcCamParams->cvtExtMat();
        pcCamParams->calcProjMat();
        pcCamParams->calcInvProjMat();
    }

    cArray<pair<Double, UInt>> dist;

    for (UInt uiCam = 0; uiCam < cfgCamnames.size(); uiCam++) {

        Double camdist = 0.0;

        for (UInt uiNeighCam = 0; uiNeighCam < cfgCamnames.size(); uiNeighCam++) {
            camdist += sqrt(
                pow((rapcCamParams[uiCam]->m_mExtMat.at(0, 3) - rapcCamParams[uiNeighCam]->m_mExtMat.at(0, 3)), 2) +
                pow((rapcCamParams[uiCam]->m_mExtMat.at(1, 3) - rapcCamParams[uiNeighCam]->m_mExtMat.at(1, 3)), 2) +
                pow((rapcCamParams[uiCam]->m_mExtMat.at(2, 3) - rapcCamParams[uiNeighCam]->m_mExtMat.at(2, 3)), 2)
            );


        }
        dist.push_back(make_pair(camdist, uiCam));
    }
    std::sort(dist.begin(), dist.end());
    UInt uiCentralCam = dist[0].second;

    pcCamParams = rapcCamParams[cfgCamnames.size()];

    pcCamParams->m_mExtMat.init(4, 4);
    pcCamParams->m_mIntMat.init(4, 4);

    for (Int h = 0; h < 3; h++) pcCamParams->m_mIntMat.at(h, 3) = 0;
    for (Int w = 0; w < 3; w++) pcCamParams->m_mIntMat.at(3, w) = 0;
    pcCamParams->m_mIntMat.at(3, 3) = 1;


    for (Int h = 0; h < 3; h++) {
        for (Int w = 0; w < 3; w++) {
            pcCamParams->m_mIntMat.at(h, w) = rapcCamParams[uiCentralCam]->m_mIntMat.at(h, w);
        }
    }

    for (Int h = 0; h < 4; h++) {
        for (Int w = 0; w < 4; w++) {
            pcCamParams->m_mExtMat.at(h, w) = rapcCamParams[uiCentralCam]->m_mExtMat.at(h, w);
        }
    }


    pcCamParams->cvtExtMat();
    pcCamParams->calcProjMat();
    pcCamParams->calcInvProjMat();

    pcCamParams = NULL;
    return uiCentralCam;
}

template class cCamParams<Float >;
template class cCamParams<Double>;
