//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2020 Pozna� University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */

#define TLibCommon_cMatrix_IMPLEMENTATION
#include "cMatrix.h"

template <class MatrixType>
cMatrix<MatrixType>::~cMatrix() {
    if (m_aatElements) {
        for (UInt h = 0; h < m_uiRows; h++) {
            delete m_aatElements[h];
        }
        delete m_aatElements;
    }
    m_aatElements = NULL;
    return;
}

template <class MatrixType>
cMatrix<MatrixType>::cMatrix() {
    m_aatElements = NULL;
    return;
}

template <class MatrixType>
cMatrix<MatrixType>::cMatrix(UInt h, UInt w) {
    init(h, w);
    return;
}

template <class MatrixType>
Void cMatrix<MatrixType>::init(UInt h, UInt w) {
    m_uiRows = h;
    m_uiCols = w;
    m_aatElements = new MatrixType*[m_uiRows];
    for (UInt h = 0; h < m_uiRows; h++) {
        m_aatElements[h] = new MatrixType[m_uiCols];
        for (UInt w = 0; w < m_uiCols; w++) {
            if (w == h) m_aatElements[h][w] = 1;
            else m_aatElements[h][w] = 0;
        }
    }
    return;
}

template <class MatrixType>
Void cMatrix<MatrixType>::display() {
    for (UInt h = 0; h < m_uiRows; h++) {
        for (UInt w = 0; w < m_uiCols; w++) {
            fprintf(stdout, "%f\t", m_aatElements[h][w]);
        }
        fprintf(stdout, "\n");
    }
    return;
}

template <class MatrixType>
Void cMatrix<MatrixType>::MatrixMultiply(cMatrix<MatrixType> &A, cMatrix<MatrixType> &B, cMatrix<MatrixType> &C) {

    if (A.m_uiCols == B.m_uiCols) {
        for (UInt i = 0; i < A.m_uiRows; i++) {
            for (UInt j = 0; j < B.m_uiCols; j++) {
                C.at(i, j) = 0;
                for (UInt k = 0; k < A.m_uiCols; k++) {
                    C.at(i, j) += (A.at(i, k) * B.at(k, j));
                }
            }
        }
        return;
    }
    else {
        for (UInt h = 0; h < 3; h++) {
            C.at(h, 0) = A.at(h, 0)*B.at(0, 0) + A.at(h, 1)*B.at(1, 0) + A.at(h, 2)*B.at(2, 0) +A.at(h, 3);
        }
        return;
    }

    return;
}

template <class MatrixType>
Void cMatrix<MatrixType>::invert(cMatrix<MatrixType> &A, cMatrix<MatrixType> &invA) {

    Double m[16], inv[16], det;

    for (int h = 0, pp = 0; h < 4; h++) {
        for (int w = 0; w < 4; w++, pp++) {
            m[pp] = A.at(h, w);
        }
    }

    inv[0] = m[5] * m[10] * m[15] - m[5] * m[11] * m[14] - m[9] * m[6] * m[15] + m[9] * m[7] * m[14] + m[13] * m[6] * m[11] - m[13] * m[7] * m[10];
    inv[1] = -m[1] * m[10] * m[15] + m[1] * m[11] * m[14] + m[9] * m[2] * m[15] - m[9] * m[3] * m[14] - m[13] * m[2] * m[11] + m[13] * m[3] * m[10];
    inv[2] = m[1] * m[6] * m[15] - m[1] * m[7] * m[14] - m[5] * m[2] * m[15] + m[5] * m[3] * m[14] + m[13] * m[2] * m[7] - m[13] * m[3] * m[6];
    inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2] * m[11] - m[5] * m[3] * m[10] - m[9] * m[2] * m[7] + m[9] * m[3] * m[6];
    inv[4] = -m[4] * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6] * m[15] - m[8] * m[7] * m[14] - m[12] * m[6] * m[11] + m[12] * m[7] * m[10];
    inv[5] = m[0] * m[10] * m[15] - m[0] * m[11] * m[14] - m[8] * m[2] * m[15] + m[8] * m[3] * m[14] + m[12] * m[2] * m[11] - m[12] * m[3] * m[10];
    inv[6] = -m[0] * m[6] * m[15] + m[0] * m[7] * m[14] + m[4] * m[2] * m[15] - m[4] * m[3] * m[14] - m[12] * m[2] * m[7] + m[12] * m[3] * m[6];
    inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2] * m[11] + m[4] * m[3] * m[10] + m[8] * m[2] * m[7] - m[8] * m[3] * m[6];
    inv[8] = m[4] * m[9] * m[15] - m[4] * m[11] * m[13] - m[8] * m[5] * m[15] + m[8] * m[7] * m[13] + m[12] * m[5] * m[11] - m[12] * m[7] * m[9];
    inv[9] = -m[0] * m[9] * m[15] + m[0] * m[11] * m[13] + m[8] * m[1] * m[15] - m[8] * m[3] * m[13] - m[12] * m[1] * m[11] + m[12] * m[3] * m[9];
    inv[10] = m[0] * m[5] * m[15] - m[0] * m[7] * m[13] - m[4] * m[1] * m[15] + m[4] * m[3] * m[13] + m[12] * m[1] * m[7] - m[12] * m[3] * m[5];
    inv[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1] * m[11] - m[4] * m[3] * m[9] - m[8] * m[1] * m[7] + m[8] * m[3] * m[5];
    inv[12] = -m[4] * m[9] * m[14] + m[4] * m[10] * m[13] + m[8] * m[5] * m[14] - m[8] * m[6] * m[13] - m[12] * m[5] * m[10] + m[12] * m[6] * m[9];
    inv[13] = m[0] * m[9] * m[14] - m[0] * m[10] * m[13] - m[8] * m[1] * m[14] + m[8] * m[2] * m[13] + m[12] * m[1] * m[10] - m[12] * m[2] * m[9];
    inv[14] = -m[0] * m[5] * m[14] + m[0] * m[6] * m[13] + m[4] * m[1] * m[14] - m[4] * m[2] * m[13] - m[12] * m[1] * m[6] + m[12] * m[2] * m[5];
    inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1] * m[10] + m[4] * m[2] * m[9] + m[8] * m[1] * m[6] - m[8] * m[2] * m[5];

    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

    det = 1.0 / det;

    for (int h = 0, pp = 0; h < 4; h++) {
        for (int w = 0; w < 4; w++, pp++) {
            invA.at(h, w) = inv[pp] * det;
        }
    }

    return;
}

template <class MatrixType>
Void cMatrix<MatrixType>::matrix3to4(cMatrix<MatrixType> M3, cMatrix<MatrixType> &M4) {

    if ((M3.m_uiCols != M3.m_uiRows) && (M3.m_uiCols != 3)) return;

    M4.m_uiRows = 4;
    M4.m_uiCols = 4;

    M4.m_aatElements = new MatrixType*[M4.m_uiRows];
    for (UInt h = 0; h < M4.m_uiRows; h++) {
        M4.m_aatElements[h] = new MatrixType[M4.m_uiCols];
    }

    for (UInt h = 0; h < M3.m_uiRows; h++) {
        for (UInt w = 0; w < M3.m_uiCols; w++) {
            M4.at(h, w) = M3.at(h, w);
        }
    }
    for (UInt h = 0; h < M3.m_uiRows; h++) M4.at(h, 3) = 0;
    for (UInt w = 0; w < M3.m_uiCols; w++) M4.at(3, w) = 0;
    M4.at(3, 3) = 1;

    return;
}

template <class MatrixType>
Void cMatrix<MatrixType>::matrix4to3(cMatrix<MatrixType> M4, cMatrix<MatrixType> &M3) {

    if ((M4.m_uiCols != M4.m_uiRows) && (M4.m_uiCols != 4)) return;

    M3.m_uiRows = 3;
    M3.m_uiCols = 3;

    M3.m_aatElements = new MatrixType*[M3.m_uiRows];
    for (UInt h = 0; h < M3.m_uiRows; h++) {
        M3.m_aatElements[h] = new MatrixType[M3.m_uiCols];
    }

    for (UInt h = 0; h < M3.m_uiRows; h++) {
        for (UInt w = 0; w < M3.m_uiCols; w++) {
            M3.at(h, w) = M4.at(h, w);
        }
    }

    return;
}

template class cMatrix<Float >;
template class cMatrix<Double>;
