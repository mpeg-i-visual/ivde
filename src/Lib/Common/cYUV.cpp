//=================================================================================
//
//  AUTORIGHTS
//  Immersive video depth estimation
//  Copyright (C) 2020 Pozna� University of Technology (PUT), Poland
//  and Electronics and Telecommunications Research Institute (ETRI), Korea
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes only.
//	If you use this software, please cite the corresponding document:
//	"Overview and Efficiency of Decoder-Side Depth Estimation in MPEG Immersive Video"
//	IEEE Transactions on Circuits and Systems for Video Technology, doi: 10.1109/TCSVT.2022.3162916
//
//=================================================================================
    /*Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met
    *This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    Neither the name of PUT or ETRI nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    */

#define TLibCommon_cYUV_IMPLEMENTATION
#include "cYUV.h"
#include <assert.h>

#if defined(_MSC_VER)
#define ftell64(FileStream)               _ftelli64(FileStream)
#define fseek64(FileStream,Offset,Origin) _fseeki64(FileStream,Offset,Origin)
#else
#define ftell64(FileStream)               ftello(FileStream)
#define fseek64(FileStream,Offset,Origin) fseeko(FileStream,Offset,Origin)
#endif


template <class PixelType>
cYUV<PixelType>::~cYUV() {
    if (m_acY) { delete[] m_acY; m_acY = NULL; }
    if (m_acU) { delete[] m_acU; m_acU = NULL; }
    if (m_acV) { delete[] m_acV; m_acV = NULL; }
}

template <class PixelType>
cYUV<PixelType>::cYUV() {
    m_acY = NULL;
    m_acU = NULL;
    m_acV = NULL;
    return;
}

template <class PixelType>
cYUV<PixelType>::cYUV(Int w, Int h, Int bps, String cs) {
    m_acY = NULL;
    m_acU = NULL;
    m_acV = NULL;
    init(w, h, bps, cs);
    return;
}

template <class PixelType>
Void cYUV<PixelType>::setOriginalSize() {

    if (m_acY) { delete m_acY; m_acY = NULL; }
    if (m_acU) { delete m_acU; m_acU = NULL; }
    if (m_acV) { delete m_acV; m_acV = NULL; }

    m_uiWidth = m_uiOriginalWidth;
    m_uiHeight = m_uiOriginalHeight;
    m_uiViewSize = m_uiWidth * m_uiHeight;

    m_acY = new UChar[m_uiViewSize * 2];
    if (m_uiBitsPerSample > 8) {
        m_atY = (UShort*)m_acY;
    }
    else {
        m_atY = new UShort[m_uiViewSize];
    }
    if (m_uiChromaSubsampling == 400) {
        m_uiWidthUV = 0;
        m_uiHeightUV = 0;
        m_uiViewSizeUV = 0;
        m_acU = NULL;
        m_acV = NULL;
    }
    if (m_uiChromaSubsampling == 420) {
        m_uiWidthUV = m_uiWidth >> 1;
        m_uiHeightUV = m_uiHeight >> 1;
        m_uiViewSizeUV = m_uiWidthUV * m_uiHeightUV;
        m_acU = new UChar[m_uiViewSizeUV * 2];
        m_acV = new UChar[m_uiViewSizeUV * 2];
        if (m_uiBitsPerSample > 8) {
            m_atU = (UShort*)m_acU;
            m_atV = (UShort*)m_acV;
        }
        else {
            m_atU = new UShort[m_uiViewSize];
            m_atV = new UShort[m_uiViewSize];
        }
    }
    if (m_uiChromaSubsampling == 444) {
        m_uiWidthUV = m_uiWidth;
        m_uiHeightUV = m_uiHeight;
        m_uiViewSizeUV = m_uiWidthUV * m_uiHeightUV;
        m_acU = new UChar[m_uiViewSizeUV * 2];
        m_acV = new UChar[m_uiViewSizeUV * 2];
        if (m_uiBitsPerSample > 8) {
            m_atU = (UShort*)m_acU;
            m_atV = (UShort*)m_acV;
        }
        else {
            m_atU = new UShort[m_uiViewSize];
            m_atV = new UShort[m_uiViewSize];
        }
    }
}

template <class PixelType>
Void cYUV<PixelType>::init(Int w, Int h, Int bps, String cs) {

    m_uiOriginalHeight = h;
    m_uiOriginalWidth = w;

    m_uiBitsPerSample = bps;
    if (cs == "YUV400")m_uiChromaSubsampling = 400;
    if (cs == "YUV420")m_uiChromaSubsampling = 420;
    if (cs == "YUV444")m_uiChromaSubsampling = 444;

    m_uiWidth = m_uiOriginalWidth;
    m_uiHeight = m_uiOriginalHeight;

    setOriginalSize();
    return;
}


template <class PixelType>
Void cYUV<PixelType>::frameReader(String sFileName, Int frame) {

    setOriginalSize();
    size_t size;

    FILE* fileYUV = fopen(sFileName.c_str(), "rb");
    if (fileYUV == NULL)perror(sFileName.c_str());

    Int64 offset = 0;
    if (m_uiChromaSubsampling == 444) offset = 3 * m_uiViewSize * ((m_uiBitsPerSample <= 8) ? 1 : 2);
    if (m_uiChromaSubsampling == 420) offset = Int(m_uiViewSize + (2 * m_uiViewSizeUV)) * ((m_uiBitsPerSample <= 8) ? 1 : 2);
    if (m_uiChromaSubsampling == 400) offset = m_uiViewSize * ((m_uiBitsPerSample <= 8) ? 1 : 2);

    fseek64(fileYUV, Int64(offset*frame), 0);
    size = fread(m_acY, 1, m_uiViewSize*((m_uiBitsPerSample <= 8) ? 1 : 2), fileYUV);

    if (m_uiChromaSubsampling == 420 || m_uiChromaSubsampling == 444) {
        size += fread(m_acU, 1, m_uiViewSizeUV*((m_uiBitsPerSample <= 8) ? 1 : 2), fileYUV);
        size += fread(m_acV, 1, m_uiViewSizeUV*((m_uiBitsPerSample <= 8) ? 1 : 2), fileYUV);

    }
    if (size) {
        if (m_uiBitsPerSample <= 8) {
            UInt pp = 0;
            for (UInt w = 0; w < m_uiWidth; w++) {
                for (UInt h = 0; h < m_uiHeight; h++) {
                    pp++;
                    m_atY[pp] = m_acY[pp];
                }
            }
            pp = 0;
            for (UInt w = 0; w < m_uiWidthUV; w++) {
                for (UInt h = 0; h < m_uiHeightUV; h++) {
                    pp++;
                    m_atU[pp] = m_acU[pp];
                    m_atV[pp] = m_acV[pp];
                }
            }
        }
    }
    else {
        std::cout << "No data read from a view";
    }

    fclose(fileYUV);

    return;
}

template <class PixelType>
Void cYUV<PixelType>::frameWriter(String sFileName, Bool bAppend) {

    FILE* fileYUV = fopen(sFileName.c_str(), (bAppend) ? "ab" : "wb");

    fwrite((void*)m_acY, (m_uiBitsPerSample <= 8) ? 1 : 2, m_uiViewSize, fileYUV);

    if (m_acU && m_acV) {
        fwrite(m_acU, (m_uiBitsPerSample <= 8) ? 1 : 2, m_uiViewSizeUV, fileYUV);
        fwrite(m_acV, (m_uiBitsPerSample <= 8) ? 1 : 2, m_uiViewSizeUV, fileYUV);
    }

    fclose(fileYUV);

    return;
}

template <class PixelType>
UInt cYUV<PixelType>::getChrominanceHorizontalShift() {
    if (m_uiChromaSubsampling == 420) {
        return 1;
    }
    if (m_uiChromaSubsampling == 444) {
        return 0;
    }
    return UINT32_MAX;
}

template <class PixelType>
UInt cYUV<PixelType>::getChrominanceVerticalShift() {
    if (m_uiChromaSubsampling == 420) {
        return 1;
    }
    if (m_uiChromaSubsampling == 444) {
        return 0;
    }
    return UINT32_MAX;
}

template class cYUV<UChar >;
template class cYUV<UShort>;
template class cYUV<UInt  >;
template class cYUV<Double>;
