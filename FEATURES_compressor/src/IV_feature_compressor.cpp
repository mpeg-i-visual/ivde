﻿#include "IV_feature_compressor.h"
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char* argv[])
{

	if (argc < 3) {
		std::cout << "ERROR! Type <filename> <mode>\n";
		std::cout << " \t mode: enc - encode txt file \n";
		std::cout << " \t mode: dec - decode enc file \n";
		std::cout << " \t mode: noflags - remove flags from txt file \n";

		return EXIT_FAILURE;
	}

	string mode = string(argv[2]);
	string fnInput = string(argv[1]);
	ifstream inputFile;


	if (mode == "enc") {

		inputFile.open(fnInput, ios::in);
		if (!inputFile.good()) {
			std::cout << "File error!\n";
			return EXIT_FAILURE;
		}

		string fnFeaturesEnc = fnInput;
		fnFeaturesEnc += (".enc");

		fstream featuresEncoded;

		//uses file with flags
		featuresEncoded.open(fnFeaturesEnc, ios::out | ios::binary);
		encode(inputFile, featuresEncoded);
		inputFile.close();
		featuresEncoded.close();

		return EXIT_SUCCESS;
	}
	else if (mode == "dec") {

		inputFile.open(fnInput, ios::in | ios::binary);
		if (!inputFile.good()) {
			std::cout << "File error!\n";
			return EXIT_FAILURE;
		}

		string fnFeaturesDec = fnInput;
		int n = fnFeaturesDec.rfind(".enc");
		if (fnFeaturesDec.substr(n, fnFeaturesDec.length() - 1) == ".enc")
		{
			fnFeaturesDec.erase(n, fnFeaturesDec.length() - 1);
			fnFeaturesDec += (".dec");
		}
		else {
			std::cout << "File extension should be .enc\n";
			return EXIT_FAILURE;
		}

		ofstream featuresDecoded;

		featuresDecoded.open(fnFeaturesDec, ios::out);
		decode(inputFile, featuresDecoded);
		inputFile.close();
		featuresDecoded.close();

		return EXIT_SUCCESS;
	}
	else if (mode == "noflags") {

		inputFile.open(fnInput, ios::in);
		if (!inputFile.good()) {
			std::cout << "File error!\n";
			return EXIT_FAILURE;
		}

		string fnFeaturesCompressed = fnInput;
		fnFeaturesCompressed += (".noflags");

		ofstream featuresCompressed;

		featuresCompressed.open(fnFeaturesCompressed);
		compress(inputFile, featuresCompressed);
		inputFile.close();
		featuresCompressed.close();

		return EXIT_SUCCESS;
	}
	else {

		std::cout << "Wrong mode!\n";
		return EXIT_FAILURE;
	}


}