﻿#pragma once
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;

int quant(int val, int q) {
	return val / q;
}

void compress(ifstream& fileInput, ofstream& fileOutput)
{
	string read_line;
	string read_word;
	string read_value;
	stringstream ss;

	int flagValue = 0;
	vector<int> flagBuffer;

	int ZminCurr = 0, ZmaxCurr = 0, ZminPrev = 0, ZmaxPrev = 0;
	int q = 1;

	getline(fileInput, read_line);
	fileOutput << read_line << endl;

	ss.str(read_line);
	while (getline(ss, read_word, '=')) {
		if (read_word == "q") {
			getline(ss, read_word, ' ');
			q = stoi(read_word);
			break;
		}
		getline(ss, read_word, ' ');
	}
	int flagcnt = 0;
	//loop through lines
	while (getline(fileInput, read_line)) {

		if (read_line[0] != 'f')
			continue;
		//remove parentheses
		read_line.erase(remove(read_line.begin(), read_line.end(), '['), read_line.end());
		read_line.erase(remove(read_line.begin(), read_line.end(), ']'), read_line.end());

		ss.clear();
		ss.str(read_line);

		getline(ss, read_word, ' '); //f
		getline(ss, read_word, ' '); //y
		getline(ss, read_word, ' '); //x

		getline(ss, read_word, '=');
		getline(ss, read_value, ' ');

		while (ss) {
			flagValue = stoi(read_value);

			if (read_word == "zmin") {
				ZminCurr = quant(flagValue, q);
				flagBuffer.push_back(ZminCurr - ZminPrev);
				swap(ZminCurr, ZminPrev);
			}
			else if (read_word == "zmax") {
				ZmaxCurr = quant(flagValue, q);
				flagBuffer.push_back(ZmaxCurr - ZmaxPrev);
				swap(ZmaxCurr, ZmaxPrev);
			}
			else {
				flagBuffer.push_back(flagValue);
				flagcnt++;
			}
			getline(ss, read_word, '=');
			getline(ss, read_value, ' ');
		}
		for (auto flag : flagBuffer) {
			fileOutput << flag << " ";
		}
		fileOutput << endl;
		flagBuffer.clear();
	}
}

void encode(ifstream& fileInput, fstream& fileOutput)
{
	string read_line;
	string read_word;
	string read_value;
	stringstream ss;

	int value = 0;
	vector<bool> flagBuffer;
	vector<int16_t> ZdiffBuffer;

	int ZminCurr = 0, ZmaxCurr = 0, ZminPrev = 0, ZmaxPrev = 0;
	int q = 1;

	getline(fileInput, read_line);
	fileOutput << read_line << endl;

	ss.str(read_line);
	while (getline(ss, read_word, '=')) {
		if (read_word == "q") {
			getline(ss, read_word, ' ');
			q = stoi(read_word);
			break;
		}
		getline(ss, read_word, ' ');
	}

	//loop through lines
	while (getline(fileInput, read_line)) {

		if (read_line[0] != 'f')
			continue;
		//remove parentheses
		read_line.erase(remove(read_line.begin(), read_line.end(), '['), read_line.end());
		read_line.erase(remove(read_line.begin(), read_line.end(), ']'), read_line.end());

		ss.clear();
		ss.str(read_line);

		getline(ss, read_word, ' '); //f
		getline(ss, read_word, ' '); //y
		getline(ss, read_word, ' '); //x

		getline(ss, read_word, '=');
		getline(ss, read_value, ' ');

		while (ss) {
			value = stoi(read_value);

			if (read_word == "zmin") {
				ZminCurr = quant(value, q);
				ZdiffBuffer.push_back(ZminCurr - ZminPrev);
				swap(ZminCurr, ZminPrev);
			}
			else if (read_word == "zmax") {
				ZmaxCurr = quant(value, q);
				ZdiffBuffer.push_back(ZmaxCurr - ZmaxPrev);
				swap(ZmaxCurr, ZmaxPrev);
			}
			else {
				flagBuffer.push_back(value);
			}
			getline(ss, read_word, '=');
			getline(ss, read_value, ' ');
		}
	}

	int numOfFlags = flagBuffer.size();
	fileOutput.write((char*)&numOfFlags, sizeof(numOfFlags));

	uint8_t oneByte = 0;
	int bitCnt = 0;

	for (int flagCnt = 0; flagCnt < flagBuffer.size(); flagCnt++) {

		if (flagBuffer[flagCnt]) oneByte |= (1 << bitCnt);
		bitCnt++;

		if (bitCnt == 8 || flagCnt == flagBuffer.size() - 1) {
			bitCnt = 0;
			fileOutput.write((char*)&oneByte, 1);
			oneByte = 0;
		}
	}

	for (int16_t Zdiff : ZdiffBuffer) {
		fileOutput.write((char*)&Zdiff, 2);
	}
}


string decodeFlags(vector<bool>::iterator& flagBufferIt, vector<int16_t>::iterator& ZdiffBufferIt, uint32_t q, int& prevZmin, int& prevZmax)
{
	string result = "";
	int ZminDiff = 0, ZmaxDiff = 0;
	int splitFlag, skipFlag, quadSplitFlag, splitOrientationFlag, splitSymmetryFlag, splitFirstBlockBigger;

	splitFlag = *flagBufferIt++ == true ? 1 : 0;
	result += " split_flag=" + to_string(splitFlag);

	skipFlag = *flagBufferIt++ == true ? 1 : 0;
	result += " skip_flag=" + to_string(skipFlag);

	if (skipFlag == 1)
		return result;

	if (splitFlag == 1) {
		quadSplitFlag = *flagBufferIt++ == true ? 1 : 0;
		result += " quad_split_flag=" + to_string(quadSplitFlag);

		if (quadSplitFlag == 1) {
			for (int i = 0; i < 4; i++)
				result += " [" + decodeFlags(flagBufferIt, ZdiffBufferIt, q, prevZmin, prevZmax) + " ]";
		}
		else {
			splitOrientationFlag = *flagBufferIt++ == true ? 1 : 0;
			result += " split_orientation_flag=" + to_string(splitOrientationFlag);

			splitSymmetryFlag = *flagBufferIt++ == true ? 1 : 0;
			result += " split_symmetry_flag=" + to_string(splitSymmetryFlag);

			if (splitSymmetryFlag == 0) {
				splitFirstBlockBigger = *flagBufferIt++ == true ? 1 : 0;
				result += " split_first_block_bigger=" + to_string(splitFirstBlockBigger);
			}

			for (int i = 0; i < 2; i++)
				result += " [" + decodeFlags(flagBufferIt, ZdiffBufferIt, q, prevZmin, prevZmax) + " ]";
		}
	}
	else {
		ZminDiff = *ZdiffBufferIt++;
		uint16_t Zmin = (prevZmin + ZminDiff) * q;
		result += " zmin=" + to_string(Zmin);
		prevZmin = quant(Zmin, q);

		ZmaxDiff = *ZdiffBufferIt++;
		uint16_t Zmax = (prevZmax + ZmaxDiff) * q;
#if 0
		result += " zmax=" + to_string(Zmax);
#else
		result += " zmax=" + to_string(int(Zmax) + q > 65535 ? 65535 : Zmax);
#endif
		prevZmax = quant(Zmax, q);
	}
	return result;
}

void decode(ifstream& fileInput, ofstream& fileOutput)
{
	char read_byte = 0;
	string read_line;
	string read_word;
	string read_value;
	stringstream ss;
	uint32_t blockSize = 0;
	uint32_t q = 1;
	uint32_t sf = 0;
	uint32_t nf = 0;
	uint32_t width = 0;
	uint32_t height = 0;

	while (read_byte != '\n') {
		fileInput.read((char*)&read_byte, 1);
		read_line += read_byte;
	}

	fileOutput << read_line;
	ss.str(read_line);

	getline(ss, read_word, '=');
	getline(ss, read_value, ' ');

	while (ss) {
		if (read_word == "blksz")
			blockSize = stoi(read_value);
		if (read_word == "q")
			q = stoi(read_value);
		if (read_word == "sf")
			sf = stoi(read_value);
		if (read_word == "nf")
			nf = stoi(read_value);
		if (read_word == "width")
			width = stoi(read_value);
		if (read_word == "height")
			height = stoi(read_value);

		getline(ss, read_word, '=');
		getline(ss, read_value, ' ');
	}

	vector<bool> flagBuffer;
	vector<int16_t> ZdiffBuffer;
	int numOfFlags = 0;
	uint8_t oneByte = 0;
	int16_t Zdiff;

	fileInput.read((char*)&numOfFlags, sizeof(numOfFlags));
	int i = 0;
	for (; i < numOfFlags; i += 8) {
		fileInput.read((char*)&oneByte, 1);

		for (int b = 0; b < 8; b++)
			flagBuffer.push_back(oneByte & (1 << b));
	}



	while (!fileInput.eof()) {
		fileInput.read((char*)&Zdiff, 2);
		ZdiffBuffer.push_back(Zdiff);
	}

	vector<bool>::iterator flagBufferIt = flagBuffer.begin();
	vector<int16_t>::iterator ZdiffBufferIt = ZdiffBuffer.begin();
	int prevZmin = 0;
	int prevZmax = 0;

	for (int f = sf; f < sf + nf; f++) {
		for (int y = 0; y < height; y += blockSize) {
			for (int x = 0; x < width; x += blockSize) {
				fileOutput << "f" << setfill('0') << setw(2) << f << " ";
				fileOutput << "y" << setfill('0') << setw(4) << y << " ";
				fileOutput << "x" << setfill('0') << setw(4) << x;
				fileOutput << decodeFlags(flagBufferIt, ZdiffBufferIt, q, prevZmin, prevZmax) << " ";
				fileOutput << endl;
			}
		}
	}
}