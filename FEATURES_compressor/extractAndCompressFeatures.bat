setlocal EnableDelayedExpansion

rem seq-independent params
set SKIPTHRESH=2
set NUMFRAMES=17
set CS=420
set PARTTHRESH=40

rem for %%e in (5.5-1,5.5-2,5.5-3) do (
for %%e in (5.5-2B,5.5-3B) do (
	for %%s in (D,E,J,O,P,R) do (
		if %%e==5.5-1 (
			set MAX_BLOCKSIZE=128
			set MIN_BLOCKSIZE=64
		) else if %%e==5.5-2 (
			set MAX_BLOCKSIZE=32
			set MIN_BLOCKSIZE=16
		) else if %%e==5.5-3 (
			set MAX_BLOCKSIZE=128
			set MIN_BLOCKSIZE=8
		) else if %%e==5.5-2B (
			set MAX_BLOCKSIZE=64
			set MIN_BLOCKSIZE=32
		) else if %%e==5.5-3B (
			set MAX_BLOCKSIZE=128
			set MIN_BLOCKSIZE=16
		)

		if %%s==D (
			if %%e==5.5-1 (
				set VIEWS=0,1,2,3,4,6,7,8,11,12,13,14,15
			) else (
				set VIEWS=0,3,12,15
			)
			set W=2048
			set H=1088
			set BPS=16
			set STARTFRAME=40
		) else if %%s==E (
			if %%e==5.5-1 (
				set VIEWS=1,2,4,5,7,8,10,11,12,13
			) else (
				set VIEWS=1,5,10,13
			)
			set W=1920
			set H=1080
			set BPS=16
			set STARTFRAME=135
		) else if %%s==J (
			if %%e==5.5-1 (
				set VIEWS=0,1,2,4,5,8,10,12,14,15,19,20,21,22,23,24
			) else (
				set VIEWS=0,4,20,24
			)
			set W=1920
			set H=1080
			set BPS=10
			set STARTFRAME=0
		) else if %%s==O (
			if %%e==5.5-1 (
				set VIEWS=0,1,2,3,4,5,9,10,11,12,13,14
			) else (
				set VIEWS=0,4,10,14
			)
			set W=1920
			set H=1080
			set BPS=16
			set STARTFRAME=0
		) else if %%s==P (
			if %%e==5.5-1 (
				set VIEWS=0,1,3,5,7,8
			) else (
				set VIEWS=0,3,5,8
			)
			set W=1920
			set H=1088
			set BPS=16
			set STARTFRAME=115
		) else if %%s==R (
			if %%e==5.5-1 (
				set VIEWS=0,1,3,4,6,8,10,11,12,13,15,16,17,18,19,20
			) else (
				set VIEWS=0,7,11,18
			)
			set W=1920
			set H=1080
			set BPS=16
			set STARTFRAME=0
		)

		if !BPS!==10 (
			set QUANTS=1,2,4,8,16,32,64,128,256,512
		) else (
			set QUANTS=1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768
		)
		
		mkdir feats_%%e
		mkdir feats_%%e\S%%s
		for %%q in (!QUANTS!) do (
			mkdir feats_%%e\S%%s\q%%q
			mkdir feats_%%e\S%%s\q%%q\txt
			mkdir feats_%%e\S%%s\q%%q\enc	
			del feats_%%e\S%%s/feat_comp_%%s_%%q.7z
			for %%v in (!VIEWS!) do (
				featExtractor L:/_CE/_SEQ/S%%s/v%%v_depth_!W!x!H!_yuv%CS%p!BPS!le.yuv feats_%%e/S%%s/q%%q/txt/feat_S%%s_v%%v.txt !W! !H! !BPS! %CS% !STARTFRAME! %NUMFRAMES% !MAX_BLOCKSIZE! !MIN_BLOCKSIZE! %SKIPTHRESH% %PARTTHRESH% %%q 1 0 0
				IV_feature_compressor feats_%%e/S%%s/q%%q/txt/feat_S%%s_v%%v.txt enc
				move feats_%%e\S%%s\q%%q\txt\feat_S%%s_v%%v.txt.enc feats_%%e\S%%s\q%%q\enc\feat_S%%s_v%%v.txt.enc
				
				IV_feature_compressor feats_%%e/S%%s/q%%q/enc/feat_S%%s_v%%v.txt.enc dec
				move feats_%%e\S%%s\q%%q\enc\feat_S%%s_v%%v.txt.dec feats_%%e\S%%s\q%%q\txt\feat_S%%s_v%%v.dec.txt
			)
			7z.exe a feats_%%e/S%%s/feat2_comp_S%%s_q%%q.7z feats_%%e/S%%s/q%%q/enc
		)
	)
)